# MYSQL

## 1. 存储引擎

### MySQL存储引擎MyISAM与InnoDB区别？
- **事务支持**
  - **MyISAM**
    强调的是性能，每次查询具有原子性,其执行数度比 InnoDB 类型更快，但是不提供事务支持。
  - **InnoDB**
    提供事务支持事务，外部键等高级数据库功能。 具有事务(commit)、回滚(rollback)和崩溃修复能力(crash recovery capabilities)的事务安全 (transaction-safe (ACID compliant))型表。
- **InnoDB 支持行级锁，而 MyISAM 支持表级锁**
  用户在操作 myisam 表时，select，update，delete，insert 语句都会给表自动加锁，如果加锁以后的表满足 insert 并发的情况下，可以在表的尾部插入新的数据。
- **InnoDB 支持 MVCC, 而 MyISAM 不支持**
- **InnoDB 支持外键，而 MyISAM 不支持**
- **表主键**
  - **MyISAM**
    允许没有任何索引和主键的表存在，索引都是保存行的地址。
  - **InnoDB**
    如果没有设定主键或者非空唯一索引，就会自动生成一个 6 字节的主键(用户不可见)，数据是主索引的一部分，附加索引保存的是主索引的值。
- **InnoDB 不支持全文索引，而 MyISAM 支持**
- **可移植性、备份及恢复**
  - **MyISAM**
    数据是以文件的形式存储，所以在跨平台的数据转移中会很方便。在备份和恢复时可单独针对某个表进行操作。
  - **InnoDB**
    免费的方案可以是拷贝数据文件、备份binlog，或者用 mysqldump，在数据量达到几十 G 的时候就相对痛苦了
- **存储结构**
  - **MyISAM**
    每个 MyISAM 在磁盘上存储成三个文件。第一个文件的名字以表的名字开始，扩展名指出文件类型。.frm 文件存储表定义。数据文件的扩展名为.MYD (MYData)。索引文件的扩展名是.MYI (MYIndex)。
  - **InnoDB**
    所有的表都保存在同一个数据文件中（也可能是多个文件，或者是独立的表空间文件），InnoDB 表的大小只受限于操作系统文件的大小，一般为 2GB

### InnoDB引擎的4大特性是什么？

- 插入缓冲（insert buffer)
- 二次写(double write)
- 自适应哈希索引(ahi)
- 预读(read ahead)

### 存储引擎该如何选择？

如果没有特别的需求，使用默认的`Innodb`即可。

- **MyISAM**：
  以读写插入为主的应用程序，比如博客系统、新闻门户网站。
- **Innodb**：
  更新（删除）操作频率也高，或者要保证数据的完整性；并发量高，支持事务和外键。比如OA自动化办公系统。

## 4. MySQL 索引

### 什么是索引？
索引是一种特殊的文件(InnoDB数据表上的索引是表空间的一个组成部分)，它们包含着对数据表里所有记录的引用指针。

索引是一种数据结构。数据库索引，是数据库管理系统中一个排序的数据结构，以协助快速查询、更新数据库表中数据。索引的实现通常使用B树及其变种B+树。

更通俗的说，索引就相当于目录。为了方便查找书中的内容，通过对内容建立索引形成目录。索引是一个文件，它是要占据物理空间的。


# MySQL的事务

MySQL 事务具有四个特性：**原子性、一致性、隔离性、持久性**，这四个特性简称 **ACID** 特性

**一、原子性（Atomicity ）**：一个事务是一个不可再分割的整体，要么全部成功，要么全部失败

事务在数据库中就是一个基本的工作单位，事务中包含的逻辑操作（SQL 语句），只有两种情况：成功和失败。事务的原子性其实指的就是这个逻辑操作过程具有原子性，不会出现有的逻辑操作成功，有的逻辑操作失败这种情况

**二、一致性（Consistency ）**：一个事务可以让数据从一种一致状态切换到另一种一致性状态

举例说明：张三给李四转账 100 元，那么张三的余额应减少 100 元，李四的余额应增加 100 元，张三的余额减少和李四的余额增加这是两个逻辑操作具有一致性

**三、隔离性（Isolution ）：**一个事务不受其他事务的影响，并且多个事务彼此隔离

一个事务内部的操作及使用的数据，对并发的其他事务是隔离的，并发执行的各个事务之间不会互相干扰

**四、持久性（Durability ）：**一个事务一旦被提交，在数据库中的改变就是永久的，提交后就不能再回滚

一个事务被提交后，在数据库中的改变就是永久的，即使系统崩溃重新启动数据库数据也不会发生改变

## 5、隔离级别

| 隔离级别         | 脏读 | 不可重复读 | 幻影读 |
| ---------------- | ---- | ---------- | ------ |
| READ-UNCOMMITTED | √    | √          | √      |
| READ-COMMITTED   | ×    | √          | √      |
| REPEATABLE-READ  | ×    | ×          | √      |
| SERIALIZABLE     | ×    | ×          | ×      |

- Read Uncommitted（读取未提交内容）
  在该隔离级别，所有事务都可以看到其他未提交事务的执行结果。本隔离级别很少用于实际应用，因为它的性能也不比其他级别好多少。读取未提交的数据，也被称之为脏读（Dirty Read）。
- Read Committed（读取提交内容）
  这是大多数数据库系统的默认隔离级别（但不是 MySQL 默认的）。它满足了隔离的简单定义：一个事务只能看见已经提交事务所做的改变。这种隔离级别也支持所谓的不可重复读（Nonrepeatable Read），因为同一事务的其他实例在该实例处理其间可能会有新的 commit，所以同一 select 可能返回不同结果。
- Repeatable Read（可重读）
  这是 MySQL 的默认事务隔离级别，它确保同一事务的多个实例在并发读取数据时，会看到同样的数据行。不过理论上，这会导致另一个棘手的问题：幻读（Phantom Read）。简单的说，幻读指当用户读取某一范围的数据行时，另一个事务又在该范围内插入了新行，当用户再读取该范围的数据行时，会发现有新的“幻影” 行。InnoDB 和 Falcon 存储引擎通过多版本并发控制（MVCC，Multiversion Concurrency Control 间隙锁）机制解决了该问题。注：其实多版本只是解决不可重复读问题，而加上间隙锁（也就是它这里所谓的并发控制）才解决了幻读问题。
- Serializable（可串行化）
  这是最高的隔离级别，它通过强制事务排序，使之不可能相互冲突，从而解决幻读问题。简言之，它是在每个读的数据行上加上共享锁。在这个级别，可能导致大量的超时现象和锁竞争。

#### 事务隔离级别与锁的关系

在Read Uncommitted级别下，读取数据不需要加共享锁，这样就不会跟被修改的数据上的排他锁冲突

在Read Committed级别下，读操作需要加共享锁，但是在语句执行完以后释放共享锁；

在Repeatable Read级别下，读操作需要加共享锁，但是在事务提交之前并不释放共享锁，也就是必须等待事务执行完毕以后才释放共享锁。

SERIALIZABLE 是限制性最强的隔离级别，因为该级别锁定整个范围的键，并一直持有锁，直到事务完成。

## 6、什么是脏读？幻读？不可重复读？

- 脏读(Drity Read)：
  某个事务已更新一份数据，另一个事务在此时读取了同一份数据，由于某些原因，前一个RollBack了操作，则后一个事务所读取的数据就会是不正确的。
  
- 不可重复读(Non-repeatable read):
  在一个事务的两次查询之中数据不一致，这可能是两次查询过程中间插入了一个事务更新的原有的数据。
  
  是指在一个事务内，多次读同一数据。在这个事务还没有结束时，另外一个事务也访问该同一数据。那么，在第一个事务中的两 次读数据之间，由于第二个事务的修改，那么第一个事务两次读到的的数据可能是不一样的。这样就发生了在一个事务内两次读到的数据是不一样的，因此称为是不可重复读。例如，一个编辑人员两次读取同一文档，但在两次读取之间，作者重写了该文档。当编辑人员第二次读取文档时，文档已更改。原始读取不可重复。如果只有在作者全部完成编写后编辑人员才可以读取文档，则可以避免该问题。
  
- 幻读(Phantom Read):
  在一个事务的两次查询中数据笔数不一致，例如有一个事务查询了几列(Row)数据，而另一个事务却在此时插入了新的几列数据，先前的事务在接下来的查询中，就会发现有几列数据是它先前所没有的。

  - 解决方案：

    - 使用for update

      在新增操作之前给一个不存在的记录加一个for update的锁，只允许自己来操作3号记录

      ![img](https://img-blog.csdnimg.cn/98184443eaf74fe9b63de49b0bc82ae8.png)

    - 使用串行读

      设置为串行读，在tx1执行读操作，会加一个共享读锁，其他事务可以读，但不能增删改

      ![img](https://img-blog.csdnimg.cn/a8207cf57fee48ea9504f92c2e4c7407.png)				


## 7、MYSQL索引

### 1. 索引的特点

page之间是双链表形式，而每个page内部的数据则是单链表形式存在。当进行数据查询时，会限定位到具体的page，然后在page中通过二分查找具体的记录。

![](http://source.mycookies.cn/202006110041_610.jpg?ERROR)

并且索引的顺序不同，数据的存储顺序则也不同。所以在开发过程中，一定要注意索引字段的先后顺序。

![](http://source.mycookies.cn/202006110041_262.jpg?ERROR)

> 最左匹配原则

当一个索引中包含多个字段时，可以称之为组合索引。MySQL中有个很重要的规则，即最左匹配原则用来定义组合索引的命中规则，它是指在检索数据时从联合索引的最左边开始匹配。假设对用户表建立一个联合索引（a，b，c），那么条件a，（a，b），（a，b，c）都会用到索引。

在匹配过程中会优先根据最左前面的字段a进行匹配，然后再判断是否用到了索引字段b，直到无法找到对应的索引字段，或者对应的索引被”破坏“（下文中会介绍）。

### 2. 正确创建索引

> 尽量使用自增长主键

使用自增长主键的原因笔者认为有两个。首先能有效减少页分裂，MySQL中数据是以页为单位存储的且每个页的大小是固定的（默认16kb），如果一个数据页的数据满了，则需要分成两个页来存储，这个过程就叫做页分裂。

如果使用了自增主键的话，新插入的数据都会尽量的往一个数据页中写，写满了之后再申请一个新的数据页写即可（大多数情况下不需要分裂，除非父节点的容量也满了）。

自增主键

![](http://file.mycookies.cn/201805191537_258.gif?imageView1/JannLee/md/01)

非自增主键

![](http://file.mycookies.cn/201805191538_202.gif?imageView1/JannLee/md/01)

其次，对于缓存友好。系统分配给MySQL的内存有限，对于数据量比较多的数据库来说，通常只有一小部分数据在内存中，而大多数数据都在磁盘中。如果使用无序的主键，则会造成随机的磁盘IO，影响系统性能。

> 选择性高的列优先

关注索引的选择性。索引的选择性，也可称为数据的熵。在创建索引的时候通常要求将选择性高的列放在最前面，对于选择性不高的列甚至可以不创建索引。如果选择性不高，极端性情况下可能会扫描全部或者大多数索引，然后再回表，这个过程可能不如直接走主键索引性能高。

![](http://source.mycookies.cn/202006160100_924.png?ERROR)

索引列的选择往往需要根据具体的业务场景来选择，但是需要注意的是索引的区分度越高则价值就越高，意味着对于检索的性价比就高。索引的区分度等于count(distinct 具体的列) / count(*)，表示字段不重复的比例。

唯一键的区分度是1，而对于一些状态值，性别等字段区分度往往比较低，在数据量比较大的情况下，甚至有无限接近0。假设一张表中用data_status来表示数据的状态，1-有效，2-删除，则数据的区分度为 1/500000。如果100万条数据中只有1条被删除，并且在查询数据时查找data_status = 0 的数据时，需要进行全表扫描。由于索引也是需要占用内存的，所以在内存较为有限的环境下，区分度不高的索引几乎没有意义。

> 联合索引优先于多列独立索引

联合索引优先于多列独立索引， 假设有三个字段a,b,c, 索引（a）(a,b)，(a,b,c)可以使用(a,b,c)代替。MySQL中的索引并不是越多越好，各个公司的规定中往往会限制单表中的索引的个数。原因在于，索引本身也会占用一定的空间，并且维护一个索引时有一定的代码的，所以在满足需求的情况下一定要尽可能创建更少的索引。

执行语句：

```sql
explain select * from test_table where a = "zhangsan";
explain select * from test_table where a = "zhangsan" and b = "188466668888";
explain select * from test_table where a = "zhangsan" and b = "188466668888" and c = "23";
```

执行结果分析:

![](http://source.mycookies.cn/202006110027_96.png?ERROR)

![](http://source.mycookies.cn/202006110030_151.png?ERROR)

![](http://source.mycookies.cn/202006110030_750.png?ERROR)

实际上建立(a, b, c)联合索引时，其作用相当于(a), (a, b), (a, b, c) 三个索引。所以以上三种查询方式均会命中索引。

> 覆盖索引避免回表

覆盖索引如果执行的语句是 select ID from T where k between 3 and 5，这时只需要查 ID 的值，而 ID 的值已经在 k 索引树上了，因此可以直接提供查询结果，不需要回表。也就是说，在这个查询里面，索引 k 已经“覆盖了”我们的查询需求，我们称为覆盖索引。由于覆盖索引可以减少树的搜索次数，显著提升查询性能，所以使用覆盖索引是一个常用的性能优化手段。

![](http://source.mycookies.cn/202006160052_611.png?ERROR)

![](http://source.mycookies.cn/202006160051_191.png?ERROR)

**覆盖索引的查询优化**

覆盖索引同时还会影响索引的选择，对于（a，b，c）索引来说，理论上来说不满足最左匹配原则，但是实际上也会走索引。原因在于，优化器认为（a，b，c）索引的性能会高于全表扫描，实际情况也是这样的，感兴趣的小伙伴不妨分析一下上文中介绍的数据结构。

### 3. 正确使用索引

建立合适的索引是前提，想要取得理想的查询性能，还应保证能够用到索引。避免索引失效即是优化。

> 不在索引上进行任何操作

索引上进行**计算，函数，类型转换**等操作都会导致索引从当前位置（联合索引多个字段，不影响前面字段的匹配）失效，可能会进行全表扫描。

```sql
explain select * from test_table where upper(a) = "ZHANGSAN" 
```

![](http://source.mycookies.cn/202006110033_481.png?ERROR)

对于需要计算的字段，则一定要将计算方法放在“=”后面，否则会破坏索引的匹配，目前来说MySQL优化器不能对此进行优化。

```sql
explain select * from test_table where a = lower("ZHANGSAN")
```

![](http://source.mycookies.cn/202006110033_180.png?ERROR)

**隐式类型转换**

需要注意的是，在查询时一定要注意字段类型问题，比如a字段时字符串类型的，而匹配参数用的是int类型，此时就会发生隐式类型转换，相当于相当于在索引上使用函数。

```sql
Copyexplain select * from test_table where a = 1;
```

[![img](http://source.mycookies.cn/202006110033_794.png?ERROR)](https://source.mycookies.cn/202006110033_794.png?ERROR)


`a是字符串类型，然后使用int类型的1进行匹配`,此时就发生了隐式类型转换，破坏索引的使用。



> 只查询需要的列

在日常开发中很多同学习惯使用 select * … 来构建查询语句，这种做法也是极不推荐的。主要原因有两个，首先查询无用的列在数据传输和解析绑定过程中会增加网络IO，以及CPU的开销，尽管往往这些消耗可以被忽略，但是我们也要避免埋坑。

```sql
Copyexplain select a,b,c from test_table where a="zhangsan" and b = "188466668888" and c = "23";
```

[![img](http://source.mycookies.cn/202006110033_403.png?ERROR)](https://source.mycookies.cn/202006110033_403.png?ERROR)



其次就是会使得覆盖索引"失效", 这里的失效并非真正的不走索引。覆盖索引的本质就是在索引中包含所要查询的字段，而 select * 将使覆盖索引失去意义，仍然需要进行回表操作，毕竟索引通常不会包含所有的字段，这一点很重要。

```sql
Copyexplain select * from test_table where a="zhangsan" and b = "188466668888" and c = "23";
```

[![img](http://source.mycookies.cn/202006110033_156.png?ERROR)](https://source.mycookies.cn/202006110033_156.png?ERROR)

> 不等式条件

查询语句中只要包含不等式，负向查询一般都不会走索引，如 !=, <>, not in, not like等。

```sql
Copyexplain select * from test_table where a !="1222" and b="12222222222" and c = 23;
explain select * from test_table where a <>"1222" and b="12222222222" and c = 23;
```

[![img](http://source.mycookies.cn/202006110034_426.png?ERROR)](https://source.mycookies.cn/202006110034_426.png?ERROR)

```sql
Copyexplain select * from test_table where a not in ("xxxx");
```

[![img](http://source.mycookies.cn/202006110034_841.png?ERROR)](https://source.mycookies.cn/202006110034_841.png?ERROR)

> 模糊匹配查询

最左前缀在进行模糊匹配时，一般禁止使用%前导的查询，如like “%zhangsan”。

```sql
Copyexplain select * from test_table where a like "zhangsan";
explain select * from test_table where a like "%zhangsan";
explain select * from test_table where a like "zhangsan%";
```

[![img](http://source.mycookies.cn/202006110034_78.png?ERROR)](https://source.mycookies.cn/202006110034_78.png?ERROR)

[![img](http://source.mycookies.cn/202006110034_241.png?ERROR)](https://source.mycookies.cn/202006110034_241.png?ERROR)

[![img](http://source.mycookies.cn/202006110034_216.png?ERROR)](https://source.mycookies.cn/202006110034_216.png?ERROR)

> 最左匹配原则

索引是有顺序的，查询条件中缺失索引列之后的其他条件都不会走索引。比如(a, b, c)索引，只使用b, c索引，就不会走索引。

```sql
Copyexplain select * from test_table where b = "188466668888" and c = "23";
```

[![img](http://source.mycookies.cn/202006110034_90.png?ERROR)](https://source.mycookies.cn/202006110034_90.png?ERROR)

如果索引从中间断开，索引会部分失效。这里的断开指的是缺失该字段的查询条件，或者说满足上述索引失效情况的任意一个。不过这里的仍然会使用到索引，只不过只能使用到索引的前半部分。

```sql
Copyexplain select * from test_table where a="zhangsan" and b != 1 and c = "23"
```

[![img](http://source.mycookies.cn/202006110034_491.png?ERROR)](http://source.mycookies.cn/202006110034_491.png?ERROR)

值得注意的是，如果使用了不等式查询条件，会导致索引完全失效。而上一个例子中即使用了不等式条件，也使用了隐式类型转换却能用到索引。

[![img](http://source.mycookies.cn/202006110034_290.png?ERROR)](https://source.mycookies.cn/202006110034_290.png?ERROR)

同理，根据最左前缀匹配原则，以下如果使用b，c作为查询条件则不会使用(a, b, c)索引。

执行语句：

```sql
Copyexplain select * from test_table where b = "188466668888" and c = "23";
```

执行结果：

[![img](http://source.mycookies.cn/202006110032_785.png?ERROR)](https://source.mycookies.cn/202006110032_785.png?ERROR)

**索引下推**

在说索引下推之前，我们先执行一下SQL。

执行语句：

```sql
Copyexplain select * from test_table where a = "zhangsan" and c = "23";
```

[![img](http://source.mycookies.cn/202006110033_166.png?ERROR)](https://source.mycookies.cn/202006110033_166.png?ERROR)

上述的最左前缀匹配原则相信大家都能很容易的理解，那么使用(a, c)条件查询能够利用(a, b, c)吗？答案是肯定的，正如上图所示。即使没有索引下推也会会根据最左匹配原则，使用到索引中的a字段。有了索引下推之后会增加查询的效率。

在面试中通常会问到这样一个问题，已知有索引(a,b,c)则根据条件(a,c)查询时会不会走索引呢？答案是肯定的，但是是有版本限制的。

而 MySQL 5.6 引入的索引下推优化（index condition pushdown)， 可以在索引遍历过程中，对索引中包含的字段先做判断，直接过滤掉不满足条件的记录，减少回表次数，是对查询的一种优化

[![img](http://source.mycookies.cn/202006170049_552.jpg?ERROR)](http://source.mycookies.cn/202006170049_552.jpg?ERROR)

上述是没有索引下推，每次查询完之后都会回表，取到对应的字段进行匹配。

[![img](http://source.mycookies.cn/202006170050_191.jpg?ERROR)](http://source.mycookies.cn/202006170050_191.jpg?ERROR)


利用索引下推，每次尽可能在辅助索引中将不符合条件数据过滤掉。比如，索引中已经包含了name和age，索引不妨暂且忽略破坏索引匹配的条件直接匹配。



**查询优化-自适应索引顺序**

查询时，mysql的优化器会优化sql的执行，即使查询条件的顺序没有按照定义顺序来使用，也是可以使用索引的。但是需要注意的是优化本身也会消耗一定的性能，所以还是推荐按照索引的定义来书写sql。

```sql
Copyexplain select  * from test_table where b="12222222222" and a="zhangsan" and c = 23;
explain select  * from test_table where a="zhangsan" and b="12222222222" and c = 23;
```

[![img](http://source.mycookies.cn/202006110034_91.png?ERROR)](https://source.mycookies.cn/202006110034_91.png?ERROR)

### 4. 总结[#](https://www.cnblogs.com/liqiangchn/p/13155536.html#875728103)

索引并不是什么高深的技术，从底层来看，不过是一个数据结构罢了。要想使用好索引，一定要先将B+Tree理解透彻，在此基础上对于日常使用和面试则是信手拈来。

脱离业务的设计都是耍流氓，技术的意义在于服务业务。所以，索引的设计需要充分考虑业务的需求与设计原则之间做一些取舍，满足需求是基础。

在工作中，各个公司的版本可能大不相同，会存在一些奇奇怪怪，不确定的问题。所以为了验证索引的有效性，强烈推荐把主要的查询sql都通过explain查看一下执行计划，是否会用到索引。javascript:void(0))



### 5、索引的类型

**普通索引**

> 即针对数据库表创建索引

添加方式

```sql
# 创建普通索引
ALTER TABLE table_name ADD INDEX index_name (column);
# 创建组合索引
ALTER TABLE table_name ADD INDEX index_name(column1, column2, column3);
```

**唯一索引**

> 与普通索引类似，不同的就是：MySQL 数据库索引列的值必须唯一，但允许有空值

添加方式

```mysql
# 创建唯一索引
ALTER TABLE table_name ADD UNIQUE (column);

# 创建唯一组合索引
ALTER TABLE table_name ADD UNIQUE (column1,column2);
```

**主键索引**

> 它是一种特殊的唯一索引，不允许有空值。一般是在建表的时候同时创建主键索引

**全文索引**

> 是目前搜索引擎使用的一种关键技术

- 添加方式

  ```mysql
  ALTER TABLE table_name ADD FULLTEXT (column);
  ```

### 6、数据库中索引的工作机制？

数据库索引，是数据库管理系统中一个排序的数据结构，以协助快速查询、更新数据库表中数据。
索引的实现通常使用 B 树及其变种 B+树

### 7、索引的基本原理是什么？

索引用来快速地寻找那些具有特定值的记录。如果没有索引，一般来说执行查询时遍历整张表。

索引的原理很简单，就是把无序的数据变成有序的查询

1. 把创建了索引的列的内容进行排序
2. 对排序结果生成倒排表
3. 在倒排表内容上拼上数据地址链
4. 在查询的时候，先拿到倒排表内容，再取出数据地址链，从而拿到具体数据

### 8、索引算法有哪些？

索引算法有 `BTree`算法和`Hash`算法

- **BTree算法**
  BTree是最常用的mysql数据库索引算法，也是mysql默认的算法。因为它不仅可以被用在=,>,>=,<,<=和between这些比较操作符上，而且还可以用于like操作符，只要它的查询条件是一个不以通配符开头的常量，例如：

  ```sql
  -- 只要它的查询条件是一个不以通配符开头的常量
  select * from user where name like 'jack%'; 
  -- 如果一通配符开头，或者没有使用常量，则不会使用索引，例如： 
  select * from user where name like '%jack'; 
  ```

- **Hash算法**
  Hash索引只能用于对等比较，例如=,<=>（相当于=）操作符。由于是一次定位数据，不像BTree索引需要从根节点到枝节点，最后才能访问到页节点这样多次IO访问，所以检索效率远高于BTree索引。

### 9、索引可以随意无限的创建吗？创建索引的原则有那些？

索引虽好，但也不是无限制的使用，最好符合一下几个原则

1. 最左前缀匹配原则，组合索引非常重要的原则，mysql会一直向右匹配直到遇到范围查询(>、<、between、like)就停止匹配，比如a = 1 and b = 2 and c > 3 and d = 4 如果建立(a,b,c,d)顺序的索引，d是用不到索引的，如果建立(a,b,d,c)的索引则都可以用到，a,b,d的顺序可以任意调整；
2. 较频繁作为查询条件的字段才去创建索引；
3. 更新频繁字段不适合创建索引；
4. 若是不能有效区分数据的列不适合做索引列(如性别，男女未知，最多也就三种，区分度实在太低)；
5. 尽量的扩展索引，不要新建索引。比如表中已经有a的索引，现在要加(a,b)的索引，那么只需要修改原来的索引即可；
6. 定义有外键的数据列一定要建立索引；
7. 对于那些查询中很少涉及的列，重复值比较多的列不要建立索引；
8. 对于定义为text、image和bit的数据类型的列不要建立索引。

### 10、索引什么时候会失效

1. 对于组合索引，未使用组合索引最左边的字段，则不会使用索引。
2. 以%开头的like查询如%abc，无法使用索引；非%开头的like查询如abc%，相当于范围查询，会使用索引。
3. 查询条件中列类型是字符串，但是没有使用引号，可能会因为类型不同发生隐式转换，使索引失效。
4. MySQL在使用不等于号（！=或者<>）的时候无法使用索引会导致全表扫描。
5. is null、is not null 也无法使用索引。
6. 对索引列进行运算。
7. 查询条件使用or连接，也会导致索引失效。

## 11、explain的使用

### Explain中的列

#### ① id列

​    id列的编号是select的序列号，有几个select就有几个id，并且id是按照select出现的顺序增长的，id列的值越大优先级越高，id相同则是按照执行计划列从上往下执行，id为空则是最后执行。

#### ② select_type列

​    表示对应行是简单查询还是复杂查询。

​	1）simple：不包含子查询和union的简单查询

![img](https://img-blog.csdnimg.cn/2f768fa6e05f4d89ab95c3b27a2886d5.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2l0aCBvdXQgZW5k,size_20,color_FFFFFF,t_70,g_se,x_16)

 	2）primary：复杂查询中最外层的select

​     3）subquery：包含在select中的子查询（不在from的子句中）

 用如下图展示primary和subquery类型

![img](https://img-blog.csdnimg.cn/b14940b3b7cb49a796eeaad688d9fc88.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2l0aCBvdXQgZW5k,size_20,color_FFFFFF,t_70,g_se,x_16)

​	4）derived：包含在from子句中的子查询。mysql会将查询结果放入一个[临时表](https://so.csdn.net/so/search?q=临时表&spm=1001.2101.3001.7020)中，此临时表也叫衍生表。

![img](https://img-blog.csdnimg.cn/a57be66c15684664add5f17aba02bbca.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2l0aCBvdXQgZW5k,size_20,color_FFFFFF,t_70,g_se,x_16)

​	5）union：在union中的第二个和随后的select，UNION RESULT为合并的结果![img](https://img-blog.csdnimg.cn/78a6f4afee1346fb9176afcd684acd73.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2l0aCBvdXQgZW5k,size_20,color_FFFFFF,t_70,g_se,x_16)

#### ③ table列

​	表示当前行访问的是哪张表。当from中有子查询时，table列的格式为<derivedN>，表示当前查询依赖id=N行的查询，所以先执行id=N行的查询，如上面select_type列图4所示。当有union查询时，UNION RESULT的table列的值为<union1,2>，1和2表示参与union的行id。

#### ④ partitions列

​    查询将匹配记录的分区。 对于非[分区表](https://so.csdn.net/so/search?q=分区表&spm=1001.2101.3001.7020)，该值为 NULL。

#### ⑤ type列

​    此列表示关联类型或访问类型。也就是MySQL决定如何查找表中的行。依次从最优到最差分别为：system > const > eq_ref > ref > range > index > all。

 NULL：MySQL能在优化阶段分解查询语句，在执行阶段不用再去访问表或者索引。![img](https://img-blog.csdnimg.cn/b750ef4009b34fd6b4fb7f3b52243532.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2l0aCBvdXQgZW5k,size_20,color_FFFFFF,t_70,g_se,x_16)

​	system、const：MySQL对查询的某部分进行优化并把其转化成一个常量（可以通过show warnings命令查看结果）。system是const的一个特例，表示表里只有一条元组匹配时为system。

![img](https://img-blog.csdnimg.cn/f2f32d2468ae435d9d19c26284647c5d.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2l0aCBvdXQgZW5k,size_20,color_FFFFFF,t_70,g_se,x_16)

![img](https://img-blog.csdnimg.cn/c85c851b0f50429b88cb60fd523afa77.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2l0aCBvdXQgZW5k,size_19,color_FFFFFF,t_70,g_se,x_16)

​     eq_ref：主键或唯一键索引被连接使用，最多只会返回一条符合条件的记录。简单的select查询不会出现这种type。![img](https://img-blog.csdnimg.cn/b9a94d9490df4a5ea55aaba5a8435d04.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2l0aCBvdXQgZW5k,size_20,color_FFFFFF,t_70,g_se,x_16)

ref：相比eq_ref，不使用唯一索引，而是使用普通索引或者唯一索引的部分前缀，索引和某个值比较，会找到多个符合条件的行。![img](https://img-blog.csdnimg.cn/ce6bb252ecdc4dbf947bcbe7bedda70d.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2l0aCBvdXQgZW5k,size_20,color_FFFFFF,t_70,g_se,x_16)

range：通常出现在范围查询中，比如in、between、大于、小于等。使用索引来检索给定范围的行。![img](https://img-blog.csdnimg.cn/573979004689414d945f60538ccfbb62.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2l0aCBvdXQgZW5k,size_20,color_FFFFFF,t_70,g_se,x_16)

 index：扫描全索引拿到结果，一般是扫描某个二级索引，二级索引一般比较少，所以通常比ALL快一点。

![img](https://img-blog.csdnimg.cn/9c220a48cb3e4a3f86bf9f1e1bbc0ba9.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2l0aCBvdXQgZW5k,size_20,color_FFFFFF,t_70,g_se,x_16)

ALL：全表扫描，扫描聚簇索引的所有叶子节点。![img](https://img-blog.csdnimg.cn/ea797e901eb04fcba6bba1c4e6827320.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2l0aCBvdXQgZW5k,size_20,color_FFFFFF,t_70,g_se,x_16)

#### ⑥ possible_keys列

​    此列显示在查询中可能用到的索引。如果该列为NULL，则表示没有相关索引，可以通过检查where子句看是否可以添加一个适当的索引来提高性能。

#### ⑦ key列

 	此列显示MySQL在查询时实际用到的索引。在执行计划中可能出现possible_keys列有值，而key列为null，这种情况可能是表中数据不多，MySQL认为索引对当前查询帮助不大而选择了全表查询。如果想强制MySQL使用或忽视possible_keys列中的索引，在查询时可使用force index、ignore index。

#### ⑧ key_len列

​    此列显示MySQL在索引里使用的字节数，通过此列可以算出具体使用了索引中的那些列。索引最大长度为768字节，当长度过大时，MySQL会做一个类似最左前缀处理，将前半部分字符提取出做索引。当字段可以为null时，还需要1个字节去记录。

 key_len计算规则：

​     字符串：

​       char(n)：n个数字或者字母占n个字节，汉字占3n个字节

​		varchar(n)： n个数字或者字母占n个字节，汉字占3n+2个字节。+2字节用来存储字符串长度。

​	 数字类型：

​       tinyint：1字节   smallint：2字节        int：4字节       bigint：8字节

 	时间类型

​       date：3字节    timestamp：4字节     datetime：8字节

 #### ⑨ ref列

​    此列显示key列记录的索引中，表查找值时使用到的列或常量。常见的有const、字段名

#### ⑩ rows列

​    此列是MySQL在查询中估计要读取的行数。注意这里不是结果集的行数。

#### ⑪ Extra列

​    此列是一些额外信息。常见的重要值如下：

​		1）Using index：使用覆盖索引（如果select后面查询的字段都可以从这个索引的树中获取，不需要通过辅助索引树找到主键，再通过主键去主键索引树里获取其它字段值，这种情况一般可以说是用到了覆盖索引）。

​		2）Using where：使用 where 语句来处理结果，并且查询的列未被索引覆盖。

​        3）Using index condition：查询的列不完全被索引覆盖，where条件中是一个查询的范围。

​		4）Using temporary：MySQL需要创建一张临时表来处理查询。出现这种情况一般是要进行优化的。

​        5）Using filesort：将使用外部排序而不是索引排序，数据较小时从内存排序，否则需要在磁盘完成排序。

​		6）Select tables optimized away：使用某些聚合函数（比如 max、min）来访问存在索引的某个字段时。

## SQL优化的30种方式

1、对查询进行优化，应尽量避免全表扫描，首先应考虑在 where 及 order by 涉及的列上建立索引。

2、应尽量避免在 where 子句中使用!=或<>操作符，否则将引擎放弃使用索引而进行全表扫描。

3、应尽量避免在 where 子句中对字段进行 null 值判断，否则将导致引擎放弃使用索引而进行全表扫描，如：

```sql
select id from t where num is null 
```


可以在num上设置默认值0，确保表中num列没有null值，然后这样查询：

```sql
select id from t where num=0 
```


4、应尽量避免在 where 子句中使用 or 来连接条件，否则将导致引擎放弃使用索引而进行全表扫描，如：

```sql
select id from t where num=10 or num=20 
```


可以这样查询：

```sql
select id from t where num=10 
union all 
select id from t where num=20 
```

5、下面的查询也将导致全表扫描：

```sql
select id from t where name like '%abc%' 
```


若要提高效率，可以考虑全文检索。

6、in 和 not in 也要慎用，否则会导致全表扫描，如：

```sql
select id from t where num in(1,2,3) 
```


对于连续的数值，能用 between 就不要用 in 了：

```sql
select id from t where num between 1 and 3 
```


7、如果在 where 子句中使用参数，也会导致全表扫描。因为SQL只有在运行时才会解析局部变量，但优化程序不能将访问计划的选择推迟到运行时；它必须在编译时进行选择。然而，如果在编译时建立访问计划，变量的值还是未知的，因而无法作为索引选择的输入项。如下面语句将进行全表扫描：

```sql
select id from t where num=@num 
```


可以改为强制查询使用索引：

```sql
select id from t with(index(索引名)) where num=@num 
```


8、应尽量避免在 where 子句中对字段进行表达式操作，这将导致引擎放弃使用索引而进行全表扫描。如：

```sql
select id from t where num/2=100 
```


应改为:

```sql
select id from t where num=100*2 
```


9、应尽量避免在where子句中对字段进行函数操作，这将导致引擎放弃使用索引而进行全表扫描。如：

```sql
select id from t where substring(name,1,3)='abc'--name以abc开头的id 
select id from t where datediff(day,createdate,'2005-11-30')=0--'2005-11-30'生成的id 
```

应改为:

```sql
select id from t where name like 'abc%' 
select id from t where createdate>='2005-11-30' and createdate<'2005-12-1' 
```


10、不要在 where 子句中的“=”左边进行函数、算术运算或其他表达式运算，否则系统将可能无法正确使用索引。

11、在使用索引字段作为条件时，如果该索引是复合索引，那么必须使用到该索引中的第一个字段作为条件时才能保证系统使用该索引，否则该索引将不会被使用，并且应尽可能的让字段顺序与索引顺序相一致。

12、不要写一些没有意义的查询，如需要生成一个空表结构：

```sql
select col1,col2 into #t from t where 1=0 
```


这类代码不会返回任何结果集，但是会消耗系统资源的，应改成这样：

```sql
create table #t(...) 
```


13、很多时候用 exists 代替 in 是一个好的选择：

```sql
select num from a where num in(select num from b) 
```


用下面的语句替换：

```sql
select num from a where exists(select 1 from b where num=a.num) 
```


14、并不是所有索引对查询都有效，SQL是根据表中数据来进行查询优化的，当索引列有大量数据重复时，SQL查询可能不会去利用索引，如一表中有字段sex，male、female几乎各一半，那么即使在sex上建了索引也对查询效率起不了作用。

15、索引并不是越多越好，索引固然可以提高相应的 select 的效率，但同时也降低了 insert 及 update 的效率，因为 insert 或 update 时有可能会重建索引，所以怎样建索引需要慎重考虑，视具体情况而定。一个表的索引数最好不要超过6个，若太多则应考虑一些不常使用到的列上建的索引是否有必要。

16、应尽可能的避免更新 clustered 索引数据列，因为 clustered 索引数据列的顺序就是表记录的物理存储顺序，一旦该列值改变将导致整个表记录的顺序的调整，会耗费相当大的资源。若应用系统需要频繁更新 clustered 索引数据列，那么需要考虑是否应将该索引建为 clustered 索引。

17、尽量使用数字型字段，若只含数值信息的字段尽量不要设计为字符型，这会降低查询和连接的性能，并会增加存储开销。这是因为引擎在处理查询和连接时会逐个比较字符串中每一个字符，而对于数字型而言只需要比较一次就够了。

18、尽可能的使用 varchar/nvarchar 代替 char/nchar ，因为首先变长字段存储空间小，可以节省存储空间，其次对于查询来说，在一个相对较小的字段内搜索效率显然要高些。

19、任何地方都不要使用 select * from t ，用具体的字段列表代替“*”，不要返回用不到的任何字段。

20、尽量使用表变量来代替临时表。如果表变量包含大量数据，请注意索引非常有限（只有主键索引）。

21、避免频繁创建和删除临时表，以减少系统表资源的消耗。

22、临时表并不是不可使用，适当地使用它们可以使某些例程更有效，例如，当需要重复引用大型表或常用表中的某个数据集时。但是，对于一次性事件，最好使用导出表。

23、在新建临时表时，如果一次性插入数据量很大，那么可以使用 select into 代替 create table，避免造成大量 log ，以提高速度；如果数据量不大，为了缓和系统表的资源，应先create table，然后insert。

24、如果使用到了临时表，在存储过程的最后务必将所有的临时表显式删除，先 truncate table ，然后 drop table ，这样可以避免系统表的较长时间锁定。

25、尽量避免使用游标，因为游标的效率较差，如果游标操作的数据超过1万行，那么就应该考虑改写。

26、使用基于游标的方法或临时表方法之前，应先寻找基于集的解决方案来解决问题，基于集的方法通常更有效。

27、与临时表一样，游标并不是不可使用。对小型数据集使用 FAST_FORWARD 游标通常要优于其他逐行处理方法，尤其是在必须引用几个表才能获得所需的数据时。在结果集中包括“合计”的例程通常要比使用游标执行的速度快。如果开发时间允许，基于游标的方法和基于集的方法都可以尝试一下，看哪一种方法的效果更好。

28、在所有的存储过程和触发器的开始处设置 SET NOCOUNT ON ，在结束时设置 SET NOCOUNT OFF 。
无需在执行存储过程和触发器的每个语句后向客户端发送 DONE_IN_PROC 消息。

29、尽量避免向客户端返回大数据量，若数据量过大，应该考虑相应需求是否合理。

30、尽量避免大事务操作，提高系统并发能力。

## SQL索引失效

1、 没有正确使用复合索引
例如我们根据user表中的A,B,C创建一个符合索引

```sql
create index on user(A,B,C)
```


实际上mysql是为我们创建了三个索引

1. A

2. AB
3. ABC

假如你从B 或者C 开始查 就没有用到索引

```sql
select B from user while B = ‘b’
```

2、范围查询右边的列使用索引无效

```sql
select A from user where C > 1 and A = 'a'
```

3、对索引列进行运算，索引失效

```sql
select A from user where subString(A,3,2) = '科技'
```

4、字符串类型没有加 ’ '， 索引失效
	在查询中，没有对字符串加单引号，MySQL的查询优化器，会自动的进行类型转化，造成索引失效

5、select * 可能导致失效
	查询全部的字段，有一些字段可能没有建立索引，导致需要去表中查询，所以会导致索引失效

6、or 前后字段有一个没有建立索引，另外一个索引也失效

7、模糊查询 like

- 尾部模糊匹配，like ‘某某值 %’ ，索引不失效
- 头部模糊匹配，like ‘% 某某值’ ，索引失效

8、in ， no in ，null ，not null 有时候

​	因为我们索引的目的是为了快速定位，如果值全是null 这个时候，索引就没有意义，直接走全表查询，如果只有一个null ，那就可以快速的定位出来

# 数据库的三范式是什么？

- 第一范式（1NF）
  字段具有原子性,不可再分。(所有关系型数据库系统都满足第一范式数据库表中的字段都是单一属性的，不可再分)

- 第二范式（2NF）

  是在第一范式（1NF）的基础上建立起来的，即满足第二范式（2NF）必须先满足第一范式（1NF）。要求数据库表中的每个实例或行必须可以被惟一地区分。通常需要为表加上一个列，以存储各个实例的惟一标识。这个惟一属性列被称为主关键字或主键。

- 第三范式（3NF）

  满足第三范式（3NF）必须先满足第二范式（2NF）。简而言之，第三范式（3NF）要求一个数据库表中不包含已在其它表中已包含的非主关键字信息。

  - 第三范式具有如下特征：
    - 每一列只有一个值 ；
    - 每一行都能区分；
    - 每一个表都不包含其他表已经包含的非主关键字信息。

# UNION与UNION ALL的区别？

- 如果使用UNION ALL，不会合并重复的记录行
- 效率 UNION ALL 高于 UNION

# MqSQL关联查询有哪些？
**交叉连接（CROSS JOIN）**

```sql
SELECT * FROM A,B(,C)或者SELECT * FROM A CROSS JOIN B (CROSS JOIN C)
```

没有任何关联条件，结果是笛卡尔积，结果集会很大，没有意义

**内连接（INNER JOIN）**

- 等值连接：ON A.id=B.id
- 不等值连接：ON A.id > B.id
- 自连接：SELECT * FROM A T1 INNER JOIN A T2 ON T1.id=T2.pid

**外连接（LEFT JOIN/RIGHT JOIN）**

- 左外连接：

  LEFT OUTER JOIN, 以左表为主，先查询出左表，按照ON后的关联条件匹配右表，没有匹配到的用NULL填充，可以简写成LEFT JOIN

- 右外连接：

  RIGHT OUTER JOIN, 以右表为主，先查询出右表，按照ON后的关联条件匹配左表，没有匹配到的用NULL填充，可以简写成RIGHT JOIN

**联合查询（UNION与UNION ALL）**

```sql
SELECT * FROM A UNION SELECT * FROM B UNION ...
```




- 就是把多个结果集集中在一起，UNION前的结果为基准，需要注意的是联合查询的列数要相等，相同的记录行会合并
- 如果使用UNION ALL，不会合并重复的记录行
- 效率 UNION ALL 高于 UNION

**全连接（FULL JOIN）**

MySQL不支持

可以使用LEFT JOIN 和UNION和RIGHT JOIN联合使用

```sql
SELECT * FROM A LEFT JOIN B ON A.id=B.id UNIONSELECT * FROM A RIGHT JOIN B ON A.id=B.id
```

# 【SELECT *】 和【SELECT 全部字段】的 2 种写法有何优缺点?

1. 前者要解析数据字典，后者不需要；
2. 结果输出顺序，前者与建表列顺序相同，后者按指定字段顺序；
3. 表字段改名，前者不需要修改，后者需要改；
4. 后者可以建立索引进行优化，前者无法优化；
5. 后者的可读性比前者要高。

# HAVNG 子句 和 WHERE 的异同点?
- 语法上

  where 用表中列名，

  having 用 select 结果别名；

- 影响结果范围

  where 从表读出数据的行数，

  having 返回客户端的行数；

- 索引

  where 可以使用索引，

  having 不能使用索引，只能在临时结果集操作；

- where 后面不能使用聚集函数，having 是专门使用聚集函数的。

# MySQL支持哪些数据类型？

| **分类**             | **类型名称** | **说明**                                                     |
| -------------------- | ------------ | ------------------------------------------------------------ |
| **整数类型**         | tinyInt      | 很小的整数(8位二进制)                                        |
|                      | smallint     | 小的整数(16位二进制)                                         |
|                      | mediumint    | 中等大小的整数(24位二进制)                                   |
|                      | int(integer) | 普通大小的整数(32位二进制)                                   |
| **小数类型**         | float        | 单精度浮点数                                                 |
|                      | double       | 双精度浮点数                                                 |
|                      | decimal(m,d) | 压缩严格的定点数                                             |
| **日期类型**         | year         | YYYY 1901~2155                                               |
|                      | time         | HH:MM:SS -838:59:59~838:59:59                                |
|                      | date         | YYYY-MM-DD 1000-01-01~9999-12-3                              |
|                      | datetime     | YYYY-MM-DD HH:MM:SS 1000-01-01 00:00:00~ 9999-12-31 23:59:59 |
|                      | timestamp    | YYYY-MM-DD HH:MM:SS 19700101 00:00:01 UTC~2038-01-19 03:14:07UTC |
| **文本、二进制类型** | CHAR(M)      | M为0~255之间的整数                                           |
|                      | VARCHAR(M)   | M为0~65535之间的整数                                         |
|                      | TINYBLOB     | 允许长度0~255字节                                            |
|                      | BLOB         | 允许长度0~65535字节                                          |
|                      | MEDIUMBLOB   | 允许长度0~167772150字节                                      |
|                      | LONGBLOB     | 允许长度0~4294967295字节                                     |
|                      | TINYTEXT     | 允许长度0~255字节                                            |
|                      | TEXT         | 允许长度0~65535字节                                          |
|                      | MEDIUMTEXT   | 允许长度0~167772150字节                                      |
|                      | LONGTEXT     | 允许长度0~4294967295字节                                     |
|                      | VARBINARY(M) | 允许长度0~M个字节的变长字节字符串                            |
|                      | BINARY(M)    | 允许长度0~M个字节的定长字节字符串                            |

# 数据库的乐观锁和悲观锁是什么？该如何选择？

数据库管理系统（DBMS）中的并发控制的任务是确保在多个事务同时存取数据库中同一数据时不破坏事务的隔离性和统一性以及数据库的统一性。乐观并发控制（乐观锁）和悲观并发控制（悲观锁）是并发控制主要采用的技术手段。

- **悲观锁**：
  假定会发生并发冲突，屏蔽一切可能违反数据完整性的操作。在查询完数据的时候就把事务锁起来，直到提交事务。实现方式：使用数据库中的锁机制    for update

- **乐观锁**：
  假设不会发生并发冲突，只在提交操作时检查是否违反数据完整性。在修改数据的时候把事务锁起来，通过version的方式来进行锁定。实现方式：乐一般会使用版本号机制或CAS算法实现。

**两种锁的使用场景**

从上面对两种锁的介绍，我们知道两种锁各有优缺点，不可认为一种好于另一种，像**乐观锁适用于写比较少的情况下（多读场景）**，即冲突真的很少发生的时候，这样可以省去了锁的开销，加大了系统的整个吞吐量。

但如果是多写的情况，一般会经常产生冲突，这就会导致上层应用会不断的进行retry，这样反倒是降低了性能，所以**一般多写的场景下用悲观锁就比较合适。**