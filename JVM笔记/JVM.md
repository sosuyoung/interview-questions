# JVM

## JVM体系结构概述

<img src="/Users/wangyang/Library/Application Support/typora-user-images/image-20210401142544872.png" alt="image-20210401142544872" style="zoom:50%;" />

### JVM位置

JVM是运行在操作系统之上的，它与硬件没有直接的交互

### 类装载器ClassLoader

​		**负责加载class文件，class文件在文件开头有特定的文件标示（cafe babe），将class文件字节码内容加载到内存中，并将这些内容转换成方法区中的运行时数据结构并且ClassLoader只负责class文件的加载，至于它是否可以运行，则由Execution Engine执行引擎决定**

![](/Users/wangyang/Desktop/interview/JVM笔记/产业数据监控规则.png)

### 类装载器ClassLoader2

<img src="/Users/wangyang/Library/Application Support/typora-user-images/image-20210401134829469.png" alt="image-20210401134829469" style="zoom: 50%;" />

​	

#### 虚拟机自带的加载器

1. 启动类加载器----Bootstrap C++
2. 扩展类加载器----Extension  JAVA
3. 应用程序类加载-----AppClassLoader   Java也叫做系统类加载，加载当前应用的classpath的所有类

#### 用户自定义加载

​	Java.lang.ClassLoader的子类，用户可以定制类的加载方式

```java
public class Hello {
    public static void main(String[] args) {
        Object object = new Object();
        System.out.println(object.getClass().getClassLoader());

        Hello hello = new Hello();
        System.out.println(hello.getClass().getClassLoader());
        System.out.println(hello.getClass().getClassLoader().getParent());
        System.out.println(hello.getClass().getClassLoader().getParent().getParent());
    }
}
```

### 类装载器ClassLoader3

#### 双亲委派机制

​		在加载一个类的时候，是不允许从底层加载的，要从最上面加载，从Bootstrap开始查找，到Extension，最后从AppClassLoader中找。

![](/Users/wangyang/Desktop/interview/JVM笔记/91c73ed9ff0be6389ee51c077c1d7088.png)

​		例如，新建一个java.Lang.package.String类，但是加载类的时候是需要从Bootstrap开始进行装载的，这个时候在找到自己新建的String类之前，已经找到了存在的String类所以，新建的String是不会被加载的。

​		当一个类收到了类加载的请求，当首先不会尝试自己去加载这个类，而是把这个请求委派给父类去完成，每一层次类加载器都是如此，因此所有的加载请求都应该传送到启动类加载器中，只有当父类加载器反馈自己无法完成这个请求的时候（在它的加载路径下没有找到所需要的Class），子类加载器才会尝试自己去加载。

​		采用双亲委派机制的一个好处是比如加载位于rt.jar包中的类java.lang.Object，不管是那个加载器加载这个类，最终都是委托给顶层的启动类加载器进行加载，这样就保证了使用不同的类加载器最终到的的都是同样一个Object对象。

#### 沙箱安全机制？

​		沙箱机制就是将Java代码限定在JVM特定的运行范围中，并严格限制代码对本地系统资源的访问，以此来保证对代码的有效隔离、防止对本地系统造成破坏。不同级别的沙箱对本地系统资源的访问限制不同（CPU、内存、文件系统、网络）

## Execution Engine 执行引擎

### 执行引擎负责解释命令，提交操作系统执行



## Native

如：

```java
public static void main(String[] args) {
        Thread t1 = new Thread();
        t1.start();
    }


 public synchronized void start() {
        /**
         * This method is not invoked for the main method thread or "system"
         * group threads created/set up by the VM. Any new functionality added
         * to this method in the future may have to also be added to the VM.
         *
         * A zero status value corresponds to state "NEW".
         */
        if (threadStatus != 0)
            throw new IllegalThreadStateException();

        /* Notify the group that this thread is about to be started
         * so that it can be added to the group's list of threads
         * and the group's unstarted count can be decremented. */
        group.add(this);

        boolean started = false;
        try {
            start0();
            started = true;
        } finally {
            try {
                if (!started) {
                    group.threadStartFailed(this);
                }
            } catch (Throwable ignore) {
                /* do nothing. If start0 threw a Throwable then
                  it will be passed up the call stack */
            }
        }
    }

    private native void start0();
```

按照线程的start()原理来讲，java仅仅支持到start()方法，但是调用的private native void start0()指的是java需要调用操作系统底层的C语言函数库来执行

### Native Method Stack

​	它的具体做法是Native Method Stack中登记native方法，在Execution Engine执行时加载本地方法库

### Native Inteface

​	本地接口的作用是融合不同的变成语言为Java作用，她的初衷是融合C/C++程序，Java是C/C++横行的时候，要想立足，必须调用C/C++程序，于是就在内存中专门开辟了一块区域处理标记为native的代码，它的具体做法是Native  Method Stack中登记native方法，在Execution Engine执行时加载native libraies

​		目前该方法使用的越来越少了，除非是与硬件有关的应用，比如通过Java程序驱动打印机或者Java系统管理生产设备，在企业级应用中已经比较少见，因为显得异构领域间的通信很发达，比如可以使用Socket通信，也可以使用Web Service等等



### Program Counter Register(程序计数器)PC寄存器

​	每个线程都有一个程序计数器，是线程私有的，就是一个指针，指向方法区中的方法字节码（用来存储执行下一条指令的地址，也即将要执行的指令代码），有执行引擎读取下一条指令，是一个非常小的内存空间，几乎可以忽略不记，这块内存区域很小，它是当前线程所执行的字节码的型号指示器，字节码解释器通过改变这个计数器的值来选取下一条需要执行的字节码指令，如果执行的是一个Native方法，那么这个计数器是空的，用以完成分支、循环、跳转、异常处理、线程恢复等基础功能，不会发生OOM（OutOfMemory）错误

## 1. JVM基础

### 1.2 堆和栈的区别

在计算机编程和内存管理中，"堆"（Heap）和"栈"（Stack）是两种用于存储和管理数据的不同内存区域，它们具有以下区别：

1. **内存分配方式**：
   - 堆：堆是用于动态内存分配的区域。在堆中分配内存需要手动申请和释放，通常由程序员负责管理。堆的内存分配是比较灵活的，但容易导致内存泄漏或垃圾数据的问题，因为需要手动释放分配的内存。
   - 栈：栈是用于静态内存分配的区域。在栈中分配内存是自动的，由编译器和程序运行时系统负责管理。栈的内存分配和释放是非常高效的，但它的大小通常有限，且生命周期受限于函数调用的范围。
2. **数据类型**：
   - 堆：堆中通常存储动态分配的数据对象，如对象实例、数组等。数据对象的生命周期可以很长，取决于程序员的管理。
   - 栈：栈中通常存储基本数据类型和引用（指针），如整数、字符、指针等。栈中的数据对象生命周期短暂，通常随着函数的调用结束而销毁。
3. **内存管理**：
   - 堆：堆的内存管理需要手动分配和释放内存，程序员需要调用`malloc`、`new`等分配内存，以及`free`、`delete`等释放内存。如果不谨慎管理，可能导致内存泄漏。
   - 栈：栈的内存管理由编译器和运行时系统自动完成，不需要手动干预。栈上的内存会在函数调用结束时自动释放。
4. **空间大小和速度**：
   - 堆：堆的大小通常比较大，但内存分配和释放的速度较慢，因为需要在堆中查找合适的内存块并进行管理。
   - 栈：栈的大小通常较小，但内存分配和释放非常快速，因为只需要简单地移动栈指针即可。
5. **适用场景**：
   - 堆：适用于需要动态分配和管理大量数据，生命周期不确定或较长的情况，如动态创建对象、大型数据结构等。
   - 栈：适用于生命周期短暂、大小确定、局部作用域的数据，如函数参数、局部变量等。

总的来说，堆和栈都是内存管理中重要的概念，它们各自适用于不同的情况。了解它们的区别和使用场景有助于编写更高效、可维护和可靠的程序。

### 1.4 队列和栈是什么？有什么区别？

队列和栈都是被用来预存储数据的。

- 操作的名称不同。队列的插入称为入队，队列的删除称为出队。栈的插入称为进栈，栈的删除称为出栈。
- 可操作的方式不同。队列是在队尾入队，队头出队，即两边都可操作。而栈的进栈和出栈都是在栈顶进行的，无法对栈底直接进行操作。
- 操作的方法不同。队列是先进先出（FIFO），即队列的修改是依先进先出的原则进行的。新来的成员总是加入队尾（不能从中间插入），每次离开的成员总是队列头上（不允许中途离队）。而栈为后进先出（LIFO）,即每次删除（出栈）的总是当前栈中最新的元素，即最后插入（进栈）的元素，而最先插入的被放在栈的底部，要到最后才能删除。

### 1.5 JVM内存模型是什么？

java 内存模型(JMM)是线程间通信的控制机制.JMM 定义了主内存和线程之间抽象关系。线程之间的共享变量存储在主内存（main memory）中，每个线程都有一个私有的本地内存（local memory），本地内存中存储了该线程以读/写共享变量的副本。本地内存是 JMM 的一个抽象概念，并不真实存在。它涵盖了缓存，写缓冲区，寄存器以及其他的硬件和编译器优化。		

线程 A 与线程 B 之间如要通信的话，必须要经历下面 2 个步骤：

1. 首先，线程 A 把本地内存 A 中更新过的共享变量刷新到主内存中去。
2. 然后，线程 B 到主内存中去读取线程 A 之前已更新过的共享变量。

### 1.6 finalize()方法什么时候被调用？析构函数(finalization)的目的是什么？
**垃圾回收器（garbage colector）决定回收某对象时，就会运行该对象的finalize()方法**
finalize是Object类的一个方法，该方法在Object类中的声明protected void finalize() throws Throwable { }
在垃圾回收器执行时会调用被回收对象的finalize()方法，可以覆盖此方法来实现对其资源的回收。注意：一旦垃圾回收器准备释放对象占用的内存，将首先调用该对象的finalize()方法，并且下一次垃圾回收动作发生时，才真正回收对象占用的内存空间

GC本来就是内存回收了，应用还需要在finalization做什么呢？ 答案是大部分时候，什么都不用做(也就是不需要重载)。只有在某些很特殊的情况下，比如你调用了一些native的方法(一般是C写的)，可以要在finaliztion里去调用C的释放函数。

### 1.7 对象的访问定位有几种方式？
Java程序需要通过 JVM 栈上的引用访问堆中的具体对象。对象的访问方式取决于 JVM 虚拟机的实现。目前主流的访问方式有 句柄 和 直接指针 两种方式。

- 句柄：
  可以理解为指向指针的指针，维护着对象的指针。句柄不直接指向对象，而是指向对象的指针（句柄不发生变化，指向固定内存地址），再由对象的指针指向对象的真实内存地址。

![](/Users/wangyang/Desktop/interview/JVM笔记/82efb7d0a9f329fbb22aec1002a3ac60.png)

**优势**：引用中存储的是**稳定**的句柄地址，在对象被移动（垃圾收集时移动对象是非常普遍的行为）时只会改变**句柄中**的**实例数据指针**，而**引用**本身不需要修改。

- 直接指针：
  指向对象，代表一个对象在内存中的起始地址。

![](/Users/wangyang/Desktop/interview/JVM笔记/8cc2f060d19ee1b7d081c5af8004f800.png)

**优势**：速度更**快**，节省了**一次指针定位**的时间开销。由于对象的访问在`Java`中非常频繁，因此这类开销积少成多后也是非常可观的执行成本。HotSpot 中采用的就是这种方式。

### 1.9 引用的分类有几种？

- 强引用：GC时不会被回收
- 软引用：描述有用但不是必须的对象，在发生内存溢出异常之前被回收
- 弱引用：描述有用但不是必须的对象，在下一次GC时被回收
- 虚引用（幽灵引用/幻影引用）:无法通过虚引用获得对象，用PhantomReference实现虚引用，虚引用用来在GC时返回一个通知。

### 1.10 Java会存在内存泄漏吗？请简单描述

内存泄漏是指不再被使用的对象或者变量一直被占据在内存中。理论上来说，Java是有GC垃圾回收机制的，也就是说，不再被使用的对象，会被GC自动回收掉，自动从内存中清除。

但是，即使这样，Java也还是存在着内存泄漏的情况，java导致内存泄露的原因很明确：长生命周期的对象持有短生命周期对象的引用就很可能发生内存泄露，尽管短生命周期对象已经不再需要，但是因为长生命周期对象持有它的引用而导致不能被回收，这就是java中内存泄露的发生场景。

## 2. JVM垃圾回收

### 2.1 简述 java 垃圾回收机制?

在 java 中，程序员是不需要显示的去释放一个对象的内存的，而是由虚拟机自行执行。在JVM 中，有一个垃圾回收线程，它是低优先级的，在正常情况下是不会执行的，只有在虚拟机空闲或者当前堆内存不足时，才会触发执行，扫面那些没有被任何引用的对象，并将它们添加到要回收的集合中，进行回收。

### 2.2 GC是什么？为什么要GC？

GC是垃圾收集的意思，内存处理是编程人员容易出现问题的地方，忘记或者错误的内存回收会导致程序或系统的不稳定甚至崩溃，Java提供的GC功能可以自动监测对象是否超过作用域从而达到自动回收内存的目的，Java语言没有提供释放已分配内存的显示操作方法。Java程序员不用担心内存管理，因为垃圾收集器会自动进行管理。要请求垃圾收集，可以调用下面的方法之一：System.gc() 或Runtime.getRuntime().gc() ，但JVM可以屏蔽掉显示的垃圾回收调用。 垃圾回收可以有效的防止内存泄露，有效的使用可以使用的内存。垃圾回收器通常是作为一个单独的低优先级的线程运行，不可预知的情况下对内存堆中已经死亡的或者长时间没有使用的对象进行清除和回收，程序员不能实时的调用垃圾回收器对某个对象或所有对象进行垃圾回收。在Java诞生初期，垃圾回收是Java最大的亮点之一，因为服务器端的编程需要有效的防止内存泄露问题，然而时过境迁，如今Java的垃圾回收机制已经成为被诟病的东西。移动智能终端用户通常觉得iOS的系统比Android系统有更好的用户体验，其中一个深层次的原因就在于android系统中垃圾回收的不可预知性。

补充：垃圾回收机制有很多种，包括：分代复制垃圾回收、标记垃圾回收、增量垃圾回收等方式。标准的Java进程既有栈又有堆。栈保存了原始型局部变量，堆保存了要创建的对象。Java平台对堆内存回收和再利用的基本算法被称为标记和清除，但是Java对其进行了改进，采用“分代式垃圾收集”。这种方法会跟Java对象的生命周期将堆内存划分为不同的区域，在垃圾收集过程中，可能会将对象移动到不同区域：

- 伊甸园（Eden）：这是对象最初诞生的区域，并且对大多数对象来说，这里是它们唯一存在过的区域。
- 幸存者乐园（Survivor）：从伊甸园幸存下来的对象会被挪到这里。
- 终身颐养园（Tenured）：这是足够老的幸存对象的归宿。年轻代收集（Minor-GC）过程是不会触及这个地方的。当年轻代收集不能把对象放进终身颐养园时，就会触发一次完全收集（Major-GC），这里可能还会牵扯到压缩，以便为大对象腾出足够的空间。 

与垃圾回收相关的JVM参数：

```shell
-Xms / -Xmx：堆的初始大小 / 堆的最大大小

-Xmn：堆中年轻代的大小

-XX:-DisableExplicitGC：让System.gc()不产生任何作用

-XX:+PrintGCDetails：打印GC的细节

-XX:+PrintGCDateStamps：打印GC操作的时间戳

-XX:NewSize / XX:MaxNewSize： 设置新生代大小/新生代最大大小

-XX:NewRatio ：可以设置老生代和新生代的比例

-XX:PrintTenuringDistribution ：设置每次新生代GC后输出幸存者乐园中对象年龄的分布

-XX:InitialTenuringThreshold / -XX:MaxTenuringThreshold：设置老年代阀值的初始值和最大值

-XX:TargetSurvivorRatio：设置幸存区的目标使用率
```

### 2.3 垃圾回收的优点有那些？

- java语言最显著的特点就是引入了垃圾回收机制，它使java程序员在编写程序时不再考虑内存管理的问题。
- 由于有这个垃圾回收机制，java中的对象不再有“作用域”的概念，只有引用的对象才有“作用域”。
- 垃圾回收机制有效的防止了内存泄露，可以有效的使用可使用的内存。
- 垃圾回收器通常作为一个单独的低级别的线程运行，在不可预知的情况下对内存堆中已经死亡的或很长时间没有用过的对象进行清除和回收。

### 2.6 垃圾回收器可以马上回收内存吗？

**不会；**通常，GC采用有向图的方式记录和管理堆(heap)中的所有对象。通过这种方式确定哪些对象是"可达的"，哪些对象是"不可达的"。当GC确定一些对象为"不可达"时，GC就有责任回收这些内存空间。而这个回收操作时达到一定阈值或者条件之后才会触发回收；并不是实时的。

### 2.9 如何判断一个对象是否存活?(或者 GC 对象的判定方法)

判断一个对象是否存活有两种方法:

**引用计数法**
所谓引用计数法就是给每一个对象设置一个引用计数器，每当有一个地方引用这个对象时，就将计数器加一，引用失效时，计数器就减一。当一个对象的引用计数器为零时，说明此对象没有被引用，也就是“死对象”,将会被垃圾回收.

引用计数法有一个缺陷就是无法解决循环引用问题，也就是说当对象 A 引用对象 B，对象 B 又引用者对象 A，那么此时 A,B 对象的引用计数器都不为零，也就造成无法完成垃圾回收，所以主流的虚拟机都没有采用这种算法。

**可达性算法(引用链法)**
该算法的思想是：从一个被称为 GC Roots 的对象开始向下搜索，如果一个对象到 GC Roots 没有任何引用链相连时，则说明此对象不可用。在 java 中可以作为 GC Roots 的对象有以下几种:

- 虚拟机栈中引用的对象
- 方法区类静态属性引用的对象
- 方法区常量池引用的对象
- 本地方法栈 JNI 引用的对象

虽然这些算法可以判定一个对象是否能被回收，但是当满足上述条件时，一个对象不一定会被回收。当一个对象不可达 GC Root 时，这个对象并不会立马被回收，而是出于一个死缓的阶段，若要被真正的回收需要经历两次标记

如果对象在可达性分析中没有与 GC Root 的引用链，那么此时就会被第一次标记并且进行一次筛选，筛选的条件是是否有必要执行 finalize()方法。当对象没有覆盖 finalize()方法或者已被虚拟机调用过，那么就认为是没必要的。如果该对象有必要执行 finalize()方法，那么这个对象将会放在一个称为 F-Queue 的对队列中，虚拟机会触发一个 Finalize()线程去执行，此线程是低优先级的，并且虚拟机不会承诺一直等待它运行完，这是因为如果 finalize()执行缓慢或者发生了死锁，那么就会造成 F-Queue 队列一直等待，造成了内存回收系统的崩溃。GC 对处于 F-Queue 中的对象进行第二次被标记，这时，该对象将被移除”即将回收”集合，等待回收。

### 2.12 JVM 有哪些垃圾回收算法？

- 标记-清除:
  这是垃圾收集算法中最基础的，根据名字就可以知道，它的思想就是标记哪些要被回收的对象，然后统一回收。这种方法很简单，但是会有两个主要问题：1.效率不高，标记和清除的效率都很低；2.会产生大量不连续的内存碎片，导致以后程序在分配较大的对象时，由于没有充足的连续内存而提前触发一次 GC 动作。

<img src="/Users/wangyang/Desktop/interview/JVM笔记/13d9c46b69573f870d3b747c7bf0e397.png" style="zoom:67%;" />

- 复制算法:
  为了解决效率问题，复制算法将可用内存按容量划分为相等的两部分，然后每次只使用其中的一块，当一块内存用完时，就将还存活的对象复制到第二块内存上，然后一次性清楚完第一块内存，再将第二块上的对象复制到第一块。但是这种方式，内存的代价太高，每次基本上都要浪费一般的内存。
  于是将该算法进行了改进，内存区域不再是按照 1：1 去划分，而是将内存划分为 8:1:1 三部分，较大那份内存交 Eden 区，其余是两块较小的内存区叫 Survior 区。每次都会优先使用 Eden 区，若 Eden 区满，就将对象复制到第二块内存区上，然后清除 Eden 区，如果此时存活的对象太多，以至于 Survivor 不够时，会将这些对象通过分配担保机制复制到老年代中。(java 堆又分为新生代和老年代)

<img src="/Users/wangyang/Desktop/interview/JVM笔记/9a2ca399919f2a5960be3c03705b1907.jpeg" style="zoom:67%;" />

- 标记-整理
  该算法主要是为了解决标记-清除，产生大量内存碎片的问题；当对象存活率较高时，也解决了复制算法的效率问题。它的不同之处就是在清除对象的时候现将可回收对象移动到一端，然后清除掉端边界以外的对象，这样就不会产生内存碎片了。

<img src="/Users/wangyang/Desktop/interview/JVM笔记/15d8596ad236a4cc2e832405e30acf12.jpeg" style="zoom:67%;" />

- 分代收集
  现在的虚拟机垃圾收集大多采用这种方式，它根据对象的生存周期，将堆分为新生代和老年代。在新生代中，由于对象生存期短，每次回收都会有大量对象死去，那么这时就采用复制算法。老年代里的对象存活率较高，没有额外的空间进行分配担保，所以可以使用标记-整理 或者 标记-清除。

![](/Users/wangyang/Desktop/interview/JVM笔记/540a906b87f08c4d1211a50c8e67d53c.jpeg)

### 2.17 JVM 有哪些垃圾回收器？

如果说垃圾收集算法是内存回收的方法论，那么垃圾收集器就是内存回收的具体实现。下图展示了7种作用于不同分代的收集器，其中用于回收新生代的收集器包括Serial、PraNew、Parallel Scavenge，回收老年代的收集器包括Serial Old、Parallel Old、CMS，还有用于回收整个Java堆的G1收集器。不同收集器之间的连线表示它们可以搭配使用。

<img src="/Users/wangyang/Desktop/interview/JVM笔记/f77fac61b07bfc2f668ceca4de207c4b.jpeg" style="zoom:67%;" />

- **Serial收集器（复制算法):**
  新生代单线程收集器，标记和清理都是单线程，优点是简单高效；
- **ParNew收集器 (复制算法):**
  新生代收并行集器，实际上是Serial收集器的多线程版本，在多核CPU环境下有着比Serial更好的表现；
- **Parallel Scavenge收集器 (复制算法):**
  新生代并行收集器，追求高吞吐量，高效利用 CPU。吞吐量 = 用户线程时间/(用户线程时间+GC线程时间)，高吞吐量可以高效率的利用CPU时间，尽快完成程序的运算任务，适合后台应用等对交互相应要求不高的场景；
- **Serial Old收集器 (标记-整理算法):**
  老年代单线程收集器，Serial收集器的老年代版本；
- **Parallel Old收集器 (标记-整理算法)：**
  老年代并行收集器，吞吐量优先，Parallel Scavenge收集器的老年代版本；
- **CMS(Concurrent Mark Sweep)收集器（标记-清除算法）：**
  老年代并行收集器，以获取最短回收停顿时间为目标的收集器，具有高并发、低停顿的特点，追求最短GC回收停顿时间。
- **G1(Garbage First)收集器 (标记-整理算法)：**
  Java堆并行收集器，G1收集器是JDK1.7提供的一个新收集器，G1收集器基于“标记-整理”算法实现，也就是说不会产生内存碎片。此外，G1收集器不同于之前的收集器的一个重要特点是：G1回收的范围是整个Java堆(包括新生代，老年代)，而前六种收集器回收的范围仅限于新生代或老年代。

### 2.18 详细介绍一下 CMS 垃圾回收器？

CMS 是英文 Concurrent Mark-Sweep 的简称，是以牺牲吞吐量为代价来获得最短回收停顿时间的垃圾回收器。对于要求服务器响应速度的应用上，这种垃圾回收器非常适合。在启动 JVM 的参数加上“-XX:+UseConcMarkSweepGC”来指定使用 CMS 垃圾回收器。

CMS 使用的是标记-清除的算法实现的，所以在 gc 的时候回产生大量的内存碎片，当剩余内存不能满足程序运行要求时，系统将会出现 Concurrent Mode Failure，临时 CMS 会采用 Serial Old 回收器进行垃圾清除，此时的性能将会被降低。

### 2.20 简述分代垃圾回收器是怎么工作的？

分代回收器有两个分区：老生代和新生代，新生代默认的空间占比总空间的 1/3，老生代的默认占比是 2/3。

新生代使用的是复制算法，新生代里有 3 个分区：Eden、To Survivor、From Survivor，它们的默认占比是 8:1:1，它的执行流程如下：

- 把 Eden + From Survivor 存活的对象放入 To Survivor 区；
- 清空 Eden 和 From Survivor 分区；
- From Survivor 和 To Survivor 分区交换，From Survivor 变 To Survivor，To Survivor 变 From Survivor。

每次在 From Survivor 到 To Survivor 移动时都存活的对象，年龄就 +1，当年龄到达 15（默认配置是 15）时，升级为老生代。大对象也会直接进入老生代。

老生代当空间占用到达某个值之后就会触发全局垃圾收回，一般使用标记整理的执行算法。以上这些循环往复就构成了整个分代垃圾回收的整体执行流程。

## 3. JVM类加载

### 3.1 JVM类加载过程？

类从被加载到虚拟机内存开始，到卸载出内存为止，整个生命周期包括：加载，验证，准备，解析，初始化，使用和卸载

**加载**
加载是类加载过程的一个阶段，在加载阶段虚拟机需要完成三件事

- 通过一个类的全限定名来获取定义此类的辅而进之字节流
- 将字节流所代表的的静态存储结构转化为方法区的运行时数据结构
- 在内存中生成一个代表这个类的java.lang.Class对象，作为方法区这个类的各种数据的访问入口

**验证**

验证就是确保Class文件的字节流中包含的信息符合当前虚拟机的要求，并且不会危害虚拟机自身的安全

- 文件格式验证
  验证字节流是否符合Class文件格式的规范
- 元数据验证
  对字节码描述的信息进行语义分析（注意：对比javac编译阶段的语义分析），以保证其描述的信息符合Java语言规范的要求
- 字节码验证
  通过数据流和控制流分析，确定程序语义是合法的、符合逻辑的
- 符号引用验证
  确保解析动作能正确执行。

**准备**

准备阶段是正式为类静态变量分配内存并设置类变量初始值的阶段，这些变量所使用的内存都在方法区中进行分配

```java
public static int value = 123 //在准备阶段 value的值是 0 并不是123
public static final int value = 123 // 准备阶段value 的值为123
```

如果属性有Constant Value 属性，那么在准备阶段变量就会被初始化为所指定的值

这时候进行内存分配的仅包括类变量（static），而不包括实例变量，实例变量会在对象实例化时随着对象一块分配在Java堆中

这里所设置的初始值通常情况下是数据类型默认的零值（如0、0L、null、false等），而不是被在Java代码中被显式地赋予的值

**解析**

解析阶段是虚拟机将常量池内的符号引用替换为直接引用的过程

- 直接引用
  直接引用可以使直接指向目标的指针，相对偏移量或是一个能间接定位到目标的句柄
- 符号引用
  符号引用以一组符号来描述所引用的目标，符号可以是任何形式的字面量，只要使用时能无歧义的定位到目标即可

主要包含：

- 类或接口的解析
- 字段解析
- 类方法解析
- 接口方法解析

**初始化**

初始化，为类的静态变量赋予正确的初始值，JVM负责对类进行初始化，主要对类变量进行初始化。在Java中对类变量进行初始值设定有两种方式：

- 声明类变量是指定初始值
- 使用静态代码块为类变量指定初始值

JVM初始化步骤：

- 假如这个类还没有被加载和连接，则程序先加载并连接该类
- 假如该类的直接父类还没有被初始化，则先初始化其直接父类
- 假如类中有初始化语句，则系统依次执行这些初始化语句

类的初始化

- 创建类的实例，也就是new的方式
- 访问某个类或接口的静态变量，或者对该静态变量赋值
- 调用类的静态方法
- 反射（如Class.forName(“com.shengsiyuan.Test”)）
- 初始化某个类的子类，则其父类也会被初始化
- Java虚拟机启动时被标明为启动类的类（Java Test），直接使用java.exe命令来运行某个主类

**卸载**

- 执行了 System.exit()方法
- 程序正常执行结束
- 程序在执行过程中遇到了异常或错误而异常终止
- 由于操作系统出现错误而导致Java虚拟机进程终止

### 3.3 JVM加载Class文件的原理机制是什么？

Java中的所有类，都需要由类加载器装载到JVM中才能运行。类加载器本身也是一个类，而它的工作就是把class文件从硬盘读取到内存中。在写程序的时候，我们几乎不需要关心类的加载，因为这些都是隐式装载的，除非我们有特殊的用法，像是反射，就需要显式的加载所需要的类。

类装载方式，有两种 ：

- 隐式装载
  程序在运行过程中当碰到通过new 等方式生成对象时，隐式调用类装载器加载对应的类到jvm中，
- 显式装载，
  通过class.forname()等方法，显式加载需要的类

Java类的加载是动态的，它并不会一次性将所有类全部加载后再运行，而是保证程序运行的基础类(像是基类)完全加载到jvm中，至于其他类，则在需要的时候才加载。这当然就是为了节省内存开销。

### 3.4 什么是类加载器，类加载器有哪些?

实现通过类的权限定名获取该类的二进制字节流的代码块叫做类加载器。

主要有一下四种类加载器:

- **启动类加载器(Bootstrap ClassLoader)用来加载java核心类库，无法被java程序直接引用。**
- **扩展类加载器(extensions class loader):它用来加载 Java 的扩展库。Java 虚拟机的实现会提供一个扩展库目录。该类加载器在此目录里面查找并加载 Java 类。**
- **系统类加载器（system class loader）：它根据 Java 应用的类路径（CLASSPATH）来加载 Java 类。一般来说，Java 应用的类都是由它来完成加载的。可以通过 ClassLoader.getSystemClassLoader()来获取它。**
- **用户自定义类加载器，通过继承 java.lang.ClassLoader类的方式实现。**

## 4. JVM调优

### 4.2 用过那些JVM 调优的工具？

常用调优工具分为两类,jdk自带监控工具：jconsole和jvisualvm，第三方有：MAT(Memory Analyzer Tool)、GChisto。

- **jconsole：**
  Java Monitoring and Management Console是从java5开始，在JDK中自带的java监控和管理控制台，用于对JVM中内存，线程和类等的监控
- **jvisualvm：**
  jdk自带全能工具，可以分析内存快照、线程快照；监控内存变化、GC变化等。
- **MAT：**
  Memory Analyzer Tool，一个基于Eclipse的内存分析工具，是一个快速、功能丰富的Java heap分析工具，它可以帮助我们查找内存泄漏和减少内存消耗
- **GChisto：**
  一款专业分析gc日志的工具

### 4.3 常用的 JVM 调优的参数都有哪些？

- **-Xms2g：**
  初始化推大小为 2g；
- **-Xmx2g：**
  堆最大内存为 2g；
- **-XX:NewRatio=4：**
  设置年轻的和老年代的内存比例为 1:4；
- **-XX:SurvivorRatio=8：**
  设置新生代 Eden 和 Survivor 比例为 8:2；
- **–XX:+UseParNewGC：**
  指定使用 ParNew + Serial Old 垃圾回收器组合；
- **-XX:+UseParallelOldGC：**
  指定使用 ParNew + ParNew Old 垃圾回收器组合；
- **-XX:+UseConcMarkSweepGC：**
  指定使用 CMS + Serial Old 垃圾回收器组合；
- **-XX:+PrintGC：**
  开启打印 gc 信息；
- **-XX:+PrintGCDetails：**
  打印 gc 详细信息。

### 4.5 你知道哪些JVM性能调优方式有那些？
- 设定堆内存大小
  -Xmx：堆内存最大限制。

- 设定新生代大小。 新生代不宜太小，否则会有大量对象涌入老年代
  -XX:NewSize：新生代大小

  -XX:NewRatio 新生代和老生代占比

  -XX:SurvivorRatio：伊甸园空间和幸存者空间的占比

- 设定垃圾回收器
  年轻代用 -XX:+UseParNewGC 年老代用-XX:+UseConcMarkSweepGC

# 5 JVM调优思路

- 常规JVM调优：当 Java 线上出现问题，如 CPU 飙升、负载突高、内存溢出等问题，需要查命令，查网络，然后 top、jps、jstat、jstack、jmap、jhat、hprof 等操作。
- 对于OOM Erro dump转储文件、GC日志进行分析，解决问题
- 使用 Arthas 轻松定位，图形化界面、迅速解决，及时止损。https://blog.csdn.net/u013735734/article/details/102930307
- 监控： arthas / jconsole / jvisualVM / Jprofiler（最好用）
- 线上查找 （ cmdline || arthas ）
- jstat -gc 动态观察gc情况 stat -gc 4655 500 : 每个500个毫秒打印GC的情况

**查看运行的 java 进程信息 （top、jps ）**
$ jps -mlvV

$ jps -mlvV | grep [xxx]

top 、 top -Hp pid

```sh
top 查看所有进程的 CPU、内存状态

PID USER PR NI VIRT RES SHR S %CPU %MEM TIME+ COMMAND
7945 root 20 0 2813304 301908 14000 S 101.3 30.3 20:44.36 java

top -Hp pid 查看指定进程下所有线程的 CPU、内存状态

PID USER PR NI VIRT RES SHR S %CPU %MEM TIME+ COMMAND
7947 root 20 0 2813304 309196 14064 S 93.7 31.0 28:16.02 java
```



**查看GC汇总信息 （ jstat ）**
jstat -gcutil 进程ID

```sh
S0 S1 E O M CCS YGC YGCT FGC FGCT GCT
0.00 0.00 99.33 86.40 97.12 94.49 21 2.968 5 1.027 3.996

S0 ---> 年轻代中第一个survivor（幸存区）已使用的占当前容量百分比
S1 ---> 年轻代中第二个survivor（幸存区）已使用的占当前容量百分比
E ---> 年轻代中Eden（伊甸园）已使用的占当前容量百分比

O ---> old代已使用的占当前容量百分比
M ---> 元数据空间使用比例
CCS ---> 压缩使用比例

YGC ---> 从应用程序启动到采样时年轻代中gc次数
YGCT ---> 从应用程序启动到采样时年轻代中gc所用时间(s)
FGC ---> 从应用程序启动到采样时old代(全gc)gc次数
FGCT ---> 从应用程序启动到采样时old代(全gc)gc所用时间(s)
GCT ---> 从应用程序启动到采样时gc用的总时间(s)
```



jstat -gc 动态观察gc情况 stat -gc 4655 500 : 每个500个毫秒打印GC的情况

**进程、线程信息并进行分析 （jinfo 、jstack、jmap 、jhat ）**

- jinfo pid

进程和线程的 具体信息

- jstack


jstack 定位线程、进程状况

- jmap -histo 4655 | head -20


查找有多少对象产生

- jmap -dump:live,format=b,file=heap.bin pid


生成dump转储文件，线上系统，内存特别大，jmap执行期间会对进程产生很大影响，甚至卡顿（电商不适合）

- 使用MAT / jhat /jvisualvm 进行dump文件分析

  https://www.cnblogs.com/baihuitestsoftware/articles/6406271.html

- jhat -J-mx512M xxx.dump

  http://192.168.17.11:7000