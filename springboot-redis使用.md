### springboot使用Redis

准备工作做完后，接下来开始在Spring Boot项目里引入Redis：

```xml
<!-- spring-boot redis -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
```

在application.yml中配置Redis：

```yaml
spring:
  redis:
    # Redis数据库索引（默认为0）
    database: 0
    # Redis服务器地址
    host: localhost
    # Redis服务器连接端口
    port: 6379
    pool:
      # 连接池最大连接数（使用负值表示没有限制）
      max-active: 8
      # 连接池最大阻塞等待时间（使用负值表示没有限制）
      max-wait: -1
      # 连接池中的最大空闲连接
      max-idle: 8
      # 连接池中的最小空闲连接
      min-idle: 0
    # 连接超时时间（毫秒）
    timeout: 0
```

更多关于Spring Boot Redis配置可参考：https://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html# REDIS

接着创建一个Redis配置类：

```java
@Configuration
public class RedisConfig extends CachingConfigurerSupport {

    // 自定义缓存key生成策略
    @Bean
    public KeyGenerator keyGenerator() {
        return new KeyGenerator() {
            @Override
            public Object generate(Object target, java.lang.reflect.Method method, Object... params) {
                StringBuffer sb = new StringBuffer();
                sb.append(target.getClass().getName());
                sb.append(method.getName());
                for (Object obj : params) {
                    sb.append(obj.toString());
                }
                return sb.toString();
            }
        };
    }

    // 缓存管理器
    @Bean
    public CacheManager cacheManager(RedisConnectionFactory factory) {
        return RedisCacheManager
                .builder(factory)
                .cacheDefaults(RedisCacheConfiguration.defaultCacheConfig().entryTtl(Duration.ofMinutes(5)))
                .transactionAware()
                .build();
    }

    @Bean
    public RedisTemplate<String, String> redisTemplate(RedisConnectionFactory factory) {
        StringRedisTemplate template = new StringRedisTemplate(factory);
        setSerializer(template);// 设置序列化工具
        template.afterPropertiesSet();
        return template;
    }

    private void setSerializer(StringRedisTemplate template) {
        @SuppressWarnings({ "rawtypes", "unchecked" })
        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);
        template.setValueSerializer(jackson2JsonRedisSerializer);
    }
}
```

运行测试，控制台输出：

```shell
2017-11-17 18:17:06.995 DEBUG 8836 --- [main] c.s.m.StudentMapper.queryStudentBySno    : ==>  Preparing: select * from student where sno=? 
2017-11-17 18:17:07.128 DEBUG 8836 --- [main] c.s.m.StudentMapper.queryStudentBySno    : ==> Parameters: 001(String)
2017-11-17 18:17:07.152 DEBUG 8836 --- [main] c.s.m.StudentMapper.queryStudentBySno    : <==      Total: 1
学号001的学生姓名为：KangKang
学号001的学生姓名为：KangKang
```

第二次查询没有访问数据库，而是从缓存中获取的，在redis中查看该值：

```shell
127.0.0.1:6379>   keys *
1) "student~keys"
2) "001"
127.0.0.1:6379> get 001
"[\"com.springboot.bean.Student\",{\"sno\":\"001\",\"name\":\"KangKang\",\"sex\":\"M \"}]"
```

在测试方法中测试更新：

```java
@Test
public void test() throws Exception {
    Student student1 = this.studentService.queryStudentBySno("001");
    System.out.println("学号" + student1.getSno() + "的学生姓名为：" + student1.getName());
    
    student1.setName("康康");
    this.studentService.update(student1);
    
    Student student2 = this.studentService.queryStudentBySno("001");
    System.out.println("学号" + student2.getSno() + "的学生姓名为：" + student2.getName());
}
```

控制台输出：

```shell
学号001的学生姓名为：KangKang
2017-11-17 19:30:05.813  INFO 11244 --- [main] com.alibaba.druid.pool.DruidDataSource   : {dataSource-1} inited
2017-11-17 19:30:05.823 DEBUG 11244 --- [main] c.s.mapper.StudentMapper.update          : ==>  Preparing: update student set sname=?,ssex=? where sno=? 
2017-11-17 19:30:05.941 DEBUG 11244 --- [main] c.s.mapper.StudentMapper.update          : ==> Parameters: 康康(String), M (String), 001(String)
2017-11-17 19:30:05.953 DEBUG 11244 --- [main] c.s.mapper.StudentMapper.update          : <==    Updates: 1
2017-11-17 19:30:05.957 DEBUG 11244 --- [main] c.s.m.StudentMapper.queryStudentBySno    : ==>  Preparing: select * from student where sno=? 
2017-11-17 19:30:05.959 DEBUG 11244 --- [main] c.s.m.StudentMapper.queryStudentBySno    : ==> Parameters: 001(String)
2017-11-17 19:30:05.976 DEBUG 11244 --- [main] c.s.m.StudentMapper.queryStudentBySno    : <==      Total: 1
学号001的学生姓名为：康康
```

在redis中查看：

```mysql
127.0.0.1:6379> get 001
"[\"com.springboot.bean.Student\",{\"sno\":\"001\",\"name\":\"\xe5\xba\xb7\xe5\xba\xb7\",\"sex\":\"M \"}]"
```

可见更新数据库的同时，缓存也得到了更新。