# 1、Java基本概念

## 1、什么是标识符

标识符是大小写字母、数字字符、$和_组成，不能以数字开头，也不能是java关键字，并且区分大小写

## 2、请说明环境变量Path 与 classpath 区别

**path**是系统用来指定可指定文件的完整路径。classpath是用来搜索所执行的可执行文件路径的，如果执行的可执行文件不在当前目录下，那就会依次搜索path中设置的路径。

# 2、Java基础

## 1、Java中数据的类型转换有几种？分别是什么？

- **强制类型转换**
  容量大的类型向容量小的类型转换时使用
- **隐式类型转换**
  容器小的类型向容量大的类型转换时使用

## 2、Java语言中的字符char可以存储一个中文汉字吗?为什么呢?

char型变量是用来存储Unicode编码的字符的，unicode编码字符集中包含了汉字，所以，char型变量中可以存储汉字。不过，如果某个特殊的汉字没有被包含在unicode编码字符集中，那么，这个char型变量中就不能存储这个特殊汉字。
补充说明：unicode编码占用两个字节，所以，char类型的变量也是占用两个字节。

## 3、注释的分类及作用？

- 单行注释
  注释单行代码或为单行代码添加描述的时候使用
- 多行注释
  注释多行代码或为代码添加多行描述的时候使用
- 文档注释
  生产java帮助文档的时候使用，开发中常用来描述类、描述方法

## 4、请解释 ==与equals()方法的区别？

对于基本类型和引用类型 == 的作用效果是不同的，如下所示：

- 基本类型：比较的是值是否相同；
- 引用类型：比较的是引用是否相同；

```java
String x = "string";
String y = "string";
String z = new String("string");
System.out.println(x==y); // true
System.out.println(x==z); // false
System.out.println(x.equals(y)); // true
System.out.println(x.equals(z)); // true
```

代码解读：因为 x 和 y 指向的是同一个引用，所以 == 也是 true，而 new String()方法则重写开辟了内存空间，所以 == 结果为 false，而 equals 比较的一直是值，所以结果都为 true。



equals 本质上就是 ==，只不过 String 和 Integer 等重写了 equals 方法，把它变成了值比较。看下面的代码就明白了。

首先来看默认情况下 equals 比较一个有相同值的对象，代码如下：

```java
class Cat {
  public Cat(String name) {
    this.name = name;
  }
  private String name;
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}

Cat c1 = new Cat("mbb");
Cat c2 = new Cat("mbb");
System.out.println(c1.equals(c2)); // false
```

输出结果出乎我们的意料，竟然是 false？这是怎么回事，看了 equals 源码就知道了，源码如下：

```java
public boolean equals(Object obj) {
  return (this == obj);
}
```

原来 equals 本质上就是 ==

那问题来了，两个相同值的 String 对象，为什么返回的是 true？代码如下：

```java
String s1 = new String("mbb");
String s2 = new String("mbb");
System.out.println(s1.equals(s2)); // true
```

同样的，当我们进入 String 的 equals 方法，找到了答案，代码如下：

```java
public boolean equals(Object anObject) {

  if (this == anObject) {
    return true;
  }

  if (anObject instanceof String) {
    String anotherString = (String)anObject;
    int n = value.length;
    if (n == anotherString.value.length) {
      char v1[] = value;
      char v2[] = anotherString.value;
      int i = 0;

      while (n-- != 0) {
        if (v1[i] != v2[i])
          return false;
        i++;
      }

      return true;
    }
  }
  return false;
}
```

原来是 String 重写了 Object 的 equals 方法，把引用比较改成了值比较。

总结 ：== 对于基本类型来说是值比较，对于引用类型来说是比较的是引用；而 equals 默认情况下是引用比较，只是很多类重新了 equals 方法，比如 String、Integer 等把它变成了值比较，所以一般情况下 equals 比较的是值是否相等。

## 5、两个对象的 hashCode() 相同，则 equals() 也一定为 true，对吗？

不对，两个对象的 hashCode() 相同，equals() 不一定 true。

```java
String str1 = "通话";
String str2 = "重地";
System. out. println(String. format("str1：%d | str2：%d", str1. hashCode(),str2. hashCode()));
System. out. println(str1. equals(str2));
```

执行的结果：

```java
str1：1179395 | str2：1179395
false
```

代码解读：很显然“通话”和“重地”的 hashCode() 相同，然而 equals() 则为 false，因为在散列表中，hashCode() 相等即两个键值对的哈希值相等，然而哈希值相等，并不一定能得出键值对相等。

## 6、请说明 && 和 & 的区别？

&和&&都可以用作逻辑与的运算符，表示逻辑与(and)，当运算符两边的表达式的结果都为true时，整个运算结果才为true，否则，只要有一方为false，则结果为false。

&&还具有短路的功能，即如果第一个表达式为false，则不再计算第二个表达式

&还可以用作位运算符，当&操作符两边的表达式不是boolean类型时，&表示按位与操作。

# 3、Java变量

## 1、成员变量与局部变量的区别？

- 在类中的位置不同
  成员变量：在类中方法外
  局部变量：在方法定义中或者方法声明上
- 在内存中的位置不同
  成员变量：在堆内存
  局部变量：在栈内存
- 生命周期不同
  成员变量：随着对象的创建而存在，随着对象的消失而消失
  局部变量：随着方法的调用而存在，随着方法的调用完毕而消失
- 初始化值不同
  成员变量：有默认初始化值
  局部变量：没有默认初始化值，必须定义，赋值，然后才能使用。

## 2、静态变量与成员变量的区别？

- 所属不同
  静态变量属于类，所以也称为为类变量
  成员变量属于对象，所以也称为实例变量(对象变量)

- 内存中位置不同
  静态变量存储于方法区的静态区
  成员变量存储于堆内存

- 内存出现时间不同
  静态变量随着类的加载而加载，随着类的消失而消失
  成员变量随着对象的创建而存在，随着对象的消失而消失

- 调用不同
  静态变量可以通过类名调用，也可以通过对象调用
  成员变量只能通过对象名调用

# 4、Java String

## 1、 如何将字符串反转？

使用 StringBuilder 或者 stringBuffer 的 reverse() 方法。

```java
// StringBuffer reverse
StringBuffer stringBuffer = new StringBuffer();
stringBuffer.append("abcdefg");
System.out.println(stringBuffer.reverse()); // gfedcba

// StringBuilder reverse
StringBuilder stringBuilder = new StringBuilder();
stringBuilder.append("abcdefg");
System. out. println(stringBuilder.reverse()); // gfedcba
```

## 2、String str=“i” 与 String str=new String(“i”)一样吗？

不一样，因为内存的分配方式不一样。String str="i"的方式，Java 虚拟机会将其分配到常量池中；而 String str=new String(“i”) 则会被分到堆内存中。

## 3、请说明StringBuffer与StringBuilder二者之间的区别？

- **StringBuilder**
  是线程不安全的，运行效率高

  > 如果一个字符串变量是在方法里面定义，这种情况只可能有一个线程访问它，不存在不安全的因素了，则用StringBuilder。

- **StringBuffer**
  是线程安全的，运行要低于StringBilder

  > 如果要在类里面定义成员变量，并且这个类的实例对象会在多线程环境下使用，那么最好用StringBuffer。
  >
  > 安全原理：
  >
  > StringBuffer重写了length()和capacity()、append等方法，在他们的方法上面都有synchronized 关键字实现线程同步

# 5、Java 类、抽象类、接口、内部类、代码块

## 1、类是什么？ 对象是什么？举例说明

类是一组相关属性和行为的集合是一个抽象的东西,

对象则是该类的一个具体的体现。

举例: 学生就是一个类,然后每一个学生都是学生的一个个具体的体现,所以每一个学生就是一个学生。

## 2、 类由哪些内容组成？

类由成员变量和成员方法组成
成员变量对应的就是事物的属性(就是事物固有的信息,比如: 人的属性有身高 , 姓名 , 年龄 , 学历…) , 成员方法对应的是行为(行为: 就是该事物可以做的事情,比如:人的行为有: 吃饭,睡觉…)

## 3、抽象类中有没有构造方法,如果有它是用来做什么的?

抽象类虽然不能进行实例化,但是抽象类中是存在构造方法,该构造方法的作用是用于子类访问父类数据时的初始化.

## 4、抽象类和接口的区别?

#### 成员区别

- 抽象类
  - 成员变量
    可以是变量，也可以是常量
- 构造方法
  **有**
- 成员方法
  可以抽象，也可以非抽象
- 接口：
  - 成员变量
    只可以常量
  - 成员方法
    只可以抽象

#### 关系区别

- 类与类
  继承，单继承
- 类与接口
  实现，单实现，多实现
- 接口与接口
  继承，单继承，多继承

#### 设计理念区别

- 抽象类
  被继承体现的是：“**is a**”的关系。 抽象类中定义的是该继承体系的共性功能。
- 接口
  被实现体现的是：“**like a**”的关系。 接口中定义的是该继承体系的扩展功能。

## 5、JDK中多态的例子

1. java.util.List 接口：List 是一个接口，它有多个实现类，如 ArrayList 和 LinkedList。
2. java.lang.Object：Object 是 Java 中的根类，所有类都是它的子类，它提供了多态的基础。
3. java.util.Collection：Collection 是一个接口，它提供了多个多态方法，如 add、remove 等。
4. java.util.Map：Map 是一个接口，它提供了多个实现类，如 HashMap 和 TreeMap。

# 6、 Java 容器

## 1、 Iterator 和 ListIterator 有什么区别？

- Iterator 可以遍历 Set 和 List 集合，而 ListIterator 只能遍历 List。
- Iterator 只能单向遍历，而 ListIterator 可以双向遍历（向前/后遍历）。

- ListIterator 从 Iterator 接口继承，然后添加了一些额外的功能，比如添加一个元素、替换一个元素、获取前面或后面元素的索引位置。

## 2、请简述ArrayList、Vector、LinkedList三者的特点？

#### ArrayList

底层数组结构

- 特点
  - 线程不同步
  - 效率高
  - 元素查找快
  - 增删慢；

#### Vector

底层数组结构

- 特点
  - 线程同步
  - 安全
  - 元素查找快
  - 增删慢

#### LinkedList

底层链表结构

- 特点
  - 线程不同步
  - 效率高
  - 元素增删快
  - 查找慢

#### 保障ArrayList的线程安全

- **CopyOnWriteList**

  - Copy On Write 也是一种重要的思想，在写少读多的场景下，为了保证集合的线程安全性，我们完全可以在当前线程中得到原始数据的一份拷贝，然后进行操作。JDK集合框架中为我们提供了 ArrayList 的这样一个实现：CopyOnWriteArrayList。但是如果不是写少读多的场景，使用 CopyOnWriteArrayList 开销比较大，因为每次对其更新操作（add/set/remove）都会做一次数组拷贝。

    与ReentrantReadWriteLock 读写锁的思想非常类似，也就是读读共享、写写互斥、读写互斥、写读互斥。JDK 中提供了 CopyOnWriteArrayList 类比相比于在读写锁的 思想又更进一步。为了将读取的性能发挥到极致，CopyOnWriteArrayList 读取是完全不用加锁的， 并且更厉害的是:写入也不会阻塞读取操作。只有写入和写入之间需要进行同步等待。这样一来，读操 作的性能就会大幅度提升。

    CopyOnWriteArrayList 类的所有可变操作(add，set 等等)都是通过创建底层数组的新副本来实现 的。当 List 需要被修改的时候，我并不修改原有内容，而是对原有数据进行一次复制，将修改的内容写 入副本。写完之后，再将修改完的副本替换原来的数据，这样就可以保证写操作不会影响读操作了。

    从 CopyOnWriteArrayList 的名字就能看出 CopyOnWriteArrayList 是满足 CopyOnWrite 的 ArrayList，所谓CopyOnWrite 也就是说:在计算机，如果你想要对一块内存进行修改时，我们不在原 有内存块中进行写操作，而是将内存拷贝一份，在新的内存中进行写操作，写完之后呢，就将指向原来 内存指针指向新的内存，原来的内存就可以被回收掉了。

    CopyOnWriteArrayList 写入操作 add() 方法在添加集合的时候加了锁，保证了同步，避免了多线程写 的时候会 copy 出多个副本出来。

    ```java
    **
     * Appends the specified element to the end of this list.
     *
     * @param e element to be appended to this list
     * @return {@code true} (as specified by {@link Collection#add})
     */
    public boolean add(E e) {
    		final ReentrantLock lock = this.lock; 
       	lock.lock();//加锁
    		try {
    				Object[] elements = getArray();
    				int len = elements.length;
    				Object[] newElements = Arrays.copyOf(elements, len + 1);//拷贝新数组 newElements[len] = e;
    				setArray(newElements);
    				return true;
    		} finally { 
          	lock.unlock();//释放锁
    		} 
     }
    ```

    

- **Collections.synchronizedList()**

  - 其实底层也是在集合的所有方法之上加上了synchronized

- **synchronized**

  - 使用**synchronized**来同步所有的[ArrayList](https://so.csdn.net/so/search?q=ArrayList&spm=1001.2101.3001.7020)操作方法

## 3、Array 和 ArrayList 有何区别？

- Array 可以存储基本数据类型和对象，ArrayList 只能存储对象。
- Array 是指定固定大小的，而 ArrayList 大小是自动扩展的。

- Array 内置方法没有 ArrayList 多，比如 addAll、removeAll、iteration 等方法只有 ArrayList 有。

## 4、在 Queue（队列） 中 poll()和 remove()有什么区别？

- 相同点：都是返回第一个元素，并在队列中删除返回的对象。
- 不同点：如果没有元素 poll()会返回 null，而 remove()会直接抛出 NoSuchElementException 异常。

## 5、请简述List<? extends T>和List<? super T>之间有什么区别？

- List<? extends T>
  **向下限制**
  ? extends T ： 代表接收的泛型类型为T类型或T子类类型
- List<? super T>
  **向上限制**
  ? super T ：代表接收的泛型类型为T类型或T父类类型

## 6、说一下 HashSet 的实现原理？

HashSet 是基于 HashMap 实现的，HashSet 底层使用 HashMap 来保存所有元素，因此 HashSet 的实现比较简单，相关 HashSet 的操作，基本上都是直接调用底层 HashMap 的相关方法来完成，HashSet 不允许重复的值。

## 7、List、Set、Map 之间的区别是什么？

List、Set、Map 的区别主要体现在两个方面：元素是否有序、是否允许元素重复。

![](/Users/wangyang/Desktop/interview/JAVA基础/5fd0771e9cc6d7f9bb583ad92343f892.png)

## 8、请简述HashSet是如何保证元素唯一性的?

HashSet集合中存储的元素，通过重写hashCode() 与 equals()方法来保证元素唯一性

## 9、请简述TreeSet是如何保证元素唯一性与排序的？

1. 实现自然排序接口 Comparable，重写 compareTo(T t)方法
2. 实现比较器排序接口 Comparator，重写 compare(T t1, T t2)方法

## 10、 如何决定使用 HashMap 还是 TreeMap？

对于在 Map 中插入、删除、定位一个元素这类操作，HashMap 是最好的选择，因为相对而言 HashMap 的插入会更快，但如果你要对一个 key 集合进行有序的遍历，那 TreeMap 是更好的选择。

## 11、sort排序，核心是什么

所以，java中的sort方法并不是选择了某一种排序算法，而是一种综合体，根据待排序的数组的长度，以及数组中正序和逆序的数量来决定使用什么样的排序。

总结一下就是下面这三条。

1.数组数量大于等于286，并且数组中相邻的逆序数量小于67，并且相邻的相等数量小于33，使用归并排序。否则进入条件2；

2.数组数量小于47，使用插入排序。否则进入条件3；

3.使用双轴快排。

# 7、JAVA异常

## 1、请说说什么是异常？异常的分类？

**什么是异常？**
Java异常是java提供的用于`处理程序中错误的一种机制`。
所谓错误是指在程序运行的过程中发生的一些异常事件（如：除0错误，数组下标越界，所要读取的文件不存在）。设计良好地程序应该在程序异常发生时提供处理这些错误的方法，使得程序不会因为异常的发送而阻断或产生不可预见的结果。

Java程序的执行过程中如出现异常事件，可以生成一个异常类对象，该异常对象封装了异常事件的信息，并将被提交给java运行时系统，这个过程称为抛出异常。当java运行时系统接收到异常对象时，会寻找能处理这一异常的代码并把当前异常对象交其处理，这一过程称为捕获异常。

**异常的分类**

- Exception
  所有异常类的父类，其子类对应了各种各样的可能出现的异常事件，一般需要用户显示的声明或捕获。
- Error
  称为错误，由java虚拟机生成并抛出，包括动态链接失败，虚拟机错误等，程序对其不做处理。

- Runtime Exception
  一类特殊的异常，如被0除、数组下标超范围等，其产生比较频繁，处理麻烦，如果显示的声明或捕获将会对程序可读性和运行效率影响很大。因此由系统自动检测并将它们交给缺省的异常处理程序（用户可不必对其处理）。

## 2、请说说throws与throw的区别？

- **throws**
  通常被应用在声明方法时，用来**指定可能抛出的异常**。多个异常可以使用逗号隔开。当在主函数中调用该方法时，如果发生异常，就会将异常抛给指定异常对象。
- **throw**
  通常用在方法体中，并且抛出一个异常对象。程序在执行到throw语句时立即停止，它后面的语句都不执行。通常throw抛出异常后，如果想在上一级代码中捕获并处理异常，则需要在抛出异常的方法中使用throws关键字在方法声明中指定要抛出的异常；如果要捕获throw抛出的异常，则必须使用try{}catch{}语句。

## 3、请说说final、finally与finalize的区别？

- **final**
  用于声明属性，方法和类，分别表示属性不可变，方法不可覆盖，类不可继承。内部类要访问局部变量，局部变量必须定义成final类型。
- **finally**
  是异常处理语句结构的一部分，表示总是执行。
- **finalize**
  是Object类的一个方法，在垃圾收集器执行的时候会调用被回收对象的此方法，可以覆盖此方法提高垃圾收集时的其他资源回收，例如关闭文件等。JVM不保证此方法总被调用。

## 4、请说出最常见到的RuntimeException异常

- **NullPointerException**
  空指针引用异常
- **ClassCastException**
  类型强制转换异常
- **IllegalArgumentException**
  传递非法参数异常
- **ArithmeticException**
  算术运算异常
- **ArrayStoreException**
  向数组中存放与声明类型不兼容对象异常
- **IndexOutOfBoundsException**
  下标越界异常
- **NumberFormatException**
  数字格式异常

## 8、JAVA IO流

## 1、BIO、NIO、AIO 有什么区别？

- BIO
  Block IO 同步阻塞式 IO，就是我们平常使用的传统 IO，它的特点是模式简单使用方便，并发处理能力低。

- NIO
  New IO 同步非阻塞 IO，是传统 IO 的升级，客户端和服务器端通过 Channel（通道）通讯，实现了多路复用。

- AIO

  Asynchronous IO 是 NIO 的升级，也叫 NIO2，实现了异步非堵塞 IO ，异步 IO 的操作基于事件和回调机制。
