# Spring Boot

## 1. Spring Boot基础

### 1.1 什么是SpringBoot？

SpringBoot来简化Spring应用开发，约定大于配置，去繁化简

### 1.2 SpringBoot有哪些优点？
- 独立运行
  Spring Boot 而且内嵌了各种 servlet 容器，Tomcat、Jetty 等，现在不再需要打成war 包部署到容器中，Spring Boot 只要打成一个可执行的 jar 包就能独立运行，所有的依赖包都在一个 jar 包内。
- 简化配置
  spring-boot-starter-web 启动器自动依赖其他组件，简少了 maven 的配置。
- 自动配置
  Spring Boot 能根据当前类路径下的类、jar 包来自动配置 bean，如添加一个 spring-boot-starter-web 启动器就能拥有 web 的功能，无需其他配置。
- 无代码生成和XML配置
  Spring Boot 配置过程中无代码生成，也无需 XML 配置文件就能完成所有配置工作，这一切都是借助于条件注解完成的，这也是 Spring4.x 的核心功能之一。
- 避免大量的Maven导入和各种版本冲突
- 应用监控
  Spring Boot 提供一系列端点可以监控服务及应用，做健康检测。

### 1.3 SpringBoot的核心注解是什么？由那些注解组成？

启动类上@SpringBootApplication是 SpringBoot 的核心注解

```java
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@SpringBootConfiguration
@EnableAutoConfiguration
@ComponentScan(excludeFilters = {
      @Filter(type = FilterType.CUSTOM, classes = TypeExcludeFilter.class),
      @Filter(type = FilterType.CUSTOM, classes = AutoConfigurationExcludeFilter.class) })
public @interface SpringBootApplication {
```

主要组合包含了以下 3 个注解：

- @SpringBootConfiguration：
  组合了 @Configuration 注解，实现配置文件的功能。
- @EnableAutoConfiguration：
  打开自动配置的功能，也可以关闭某个自动配置的选项，如关闭数据源自动配置功能： @SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })。
- @ComponentScan：
  Spring组件扫描。

### 1.4 SpringBoot、Spring MVC和Spring有什么区别？
- Spring
  Spring最重要的特征是依赖注入。所有Spring Modules不是依赖注入就是IOC控制反转。
  当我们恰当的使用DI或者是IOC的时候，可以开发松耦合应用。
- Spring MVC
  Spring MVC提供了一种分离式的方法来开发Web应用。通过运用像DispatcherServelet，MoudlAndView 和 ViewResolver 等一些简单的概念，开发 Web 应用将会变的非常简单。
- SpringBoot
  Spring和Spring MVC的问题在于需要配置大量的参数。
  SpringBoot通过一个自动配置和启动的项来解决这个问题。

### 1.6 SpringBoot启动时都做了什么?
1. 加载配置：读取启动参数，加载配置文件，根据配置的属性初始化系统。
2. 初始化环境：创建并初始化Spring上下文，加载bean定义并创建bean实例。
3. 执行初始化：调用初始化方法或者实现特定接口，进一步初始化应用。
4. 启动服务：启动web服务器，监听端口并处理请求。
5. 运行应用：处理请求，执行业务逻辑，访问数据库，返回结果给客户端。

### 1.7 SpringBoot自动配置原理是什么？**@EnableAutoConfiguration**
- SpringBoot启动会加载大量的自动配置类
- 我们看我们需要的功能有没有在SpringBoot默认写好的自动配置类当中；
- 我们再来看这个自动配置类中到底配置了哪些组件；（只要我们要用的组件存在在其中，我们就不需要再手动配置了）
- 给容器中自动配置类添加组件的时候，会从properties类中获取某些属性。我们只需要在配置文件中指定这些属性的值即可；
  xxxxAutoConfigurartion：自动配置类；给容器中添加组件

#### 如何定制SpringBoot的自动配置？

1. 创建一个配置类，并在该类上使用 @Configuration 注解。
2. 在该配置类中定义一个 @Bean 方法，该方法提供了要替换的配置信息。
3. 在该配置类上使用 @ConditionalOnMissingBean 注解，该注解告诉 Spring Boot 在没有相同类型的 bean 定义的情况下使用该类的配置信息。
4. 将该配置类的路径传递给 Spring Boot 应用程序的启动类的 @ComponentScan 注解，以确保该配置类在启动时被加载。

#### SpringBoot如何管理Bean？

Spring Boot通过使用Spring的Bean管理功能，来管理Bean。Spring Boot通过扫描类路径上的@Configuration和@Component类，以及使用@Bean注释的方法，来定义和注册应用程序中的Bean。Spring Boot可以在启动时自动扫描并将所有可用的Bean注册到应用程序上下文中，并可以在整个应用程序的生命周期内通过@Autowired注解直接注入Bean。

### 1.8 你如何理解SpringBoot配置加载顺序？

```xml
1、默认配置：Spring Boot会提供一组默认配置，这些配置通常是硬编码在Spring Boot内部的，用于配置Spring Boot框架的基本设置。这些默认配置是最低优先级的配置。

2、应用程序级配置：在Spring Boot应用程序中，可以使用application.properties或application.yml文件来定义应用程序级别的配置。这些配置文件的加载顺序是根据以下优先级确定的：
	1)、application.properties文件
	2)、application.yml文件
	3)、application-{profile}.properties文件（如果指定了激活的配置文件）
	4)、application-{profile}.yml文件（如果指定了激活的配置文件）
配置文件的加载顺序是从上到下的，后面加载的配置会覆盖前面加载的配置。

3、外部配置文件：除了应用程序级配置，Spring Boot还支持在外部目录中放置配置文件，如/config或指定的自定义目录。这些外部配置文件的加载顺序与应用程序级配置相同，但具有更高的优先级，可以覆盖应用程序级配置。

4、命令行参数：通过命令行参数传递的配置会覆盖配置文件中的配置。例如，可以使用--server.port=8080来指定应用程序的端口。

5、环境变量：环境变量中定义的配置也具有较高的优先级，可以覆盖配置文件中的配置。

6、属性源：Spring Boot支持多种属性源，包括Java系统属性、操作系统环境变量、JNDI、属性文件、命令行参数等。这些属性源的优先级可以通过spring.config.name和spring.config.location来配置。

综合上述配置加载顺序，Spring Boot会按照优先级从低到高依次加载配置，较高优先级的配置可以覆盖较低优先级的配置。这使得开发者可以根据需要提供不同环境、不同配置文件和不同命令行参数来管理应用程序的配置，而不需要修改应用程序的源代码。这种配置加载顺序的灵活性使得Spring Boot适用于各种不同的部署环境和应用场景。
```



### 1.9 SpringBoot的异常处理

#### 异常处理主要分为三类：

1. 基于**请求转发**的方式处理异常；
2. 基于**异常处理器**的方式处理异常；
3. 基于**过滤器**的方式处理异常。

#### 基于请求转发

基于请求转发的异常处理方式是真正的全局异常处理。

**实现方式有：**

- BasicExceptionController

#### 基于异常处理器

基于异常处理器的异常处理方式其实并不是真正的全局异常处理，因为它处理不了过滤器等抛出的异常。

**实现方式有：**

- @ExceptionHandler
- @ControllerAdvice+@ExceptionHandler
- SimpleMappingExceptionResolver
- HandlerExceptionResolver

#### 基于过滤器

基于过滤器的异常处理方式近似与全局异常处理。它能处理过滤器及之后的环节抛出的异常。

**实现方式有：**

- Filter

#### 常见异常处理实现方案

##### 1. BasicExceptionController

**这是SpringBoot默认处理异常方式**：一旦程序中出现了异常SpringBoot就会请求/error的url，在SpringBoot中提供了一个叫BasicExceptionController的类来处理/error请求，然后跳转到默认显示异常的页面来展示异常信息。显示异常的页面也可以自定义，在目录src/main/resources/templates/下定义一个叫error的文件，可以是jsp也可以是html 。

**此种方式是通过请求转发实现的**，出现异常时，会转发到请求到/error，该接口对异常进行处理返回。是最符合**全局异常处理**的。

可以自定义Controller继承BasicErrorController异常处理来实现异常处理的自定义。

##### 2. @ExceptionHandler

该种方式只能作用于使用@ExceptionHandler注解的Controller的异常，对于其他Controller的异常就无能为力了，所以并不不推荐使用。

**此种方式是通过异常处理器实现的**，使用HandlerExceptionResolverComposite异常处理器中的ExceptionHandlerExceptionResolver异常处理器处理的。

##### 3. @ControllerAdvice+@ExceptionHandler（项目使用）

使用 @ControllerAdvice+@ExceptionHandler注解能够进行**近似全局异常处理**，这种方式**推荐使用**。

一般说它只能处理**控制器中抛出的异常**，这种说法并不准确，其实它能处理DispatcherServlet.doDispatch方法中DispatcherServlet.processDispatchResult方法之前捕捉到的所有异常，包括：拦截器、参数绑定（参数解析、参数转换、参数校验）、控制器、返回值处理等模块抛出的异常。

**此种方式是通过异常处理器实现的**，使用HandlerExceptionResolverComposite异常处理器中的ExceptionHandlerExceptionResolver异常处理器处理的。

**使用方式**

定义一个类，使用@ControllerAdvice注解该类，使用@ExceptionHandler注解方法。@RestControllerAdvice注解是@ControllerAdvice注解的扩展（@RestControllerAdvice=@ControllerAdvice+@ResponseBody），返回值自动为JSON的形式。

@ResponseStatus注解

作用：指定http状态码，正确执行时返回该状态码，但方法执行报错时，该返回啥状态码就是啥状态码，指定的状态码无效。

#### 全局异常处理实现方案

要想实现正在的全局异常处理，显然只通过异常处理器的方式处理是不够的，这种方案处理不了过滤器等抛出的异常。

**全局异常处理的几种实现方案：**

- 基于**请求转发**；

该方案**貌似不好获取到特殊的异常描述信息**（没仔细研究），如参数校验中的message属性信息：

```kotlin
@NotNull(message = "主键不能为空")
```

本方案通过自定义错误处理Controller继承BasicExceptionController来实现。

具体实现参考：**常用异常处理实现方案1**。

- 基于**异常处理器+请求转发补充**；

**1）自定义异常处理Controller实现BasicExceptionController**,具体实现参考：**常用异常处理实现方案1**。

**（2）异常处理器实现**

- 方式1：@ControllerAdvice+@ExceptionHandler（**推荐使用**）具体实现参考：**常用异常处理实现方案3**。	

  - 方式2：SimpleMappingExceptionResolver具体实现参考：**常用异常处理实现方案4**。

  - 方式3：HandlerExceptionResolver具体实现参考：**常用异常处理实现方案5**。

- 基于**过滤器**；

- 基于**异常处理器+过滤器补充**。


### 1.10、如何使用SpringBoot的DevTools实现热部署？

当我们修改代码时，DevTools会监测文件系统的变化并自动重新启动应用。在重新启动过程中，DevTools会把更改的类加载到新的ClassLoader中，这样我们就可以看到更改后的代码对应用产生的影响。

要使用DevTools，只需要在项目中添加spring-boot-devtools依赖即可。并且，在启动时，要确保JVM参数 -Dspring.devtools.restart.enabled=true 被设置。这样DevTools就可以正常工作并实现热部署了。

## 2. SpringBoot配置

### 2.5 SpringBoot核心配置文件是什么？

bootstrap.properties和application.properties

### 2.6 bootstrap.properties和application.properties 有何区别 ?
SpringBoot两个核心的配置文件：

- bootstrap(.yml 或者 .properties)：boostrap 由父 ApplicationContext 加载的，比applicaton优先加载，配置在应用程序上下文的引导阶段生效。一般来说我们在 SpringCloud Config 或者Nacos中会用到它。且boostrap里面的属性不能被覆盖；
- application (.yml或者.properties)：由ApplicatonContext 加载，用于 SpringBoot项目的自动化配置。

### 2.7 什么是Spring Profiles？
主要用来区分环境；
Spring Profiles 允许用户根据配置文件（dev，test，prod 等）来注册 bean。因此，当应用程序在开发中运行时，只有某些 bean 可以加载，而在 PRODUCTION中，某些其他 bean 可以加载。假设我们的要求是 Swagger 文档仅适用于 QA 环境，并且禁用所有其他文档。这可以使用配置文件来完成。Spring Boot 使得使用配置文件非常简单。

### 2.8、SpringBoot的配置文件如何加载？

1. 首先加载jar包外的application.properties或application.yml文件，从上往下加载。
2. 其次加载jar包内的application.properties或application.yml文件。
3. 然后加载通过命令行参数传入的配置文件。
4. 最后加载通过代码的方式传入的配置文件。

## 3. SpringBoot安全性

### 3.2 比较一下Spring Security 和Shiro各自的优缺点 ?
由于SpringBoot官方提供了大量的非常方便的开箱即用的Starter，包括Spring Security的Starter ，使得在 SpringBoot中使用Spring Security变得更加容易，甚至只需要添加一个依赖就可以保护所有的接口，所以，如果是SpringBoot 项目，一般选择 Spring Security 。当然这只是一个建议的组合，单纯从技术上来说，无论怎么组合，都是没有问题的。Shiro和Spring Security相比，主要有如下一些特点：

- Spring Security 是一个重量级的安全管理框架；Shiro 则是一个轻量级的安全管理框架
- Spring Security 概念复杂，配置繁琐；Shiro 概念简单、配置简单
- Spring Security 功能强大；Shiro 功能简单

### 3.3 什么是 CSRF 攻击？

CSRF 代表跨站请求伪造。这是一种攻击，迫使最终用户在当前通过身份验证的Web 应用程序上执行不需要的操作。CSRF 攻击专门针对状态改变请求，而不是数据窃取，因为攻击者无法查看对伪造请求的响应。

### 3.4 SpringBoot中如何解决跨域问题 ?

跨域可以在前端通过 JSONP 来解决，但是 JSONP 只可以发送 GET 请求，无法发送其他类型的请求，在 RESTful 风格的应用中，就显得非常鸡肋，因此我们推荐在后端通过 （CORS，Cross-origin resource sharing） 来解决跨域问题。这种解决方案并非 Spring Boot 特有的，在传统的 SSM 框架中，就可以通过 CORS 来解决跨域问题，只不过之前我们是在 XML 文件中配置 CORS ，现在可以通过实现WebMvcConfigurer接口然后重写addCorsMappings方法解决跨域问题。

```java
@Configuration
public class CorsConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowCredentials(true)
                .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
                .maxAge(3600);
    }
}
```

项目中前后端分离部署，所以需要解决跨域的问题。
我们使用cookie存放用户登录的信息，在spring拦截器进行权限控制，当权限不符合时，直接返回给用户固定的json结果。
当用户登录以后，正常使用；当用户退出登录状态时或者token过期时，由于拦截器和跨域的顺序有问题，出现了跨域的现象。
我们知道一个http请求，先走filter，到达servlet后才进行拦截器的处理，如果我们把cors放在filter里，就可以优先于权限拦截器执行。

```java
@Configuration
public class CorsConfig {
    @Bean
    public CorsFilter corsFilter() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.addAllowedOrigin("*");
        corsConfiguration.addAllowedHeader("*");
        corsConfiguration.addAllowedMethod("*");
        corsConfiguration.setAllowCredentials(true);
        UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
        urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
        return new CorsFilter(urlBasedCorsConfigurationSource);
    }
}
```

## 4. SpringBoot进阶

### 4.1 什么是 WebSockets？

WebSocket是一种计算机通信协议，通过单个TCP连接提供全双工通信信道。
1、WebSocket是双向的 -使用 WebSocket 客户端或服务器可以发起消息发送。
2、WebSocket是全双工的 -客户端和服务器通信是相互独立的。
3、单个TCP连接 -初始连接使用 HTTP，然后将此连接升级到基于套接字的连接。然后这个单一连接用于所有未来的通信
4、Light与http相比，WebSocket消息数据交换要轻得多。

### 4.7 Swagger用过麽？他用来做什么？

Swagger广泛用于可视化API，使用SwaggerUl为前端开发人员提供在线沙箱。Swagger 是用于生成RESTful Web服务的可视化表示的工具，规范和完整框架实现。它使文档能够以与服务器相同的速度更新。当通过Swagger 正确定义时，消费者可以使用最少量的实现逻辑来理解远程服务并与其进行交互。因此，Swagger 消除了调用服务时的猜测。

### 4.9 SpringBoot 中的starter到底是什么 ?
首先，这个 Starter 并非什么新的技术点，基本上还是基于 Spring 已有功能来实现的。首先它提供了一个自动化配置类，一般命名为 XXXAutoConfiguration ，在这个配置类中通过条件注解来决定一个配置是否生效（条件注解就是 Spring 中原本就有的），然后它还会提供一系列的默认配置，也允许开发者根据实际情况自定义相关配置，然后通过类型安全的属性注入将这些配置属性注入进来，新注入的属性会代替掉默认属性。正因为如此，很多第三方框架，我们只需要引入依赖就可以直接使用了。当然，开发者也可以自定义 Starter

### 4.13 如何使用SpringBoot实现分页和排序？

使用Spring Boot实现分页非常简单。使用Spring Data-JPA可以实现将可分页的`org.springframework.data.domain.Pageable`传递给存储库方法。

## 5. Spring MVC 工作流程

- 用户发送请求至前端控制器DispatcherServlet；
- DispatcherServlet收到请求后，调用HandlerMapping处理器映射器，请求获取Handle；
- 处理器映射器根据请求url找到具体的处理器，生成处理器对象及处理器拦截器(如果有则生成)一并返回给DispatcherServlet；
- DispatcherServlet 调用 HandlerAdapter处理器适配器；
- HandlerAdapter 经过适配调用 具体处理器(Handler，也叫后端控制器)；
- Handler执行完成返回ModelAndView；
- HandlerAdapter将Handler执行结果ModelAndView返回给DispatcherServlet；
- DispatcherServlet将ModelAndView传给ViewResolver视图解析器进行解析；
- ViewResolver解析后返回具体View；
- DispatcherServlet对View进行渲染视图（即将模型数据填充至视图中）
- DispatcherServlet响应用户。

![](/Users/wangyang/Desktop/interview/Spring笔记/1649e9bc38cfa7ece6ae2d3904ccf675.png)

## 事务的上下文

在Java开发中，事务的上下文通常指的是在进行数据库事务处理时所处的环境、情境、背景和相关信息，包括数据库连接、数据源配置、事务隔离级别、事务传播行为、异常处理等因素。在Java开发中，使用事务可以确保一组相关操作要么全部执行成功，要么全部失败回滚，从而保证数据的一致性和完整性。

在一个Java事务的上下文中，通常需要考虑以下因素：

1. 数据库连接：事务的上下文需要一个有效的数据库连接。
2. 数据源配置：需要配置正确的数据源信息，包括数据源类型、数据源地址、用户名和密码等。
3. 事务隔离级别：需要考虑事务的隔离级别，例如读未提交、读已提交、可重复读和串行化。
4. 事务传播行为：需要考虑事务传播行为，例如REQUIRED、REQUIRES_NEW、NESTED等，以确定如何处理事务。
5. 异常处理：需要考虑异常处理机制，例如在事务中出现异常时如何处理，是回滚还是提交。

理解事务的上下文可以帮助Java开发人员正确地配置和处理事务，从而确保数据的一致性和完整性。

在Java开发中，事务上下文通常可以分为以下几种：

1. 声明式事务上下文：使用注解或XML配置声明式事务管理。这种上下文通常由Spring框架来管理，通过对标注了事务注解的方法进行拦截，自动开启事务、提交或回滚事务。这种方式非常方便，代码相对简洁。
2. 编程式事务上下文：在代码中使用编程式事务管理来控制事务。这种方式需要手动开启事务、提交或回滚事务，可以使用Java中的TransactionTemplate或者编写底层代码实现。
3. JTA事务上下文：在多个数据源或者不同的应用之间需要进行分布式事务管理时，可以使用JTA事务管理器。这种方式可以控制多个事务之间的协调和回滚，但需要对应用进行适当的配置和支持。

对于一个特定的Java应用，可能会使用一种或多种事务上下文，具体取决于应用的复杂度和需求。不同的事务上下文方式有不同的优缺点和适用场景，开发人员需要根据具体情况选择适合的方式。
