# Spring



## 1. Spring基础

### 1.1 什么是 spring?

Spring是一个轻量级Java开发框架，最早由Rod Johnson创建，目的是为了解决企业级应用开发的业务逻辑层和其他各层的耦合问题。它是一个分层的JavaSE/JavaEE full-stack（一站式）轻量级开源框架，为开发Java应用程序提供全面的基础架构支持。Spring负责基础架构，因此Java开发者可以专注于应用程序的开发。

Spring最根本的使命是解决企业级应用开发的复杂性，即简化Java开发。

Spring可以做很多事情，它为企业级开发提供给了丰富的功能，但是这些功能的底层都依赖于它的两个核心特性，也就是依赖注入（dependency injection，DI）和面向切面编程（aspect-oriented programming，AOP）。

为了降低Java开发的复杂性，Spring采取了以下4种关键策略

1. 基于POJO的轻量级和最小侵入性编程；
2. 通过依赖注入和面向接口实现松耦合；
3. 基于切面和惯例进行声明式编程；
4. 通过切面和模板减少样板式代码

### 1.2 Spring框架的设计目标，设计理念，和核心是什么?

Spring设计目标：
Spring为开发者提供一个一站式轻量级应用开发平台；

Spring设计理念：
在JavaEE开发中，支持POJO和JavaBean开发方式，使应用面向接口开发，充分支持OO（面向对象）设计方法；Spring通过IoC容器实现对象耦合关系的管理，并实现依赖反转，将对象之间的依赖关系交给IoC容器，实现解耦；

Spring框架的核心：
IoC容器和AOP模块。通过IoC容器管理POJO对象以及他们之间的耦合关系；通过AOP以动态非侵入的方式增强服务。

### 1.5 Spring 框架中都用到了哪些设计模式？

- 工厂模式：BeanFactory就是简单工厂模式的体现，用来创建对象的实例；
- 单例模式：Bean默认为单例模式。
- 代理模式：Spring的AOP功能用到了JDK的动态代理和CGLIB字节码生成技术；
- 模板方法：用来解决代码重复的问题。比如. RestTemplate, JmsTemplate, JpaTemplate。
- 观察者模式：定义对象键一种一对多的依赖关系，当一个对象的状态发生改变时，所有依赖于它的对象都会得到通知被制动更新，如Spring中listener的实现–ApplicationListener。

### 1.6 spring context应用上下文有什么作用？


Spring的应用上下文（Application Context）是Spring框架中的一个关键组件，它的作用非常重要，主要包括以下几个方面：

1. **依赖注入**：应用上下文负责管理应用程序中的Bean实例，以及它们之间的依赖关系。通过应用上下文，Spring容器可以自动地将Bean注入到其他Bean中，实现了依赖注入（DI）。
2. **Bean的生命周期管理**：应用上下文负责管理Bean的生命周期，包括Bean的创建、初始化、销毁等阶段。开发者可以通过配置来自定义Bean的生命周期。
3. **AOP（面向切面编程）支持**：应用上下文提供了对AOP的支持，可以定义切面、通知和切点，从而实现横切关注点的编程。
4. **国际化和本地化**：Spring应用上下文支持国际化和本地化，使得应用程序可以根据不同的语言和地区显示内容。
5. **资源管理**：应用上下文提供了资源加载、文件访问、URL访问等资源管理的功能，可以方便地访问各种资源。
6. **事件传播**：应用上下文支持事件传播，可以定义自定义事件并监听事件，实现模块之间的解耦。
7. **配置管理**：Spring应用上下文可以读取配置信息，包括配置文件、属性文件、XML配置等，用于配置应用程序的行为。
8. **作用域管理**：应用上下文支持不同的作用域，如单例、原型、会话作用域等，使得可以灵活地管理Bean的生命周期。

总之，Spring应用上下文是Spring框架的核心容器，它提供了一种轻量级的方式来管理Bean、处理依赖关系、支持AOP、处理资源、国际化和本地化等功能。应用上下文的存在使得Spring应用程序更加模块化、可维护和可测试，降低了应用程序的耦合度，提高了代码的可重用性。不同的应用上下文实现可以用于不同的应用场景，例如Web应用程序可以使用`WebApplicationContext`，基于Spring Boot的应用程序可以使用`ApplicationContext`等。

## 2. Spring IOC

### 2.1 什么是 Spring IOC 容器？

控制反转即IoC (Inversion of Control)，它把传统上由程序代码直接操控的对象的调用权交给容器，通过容器来实现对象组件的装配和管理。所谓的“控制反转”概念就是对组件对象控制权的转移，从程序代码本身转移到了外部容器。

Spring IOC 负责创建对象，管理对象（通过依赖注入（DI），装配对象，配置对象，并且管理这些对象的整个生命周期

![](/Users/wangyang/Desktop/interview/Spring笔记/f204a0230aa1a7470fc4102fe9ceb774.png)

### 2.2 Spring IoC 的实现机制

Spring 中的 IoC 的实现原理就是工厂模式加反射机制 。

```java
interface Fruit {
  public abstract void eat();
}

class Apple implements Fruit {
  public void eat(){
    System.out.println("Apple");
  }
}
class Orange implements Fruit {
  public void eat(){
    System.out.println("Orange");
  }
}
class Factory {
  public static Fruit getInstance(String ClassName) { 
    Fruit f=null;
    try {
      f=(Fruit)Class.forName(ClassName).newInstance(); } catch (Exception e) {
    e.printStackTrace();
    }
    return f;
  }
}

class Client {
  public static void main(String[] a) {
    Fruit f=Factory.getInstance("com.xxx.xxx.Apple");
    if(f!=null){
      f.eat();
    } 
  }
}
```

### 2.3 Spring 如何设计容器的，BeanFactory和ApplicationContext的关系详解?

Spring 作者 Rod Johnson 设计了两个接口用以表示容器。

- BeanFactory
- ApplicationContext

BeanFactory 简单粗暴，可以理解为就是个 HashMap，Key 是 BeanName，Value 是 Bean 实例。通常只提供注册（put），获取（get）这两个功能。我们可以称之为 “低级容器”。

ApplicationContext 可以称之为 “高级容器”。因为他比 BeanFactory 多了更多的功能。他继承了多个接口。因此具备了更多的功能。例如资源的获取，支持多种消息（例如 JSP tag 的支持），对 BeanFactory 多了工具级别的支持等待。所以你看他的名字，已经不是 BeanFactory 之类的工厂了，而是 “应用上下文”， 代表着整个大容器的所有功能。该接口定义了一个 refresh 方法，此方法是所有阅读 Spring 源码的人的最熟悉的方法，用于刷新整个容器，即重新加载/刷新所有的 bean。

当然，除了这两个大接口，还有其他的辅助接口，这里就不介绍他们了。

BeanFactory和ApplicationContext的关系

为了更直观的展示 “低级容器” 和 “高级容器” 的关系，这里通过常用的 ClassPathXmlApplicationContext 类来展示整个容器的层级 UML 关系。

![](/Users/wangyang/Desktop/interview/Spring笔记/a6aae165c3dddab385cfa1849c9c16da.png)

最上面的是 BeanFactory，下面的 3 个绿色的，都是功能扩展接口，这里就不展开讲。

看下面的隶属 ApplicationContext 粉红色的 “高级容器”，依赖着 “低级容器”，这里说的是依赖，不是继承哦。他依赖着 “低级容器” 的 getBean 功能。而高级容器有更多的功能：支持不同的信息源头，可以访问文件资源，支持应用事件（Observer 模式）。

通常用户看到的就是 “高级容器”。 但 BeanFactory 也非常够用啦！

左边灰色区域的是 “低级容器”， 只负载加载 Bean，获取 Bean。容器其他的高级功能是没有的。例如上图画的 refresh 刷新 Bean 工厂所有配置，生命周期事件回调等。

小结

说了这么多，不知道你有没有理解Spring IoC？ 这里小结一下：IoC 在 Spring 里，只需要低级容器就可以实现，2 个步骤：

1. 加载配置文件，解析成 BeanDefinition 放在 Map 里。
2. 调用 getBean 的时候，从 BeanDefinition 所属的 Map 里，拿出 Class 对象进行实例化，同时，如果有依赖关系，将递归调用 getBean 方法 —— 完成依赖注入。

上面就是 Spring 低级容器（BeanFactory）的 IoC。

至于高级容器 ApplicationContext，他包含了低级容器的功能，当他执行 refresh 模板方法的时候，将刷新整个容器的 Bean。同时其作为高级容器，包含了太多的功能。一句话，他不仅仅是 IoC。他支持不同信息源头，支持 BeanFactory 工具类，支持层级容器，支持访问文件资源，支持事件发布通知，支持接口回调等等。

### 2.4 BeanFactory 和 ApplicationContext有什么区别?

BeanFactory和ApplicationContext是Spring的两大核心接口，都可以当做Spring的容器。其中ApplicationContext是BeanFactory的子接口。

- 依赖关系
  BeanFactory：是Spring里面最底层的接口，包含了各种Bean的定义，读取bean配置文档，管理bean的加载、实例化，控制bean的生命周期，维护bean之间的依赖关系。
  ApplicationContext接口作为BeanFactory的派生，除了提供BeanFactory所具有的功能外，还提供了更完整的框架功能：
  - 继承MessageSource，因此支持国际化。
  - 统一的资源文件访问方式。
  - 提供在监听器中注册bean的事件。
  - 同时加载多个配置文件。
  - 载入多个（有继承关系）上下文 ，使得每一个上下文都专注于一个特定的层次，比如应用的web层。
- 加载方式
  BeanFactroy采用的是延迟加载形式来注入Bean的，即只有在使用到某个Bean时(调用getBean())，才对该Bean进行加载实例化。这样，我们就不能发现一些存在的Spring的配置问题。如果Bean的某一个属性没有注入，BeanFacotry加载后，直至第一次使用调用getBean方法才会抛出异常。
  ApplicationContext，它是在容器启动时，一次性创建了所有的Bean。这样，在容器启动时，我们就可以发现Spring中存在的配置错误，这样有利于检查所依赖属性是否注入。 ApplicationContext启动后预载入所有的单实例Bean，通过预载入单实例bean ,确保当你需要的时候，你就不用等待，因为它们已经创建好了。
  相对于基本的BeanFactory，ApplicationContext 唯一的不足是占用内存空间。当应用程序配置Bean较多时，程序启动较慢。
- 创建方式
  BeanFactory通常以编程的方式被创建，ApplicationContext还能以声明的方式创建，如使用ContextLoader。
- 注册方式
  BeanFactory和ApplicationContext都支持BeanPostProcessor、BeanFactoryPostProcessor的使用，但两者之间的区别是：BeanFactory需要手动注册，而ApplicationContext则是自动注册。

### 2.9 什么是Spring的依赖注入

Spring的依赖注入（Dependency Injection，简称DI）是一种设计模式和软件开发原则，用于管理对象之间的依赖关系。依赖注入的核心思想是将一个对象的依赖关系委托给外部容器（通常是Spring容器）来管理，而不是在对象内部直接创建它所依赖的其他对象。

在依赖注入中，对象不再负责自己的依赖项的创建或查找，而是通过外部容器来提供这些依赖项。

## 3. Spring Beans

### 3.1 什么是Spring Beans

1. 它们是构成用户应用程序主干的对象。
2. Bean 由 Spring IoC 容器管理。
3. 它们由 Spring IoC 容器实例化，配置，装配和管理。
4. Bean 是基于用户提供给容器的配置元数据创建。

### 3.2 spring 提供了哪些配置方式？

- 基于xml配置
- 基于注解配置
- 基于JAVA API配置（常用 @Bean和@Configuration）

```java
@Configuration
public class StudentConfig {
  @Bean
  public StudentBean myStudent() {
    return new StudentBean();
  }
}
```

### 3.6 Spring框架中的单例bean是线程安全的吗？
不是，Spring框架中的单例bean不是线程安全的。

spring 中的 bean 默认是单例模式，spring 框架并没有对单例 bean 进行多线程的封装处理。

实际上大部分时候 spring bean 无状态的（比如 dao 类），所有某种程度上来说 bean 也是安全的，但如果 bean 有状态的话（比如 view model 对象），那就要开发者自己去保证线程安全了，最简单的就是改变 bean 的作用域，把“singleton”变更为“prototype”，这样请求 bean 相当于 new Bean()了，所以就可以保证线程安全了。

- 有状态就是有数据存储功能。
- 无状态就是不会保存数据。

### 3.7 Spring如何处理线程并发问题？
在一般情况下，只有无状态的Bean才可以在多线程环境下共享，在Spring中，绝大部分Bean都可以声明为singleton作用域，因为Spring对一些Bean中非线程安全状态采用ThreadLocal进行处理，解决线程安全问题。

ThreadLocal和线程同步机制都是为了解决多线程中相同变量的访问冲突问题。同步机制采用了“时间换空间”的方式，仅提供一份变量，不同的线程在访问前需要获取锁，没获得锁的线程则需要排队。而ThreadLocal采用了“空间换时间”的方式。

ThreadLocal会为每一个线程提供一个独立的变量副本，从而隔离了多个线程对数据的访问冲突。因为每一个线程都拥有自己的变量副本，从而也就没有必要对该变量进行同步了。ThreadLocal提供了线程安全的共享对象，在编写多线程代码时，可以把不安全的变量封装进ThreadLocal。

### 3.14 使用@Autowired注解自动装配的过程是怎样的？

使用@Autowired注解来自动装配指定的bean。在使用@Autowired注解之前需要在Spring配置文件进行配置，<context:annotation-config />。

在启动spring IoC时，容器自动装载了一个AutowiredAnnotationBeanPostProcessor后置处理器，当容器扫描到@Autowied、@Resource或@Inject时，就会在IoC容器自动查找需要的bean，并装配给该对象的属性。在使用@Autowired时，首先在容器中查询对应类型的bean：

- 如果查询结果刚好为一个，就将该bean装配给@Autowired指定的数据；
- 如果查询的结果不止一个，那么@Autowired会根据名称来查找；
- 如果上述查找的结果为空，那么会抛出异常。解决方法时，使用required=false。

## 4. Spring注解

### 4.1 什么是基于Java的Spring注解配置?
基于Java的配置，允许你在少量的Java注解的帮助下，进行你的大部分Spring配置而非通过XML文件。

以@Configuration 注解为例，它用来标记类可以当做一个bean的定义，被Spring IOC容器使用。

另一个例子是@Bean注解，它表示此方法将要返回一个对象，作为一个bean注册进Spring应用上下文。

### 4.2 怎样开启注解装配？

注解装配在默认情况下是不开启的，为了使用注解装配，我们必须在Spring配置文件中配置 `<context:annotation-config/>`元素。

### 4.5 @Autowired 注解有什么作用？

@Autowired默认是按照类型装配注入的，默认情况下它要求依赖对象必须存在（可以设置它required属性为false）。@Autowired 注解提供了更细粒度的控制，包括在何处以及如何完成自动装配。它的用法和@Required一样，修饰setter方法、构造器、属性或者具有任意名称和/或多个参数的PN方法。

### 4.6 @Autowired和@Resource有什么区别？
@Autowired可用于：构造函数、成员变量、Setter方法

@Autowired和@Resource之间的区别

- @Autowired默认是按照类型装配注入的，默认情况下它要求依赖对象必须存在（可以设置它required属性为false）。
- @Resource默认是按照名称来装配注入的，只有当找不到与名称匹配的bean才会按照类型来装配注入。

## 5. Spring数据访问

### 5.9 Spring支持的事务管理类型是什么？spring 事务实现方式有哪些？
Spring支持两种类型的事务管理：

- 编程式事务管理：
  这意味你通过编程的方式管理事务，给你带来极大的灵活性，但是难维护。
- 声明式事务管理：
  这意味着你可以将业务代码和事务管理分离，你只需用注解和XML配置来管理事务。

### 5.12 说一下 Spring 的事务隔离？
spring 有五大隔离级别，默认值为 ISOLATION_DEFAULT（使用数据库的设置），其他四个隔离级别和数据库的隔离级别一致：

- ISOLATION_DEFAULT：用底层数据库的设置隔离级别，数据库设置的是什么我就用什么；
- ISOLATION_READ_UNCOMMITTED：未提交读，最低隔离级别、事务未提交前，就可被其他事务读取（会出现幻读、脏读、不可重复读）；
- ISOLATION_READ_COMMITTED：提交读，一个事务提交后才能被其他事务读取到（会造成幻读、不可重复读），SQL server 的默认级别；
- ISOLATION_REPEATABLE_READ：可重复读，保证多次读取同一个数据时，其值都和事务开始时候的内容是一致，禁止读取到别的事务未提交的数据（会造成幻读），MySQL 的默认级别；
- ISOLATION_SERIALIZABLE：序列化，代价最高最可靠的隔离级别，该隔离级别能防止脏读、不可重复读、幻读。

**脏读** ：表示一个事务能够读取另一个事务中还未提交的数据。比如，某个事务尝试插入记录 A，此时该事务还未提交，然后另一个事务尝试读取到了记录 A。

**不可重复读** ：是指在一个事务内，多次读同一数据。

**幻读** ：指同一个事务内多次查询返回的结果集不一样。比如同一个事务 A 第一次查询时候有 n 条记录，但是第二次同等条件下查询却有 n+1 条记录，这就好像产生了幻觉。发生幻读的原因也是另外一个事务新增或者删除或者修改了第一个事务结果集里面的数据，同一个记录的数据内容被修改了，所有数据行的记录就变多或者变少了。

## 6. Spring AOP

### 6.1 什么是AOP？
OOP(Object-Oriented Programming)面向对象编程，允许开发者定义纵向的关系，但并适用于定义横向的关系，导致了大量代码的重复，而不利于各个模块的重用。

AOP(Aspect-Oriented Programming)，一般称为面向切面编程，作为面向对象的一种补充，用于将那些与业务无关，但却对多个对象产生影响的公共行为和逻辑，抽取并封装为一个可重用的模块，这个模块被命名为“切面”（Aspect），减少系统中的重复代码，降低了模块间的耦合度，同时提高了系统的可维护性。可用于权限认证、日志、事务处理等。

### 6.2 Spring通知有哪些类型？
在AOP术语中，切面的工作被称为通知，实际上是程序执行时要通过SpringAOP框架触发的代码段。

Spring切面可以应用5种类型的通知：

前置通知（Before）：在目标方法被调用之前调用通知功能；
后置通知（After）：在目标方法完成之后调用通知，此时不会关心方法的输出是什么；
返回通知（After-returning ）：在目标方法成功执行之后调用通知；
异常通知（After-throwing）：在目标方法抛出异常后调用通知；
环绕通知（Around）：通知包裹了被通知的方法，在被通知的方法调用之前和调用之后执行自定义的行为。
同一个aspect，不同advice的执行顺序：

没有异常情况下的执行顺序：
around before advice
before advice
target method 执行
around after advice
after advice
afterReturning
有异常情况下的执行顺序：
around before advice
before advice
target method 执行
around after advice
after advice
afterThrowing:异常发生
java.lang.RuntimeException: 异常发生

## 7、spring的工厂模式

### BeanFactory

相信大家都对BeanFactory不陌生，直接翻译就是Bean工厂。Spring框架的两个关键就是IOC和AOP，其中IOC的设计当然就涉及到BeanFactory；有了Spring框架，我们就很少自己进行对象的创建了，而我们使用到的对象当然就是交给Spring的工厂模式来创建的了。其中BeanFactory是Spring容器的顶层接口，也是Bean工厂最上层的接口，其会有很多工厂实现例如，我们可以把BeanFactory看成是一种工厂方法模式。

![BeanFactory结构及其实现](https://img-blog.csdnimg.cn/d0a2cf67bc5546ae8b8662205dcc6455.png)

从BeanFactory接口的结构我们可以看出，通过getBean重载方法，为我们创建不同的Bean对象，当然其也有很多工厂实现，例如我们用的最多的DefaultListableBeanFactory，还有SimpleJndiBeanFactory、StaticListableBeanFactory等等。Spring工厂模式的应用还加入了反射及配置。通过对各种配置，例如xml，注解等等解析成BeanDefinition，然后根据不同工厂要求通过反射创建不同的Bean对象，这样开发过程中，我们可以将需要创建的对象通过配置等方式交给Bean工厂去完成，使用时直接获取便可。Bean工厂的设计，大大降低耦合度，并增强可扩展性，提高了代码的可维护性，符合设计模式的原则。

### FactoryBean

讲完BeanFactory，再介绍另外一个工厂模式的应用FactoryBean，想必大家也经常会比较这俩。实际上，这两个接口都是用于创建对象，都可以看做是工厂方法模式的实现。BeanFactory是Spring的一个大工厂，创建着Spring框架运行过程中所需要的Bean；而FactoryBean是一个定制化工厂，其会存在于BeanFactory创建对象的过程中，当有需要时，会通过FactoryBean去自定制个性化的Bean，从而Spring框架提高扩展能力。

![img](https://img-blog.csdnimg.cn/6a797e894fcd4f1496ff9438089182db.png)

FactoryBean工厂通过getObject()方法来创建并返回对象，我们可以通过实现FactoryBean来定制化自己需要的Bean对象，因此，FactoryBean是一种典型的工厂方法模式的实现。
我们在引入其他框架整合Spring时，便会有很多桥接整合包，例如mybatis-spring等，其中就会有FactoryBean的实现，例如SqlSessionFactoryBean、MapperFactoryBean等，将需要整合的定制化Bean通过工厂方法的模式，加入进Spring容器中，从而大大降低了耦合程度，也为开发提供了扩展能力。
