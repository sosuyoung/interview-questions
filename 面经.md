# 面经

## 长亮科技

### 1、怎么理解面向对象

### 2、什么是多态

### 3、springboot的开发流程

### 4、mysql中索引的原理

### 5、mysql中常用的关键字

### 6、linux的常用命令

### 7、git的常用命令

## 雪球技术

### 1、快速排序的原理

它的时间复杂度也是 O(nlogn)，但它在时间复杂度为 O(nlogn) 级的几种排序算法中，大多数情况下效率更高，所以快速排序的应用非常广泛

- 从数组中取出一个数，称之为基础（pivot）
- 遍历数组，将比基数大的数字放到它的右边，比基数晓得数字放到它的左边，遍历完成后，数组呗分成了左右两个区域
- 将左右两个区域视为两个数组，重复前两个步骤，直到排序完成

事实上，快速排序的每一次遍历，都将基数摆到了最终位置上。第一轮遍历排好 1 个基数，第二轮遍历排好 2 个基数（每个区域一个基数，但如果某个区域为空，则此轮只能排好一个基数），第三轮遍历排好 4 个基数（同理，最差的情况下，只能排好一个基数），以此类推。总遍历次数为 logn～n 次，每轮遍历的时间复杂度为 O(n)，所以很容易分析出快速排序的时间复杂度为 O(nlogn)～ O(n^2)，平均时间复杂度为 O(nlogn)。

- 递归前先判断当前区域剩余数组是否为0个或者1个，符合的话才执行排序，这样就可以保证递归能够正常退出

```java
if (start >= end) return;
```

- 然后便是最重要的分区算法实现，partition函数，首先要先取出一个基础，我们称之为pirvot，在区间内选择pirvot的时候，随机选择的平均时间复杂度是最优的。
- 使用双指针分区算法，从left开始，遇到比基数大的数，记录其下标；再从right往前便利，找到第一个比基数小的数，记录其下标；然后交换这两个数，继续遍历，直到left和right相遇，交换基数和中间值，返回中间值的下标。

```java
public static void quickSort(int[] arr){
  quickSort(arr, 0, arr.length - 1);
}
public static void quickSort(int[] arr, int start, int end){
  // 如果区域内的数字少于2个，退出递归
  if (start >= end) return;
  // 将数组分区，并获得中间值的下标
  int middle = partition(arr, start, end);
  // 对左边的区域快速排序
  quickSort(arr, start, middle - 1);
  // 对右边的区域快速排序
  quickSort(arr, middle + 1, end);
}
// 将arr从start到end分区，左边区域比基数小，右边区域比基数大，然后返回中间值的下标
public static int partition(int[] arr, int start, int end){
  // 取第一个数为基数
  int pirvot = arr[start];
  // 从第二个数开始分区
  int left = start + 1;
  // 右边界
  int right = end;
  while(left < right){
    // 找到第一个大于基数的位置
    while(left < right && arr[left] <= pirvot) left++;
    // 找到第一个小于基数的位置
    while(left < right && arr[right] >= pirvot) right--;
    // 交换这两个数，使得左边分区都小于或等于基数，右边分区都大于或等于基数
    if(left < right){
      exchange(arr, left, right);
      left++:
      right--;
    }
  }
  // 如果left和right相等，单独比较arr[right]和pirvot
  if(left == right && arr[right] > pirvot) right--;
  exchange(arr, start, right);
  return right;
}
private static void exchange(int[] arr, int i, int j){
  int temp = arr[i];
  arr[i] = arr[j];
  arr[j] = temp;
}
```

- 快速排序是一种不稳定的排序算法，相同数字的相对顺序可能会被修改
- 快速排序的优化：
  - 每轮选择基数的时候，从剩余的数组中随机算则一个数字作为基数
  - 排序之前，使用Collections.shuffle函数进行洗牌（洗牌算法：当数据量小5的时候，shuffle函数直接通过列表的set方法进行洗牌，否则现将list转为array，在进行洗牌，以提高交换效率，洗牌完成后再讲array转成list返回）
  - 排序之前进行判断，如果有序直接返回，如果逆序直接倒序即可。

### 2、给出数组，取出前K个最小元素

### 3、如何定义一个线程

- 集成Thread类
  - 编写简单
  - 可以使用this关键字直接访问当前线程
  - 无法继承其他的类
- 实现runnable接口
  - 可以继承其他的类
  - 多个线程之间可以使用同一个Runnable对象
  - 编程方式稍微复杂，如需访问当前线程，则需要低啊用Thread类的currentThead()方法

### 4、Runnable和callable的区别

### 5、future和futuretask？

Future<V>接口是用来获取异步计算结果的，说白了就是对具体的Runnable或者Callable对象任务执行的结果进行获取(get()),取消(cancel()),判断是否完成等操作，但是我们必须明白Future只是一个接口，我们无法直接创建对象

FutureTask除了实现了Future接口外还实现了Runnable接口（即可以通过Runnable接口实现线程，也可以通过Future取得线程执行完后的结果），因此FutureTask也可以直接提交给Executor执行。

Future是一个接口，代表可以取消的任务，并可以获得任务的执行结果

FutureTask 是基本的实现了Future和runnable接口
           实现runnable接口，说明可以把FutureTask实例传入到Thread中，在一个新的线程中执行。
           实现Future接口，说明可以从FutureTask中通过get取到任务的返回结果，也可以取消任务执行（通过interreput中断）

FutureTask可用于异步获取执行结果或取消执行任务的场景。通过传入Runnable或者Callable的任务给FutureTask，直接调用其run方法或者放入线程池执行，之后可以在外部通过FutureTask的get方法异步获取执行结果，因此，FutureTask非常适合用于耗时的计算，主线程可以在完成自己的任务后，再去获取结果。另外，FutureTask还可以确保即使调用了多次run方法，它都只会执行一次Runnable或者Callable任务，或者通过cancel取消FutureTask的执行等

### 6、如何启动一个线程？

### 7、ArrayList和linkedList的区别

### 8、如何保证ArrayList的线程安全

### 9、copyOnWriteArraylist的原理

​		与ReentrantReadWriteLock 读写锁的思想非常类似，也就是读读共 享、写写互斥、读写互斥、写读互斥。JDK 中提供了 CopyOnWriteArrayList 类比相比于在读写锁的 思想又更进一步。为了将读取的性能发挥到极致，CopyOnWriteArrayList 读取是完全不用加锁的， 并且更厉害的是:写入也不会阻塞读取操作。只有写入和写入之间需要进行同步等待。这样一来，读操 作的性能就会大幅度提升。

CopyOnWriteArrayList 类的所有可变操作(add，set 等等)都是通过创建底层数组的新副本来实现 的。当 List 需要被修改的时候，我并不修改原有内容，而是对原有数据进行一次复制，将修改的内容写 入副本。写完之后，再将修改完的副本替换原来的数据，这样就可以保证写操作不会影响读操作了。

从 CopyOnWriteArrayList 的名字就能看出 CopyOnWriteArrayList 是满足 CopyOnWrite 的 ArrayList，所谓CopyOnWrite 也就是说:在计算机，如果你想要对一块内存进行修改时，我们不在原 有内存块中进行写操作，而是将内存拷贝一份，在新的内存中进行写操作，写完之后呢，就将指向原来 内存指针指向新的内存，原来的内存就可以被回收掉了。

CopyOnWriteArrayList 写入操作 add() 方法在添加集合的时候加了锁，保证了同步，避免了多线程写 的时候会 copy 出多个副本出来。

```java
**
 * Appends the specified element to the end of this list.
 *
 * @param e element to be appended to this list
 * @return {@code true} (as specified by {@link Collection#add})
 */
public boolean add(E e) {
		final ReentrantLock lock = this.lock; 
   	lock.lock();//加锁
		try {
				Object[] elements = getArray();
				int len = elements.length;
				Object[] newElements = Arrays.copyOf(elements, len + 1);//拷贝新数组 newElements[len] = e;
				setArray(newElements);
				return true;
		} finally { 
      	lock.unlock();//释放锁
		} 
 }
```



### 10、springboot的启动原理

### 11、BeanFactory和applicationContext的区别

### 12、java中的LRU算法

LRU（Least Recently Used）是一种常见的页面置换算法，在计算中，所有的文件操作都要放在内存中进行，然而计算机内存大小是固定的，所以我们不可能把所有的文件都加载到内存，因此我们需要制定一种策略对加入到内存中的文件进项选择。

常见的页面置换算法有如下几种：

- LRU 最近最久未使用
- FIFO 先进先出置换算法 类似队列
- OPT 最佳置换算法 （理想中存在的）
- NRU Clock置换算法
- LFU 最少使用置换算法
- PBA 页面缓冲算法

#### LRU原理

LRU（Least recently used，最近最少使用）算法根据数据的历史访问记录来进行淘汰数据，其核心思想是“如果数据最近被访问过，那么将来被访问的几率也更高”。

- 最常见的实现是使用一个链表保存缓存数据，详细算法实现如下

![](/Users/wangyang/Desktop/interview/5682416-3a5d7333c349fd44.webp)

新数据插入到链表头部；

每当缓存命中（即缓存数据被访问），则将数据移到链表头部；

当链表满的时候，将链表尾部的数据丢弃。
 【命中率】
 当存在热点数据时，LRU的效率很好，但偶发性的、周期性的批量操作会导致LRU命中率急剧下降，缓存污染情况比较严重。
 【复杂度】
 实现简单。
 【代价】
 命中时需要遍历链表，找到命中的数据块索引，然后需要将数据移到头部。

#### LRU-K原理

LRU-K中的K代表最近使用的次数，因此LRU可以认为是LRU-1。LRU-K的主要目的是为了解决LRU算法“缓存污染”的问题，其核心思想是将“最近使用过1次”的判断标准扩展为“最近使用过K次”。

相比LRU，LRU-K需要多维护一个队列，用于记录所有缓存数据被访问的历史。只有当数据的访问次数达到K次的时候，才将数据放入缓存。当需要淘汰数据时，LRU-K会淘汰第K次访问时间距当前时间最大的数据。详细实现如下：

![](/Users/wangyang/Desktop/interview/5682416-3225d65c3c8a34a9.webp)

数据第一次被访问，加入到访问历史列表；

如果数据在访问历史列表里后没有达到K次访问，则按照一定规则（FIFO，LRU）淘汰；

当访问历史队列中的数据访问次数达到K次后，将数据索引从历史队列删除，将数据移到缓存队列中，并缓存此数据，缓存队列重新按照时间排序；

缓存数据队列中被再次访问后，重新排序；

需要淘汰数据时，淘汰缓存队列中排在末尾的数据，即：淘汰“倒数第K次访问离现在最久”的数据。
 LRU-K具有LRU的优点，同时能够避免LRU的缺点，实际应用中LRU-2是综合各种因素后最优的选择，LRU-3或者更大的K值命中率会高，但适应性差，需要大量的数据访问才能将历史访问记录清除掉。
 【命中率】
 LRU-K降低了“缓存污染”带来的问题，命中率比LRU要高。
 【复杂度】
 LRU-K队列是一个优先级队列，算法复杂度和代价比较高。
 【代价】
 由于LRU-K还需要记录那些被访问过、但还没有放入缓存的对象，因此内存消耗会比LRU要多；当数据量很大的时候，内存消耗会比较可观。
 LRU-K需要基于时间进行排序（可以需要淘汰时再排序，也可以即时排序），CPU消耗比LRU要高。

### 13、RabbitMQ的工作原理

-----发送消息-----
1、生产者和Broker建立TCP连接。
2、生产者和Broker建立通道。
3、生产者通过通道消息发送给Broker，由Exchange将消息进行转发。
4、Exchange将消息转发到指定的Queue（队列）

----接收消息-----
1、消费者和Broker建立TCP连接
2、消费者和Broker建立通道
3、消费者监听指定的Queue（队列）
4、当有消息到达Queue时Broker默认将消息推送给消费者。
5、消费者接收到消息。

### 14、RabbitMQ的优点

### 15、MyBaits和MyBaits-plus之间的区别

### 16、GC

### 17、Array.copyof，如何实现，是深拷贝还是浅拷贝，有什么区别

System.arraycopy(Object src, int srcPos, Object dest, int destPos, int length)：相对于数组来说是深拷贝（又复制了一份新的数组空间），相对于数组元素来说：只有数组为一维数组，并且元素为基本类型、包装类或String类型时，才是深拷贝；其它都属于浅拷贝；

Arrays.copyOf(int[] original, int newLength)：相对于数组来说是深拷贝（又复制了一份新的数组空间），相对于数组元素来说：只有数组为一维数组，并且元素为基本类型、包装类或String类型时，才是深拷贝；其它都属于浅拷贝；

# 高途课堂

## 1、HashMap聊聊，数据结构？

## 2、自1.8以后除了数据结构，HashMap还有哪些提升？

## 3、loadfactor如何定义，在hashmap中起到什么作用

## 4、HashMap是如何扩容的

## 5、将一个链表倒序，不能使用其他数据结构，不能递归

```java
class Solution {
    public ListNode reverseList(ListNode head) {
        ListNode prev = null;
      	ListNode curr = head;
      	while(curr != null){
          ListNode next = curr.next;
          curr.next = prev;
          prev = curr;
          curr = next;
        }
      return prev;
    }
}
```

## 6、[求一个链表的倒数第K个元素](https://blog.csdn.net/soygrow/article/details/79230275)

## 7、线程池的核心参数，拒绝策略有哪些？

- ThreadPoolExecutor.AbortPolicy：抛出 RejectedExecutionException来拒绝新任务的处理。
- ThreadPoolExecutor.CallerRunsPolicy：调用执行自己的线程运行任务。您不会任务请求。但是这种策略会降低对于新任务提交速度，影响程序的整体性能。另外，这个策略喜欢增加队列容量。如果您的应用程序可以承受此延迟并且你不能任务丢弃任何一个任务请求的话，你可以选择这个策略。
- ThreadPoolExecutor.DiscardPolicy：不处理新任务，直接丢弃掉。
- ThreadPoolExecutor.DiscardOldestPolicy： 此策略将丢弃最早的未处理的任务请求。

## 8、*<u>如果JVM突然中断，你如何保证线程池中的线程执行完任务才最终退出JVM</u>*

## 9、数据库MySQL的数据结构了解哪些？

## 10、数据库的隔离级别有哪些？

- Read Uncommitted（读取未提交内容）
  在该隔离级别，所有事务都可以看到其他未提交事务的执行结果。本隔离级别很少用于实际应用，因为它的性能也不比其他级别好多少。读取未提交的数据，也被称之为脏读（Dirty Read）。
- Read Committed（读取提交内容）
  这是大多数数据库系统的默认隔离级别（但不是 MySQL 默认的）。它满足了隔离的简单定义：一个事务只能看见已经提交事务所做的改变。这种隔离级别也支持所谓的不可重复读（Nonrepeatable Read），因为同一事务的其他实例在该实例处理其间可能会有新的 commit，所以同一 select 可能返回不同结果。
- Repeatable Read（可重读）
  这是 MySQL 的默认事务隔离级别，它确保同一事务的多个实例在并发读取数据时，会看到同样的数据行。不过理论上，这会导致另一个棘手的问题：幻读（Phantom Read）。简单的说，幻读指当用户读取某一范围的数据行时，另一个事务又在该范围内插入了新行，当用户再读取该范围的数据行时，会发现有新的“幻影” 行。InnoDB 和 Falcon 存储引擎通过多版本并发控制（MVCC，Multiversion Concurrency Control 间隙锁）机制解决了该问题。注：其实多版本只是解决不可重复读问题，而加上间隙锁（也就是它这里所谓的并发控制）才解决了幻读问题。
- Serializable（可串行化）
  这是最高的隔离级别，它通过强制事务排序，使之不可能相互冲突，从而解决幻读问题。简言之，它是在每个读的数据行上加上共享锁。在这个级别，可能导致大量的超时现象和锁竞争。

## 11、什么叫做不可重复读？

是指在一个事务内，多次读同一数据。在这个事务还没有结束时，另外一个事务也访问该同一数据。那么，在第一个事务中的两 次读数据之间，由于第二个事务的修改，那么第一个事务两次读到的的数据可能是不一样的。这样就发生了在一个事务内两次读到的数据是不一样的，因此称为是不可重复读。例如，一个编辑人员两次读取同一文档，但在两次读取之间，作者重写了该文档。当编辑人员第二次读取文档时，文档已更改。原始读取不可重复。如果只有在作者全部完成编写后编辑人员才可以读取文档，则可以避免该问题。

## 12、数据库优化，做过哪些？

- SQL的优化

  1) 只返回需要的数据

  a) 不要写SELECT *的语句

  b) 合理写WHERE子句，不要写没有WHERE的SQL语句。

  2) 尽量少做重复的工作

  可以合并一些sql语句

  3) 适当建立索引（不是越多越好）但以下几点会进行全表扫描

  a) 左模糊查询’%...’

  b) 使用了不等操作符！=

  c) Or使用不当，or两边都必须有索引才行

  d) In 、not in

  e) Where子句对字段进行表达式操作

  f) 对于创建的复合索引（从最左边开始组合），查询条件用到的列必须从左边开始不能间隔。否则无效，复合索引的结构与电话簿类似

  g) 全文索引：当于对文件建立了一个以词库为目录的索引（文件大全文索引比模糊匹配效果好）

  能在char、varchar、text类型的列上面创建全文索引

  MySQL 5.6 Innodb引擎也能进行全文索引

  搜索语法：MATCH (列名1, 列名2,…) AGAINST (搜索字符串 [搜索修饰符])

  如果列类型是字符串，但在查询时把一个数值型常量赋值给了一个字符型的列名name，那么虽然在name列上有索引，但是也没有用到。

  4) 使用join代替子查询

  5) 使用union代替手动创建临时表

- 创建索引

  一、创建索引，以下情况不适合建立索引

  l 表记录太少

  l 经常插入、删除、修改的表

  l 数据重复且分布平均的表字段

  二、 复合索引

  如果一个表中的数据在查询时有多个字段总是同时出现则这些字段就可以作为复合索引

## 13、JVM调优做过哪些，调优的参数知道哪些？

- JConsole工具

- **-Xms2g：**
  初始化推大小为 2g；

- **-Xmx2g：**
  堆最大内存为 2g；

- **-XX:NewRatio=4：**
  设置年轻的和老年代的内存比例为 1:4；

- **-XX:SurvivorRatio=8：**
  设置新生代 Eden 和 Survivor 比例为 8:2；

- **–XX:+UseParNewGC：**
  指定使用 ParNew + Serial Old 垃圾回收器组合；

- **-XX:+UseParallelOldGC：**
  指定使用 ParNew + ParNew Old 垃圾回收器组合；

- **-XX:+UseConcMarkSweepGC：**
  指定使用 CMS + Serial Old 垃圾回收器组合；

- **-XX:+PrintGC：**
  开启打印 gc 信息；

- **-XX:+PrintGCDetails：**
  打印 gc 详细信息。

- 设定堆内存大小
  -Xmx：堆内存最大限制。

- 设定新生代大小。 新生代不宜太小，否则会有大量对象涌入老年代
  -XX:NewSize：新生代大小

  -XX:NewRatio 新生代和老生代占比

  -XX:SurvivorRatio：伊甸园空间和幸存者空间的占比

- 设定垃圾回收器
  年轻代用 -XX:+UseParNewGC 年老代用-XX:+UseConcMarkSweepGC

## 14、用过GC么？GC是用来做什么的？

## 15、sychornized和lock的区别，sychornized底层实现原理？

- 相同点
  Lock 能完成 synchronized 所实现的所有功能；
- 不同点
  - Lock 有比 synchronized 更精确的线程语义和更好的性能；
  - synchronized 会自动释放锁，而 Lock 一定要求程序员手工释放，并且必须在finally从句中释放；
  - 通过 Lock 可以知道有没有成功获取锁，而 synchronized 却无法办到
  - Lock 还有更强大的功能，例如，它的 tryLock 方法可以非阻塞方式去拿锁

## 16、堆和栈的区别，了解堆和栈么？

## 17、Java的内存模型了解么？

## 18、锁在JVM中是如何实现的？

## 19、native方式如何实现

## 20、平时做前后端分离是如何做的？

## 21、你们项目中的登录是如何做的？

# 北森云计算

## 1、什么是树

## 2、HashMap的扩容机制

## 3、数组和链表的区别，分别分析一下时间复杂度和空间复杂度

## 4、进程和线程的区别

## 5、synchronized和volatile的区别

## 6、分段锁、自旋锁、互斥锁是什么？

## 7、线程池的七大参数

## 8、什么是CPU的上下文切换

## 9、MySQL两大存储引擎是什么？有什么区别？分别适用于什么场景？

## 10、Spring中的IOC和AOP分别是什么意思？

## 11、微服务的优点和缺点

## 12、localhost和127.0.0.1的区别

## 13、回环地址是什么？

## 14、计算机网络中的五层体系结构？IP和端口处于那一层？

## 15、MAC是指的是什么？

## 16、TCP和UDP的区别

## 17、三次握手的概念？三次握手中的一些关键字？

## 18、http请求头的参数？

## 19、什么是长链接和短连接，连接池？有什么优点？

## 20、http的有哪几种工作状态？

## 21、SWAP分区是什么意思？

# 滴滴

## 1、手写代码（数组合并,并返回深排序）

```java
class User{
  int age;
}
public List<User> mergeSort(List<User> users1, List<User> users2){
  
}
```



## 2、上题，利用最简单的函数合并，排序

## 3、sort排序，核心是什么？那些场景下不使用快速排序？

## 4、Python爬虫是如何做的？

## 5、在做爬虫的时候，如何合理分配多个线程？如何分配不同线程之间工作量

## 6、你做的参与最久的，印象最深的项目？你的具体工作是什么？

## 7、在这个项目中你觉得最复杂的技术或最复杂的业务是什么？这个复杂的业务是如何优化的（可以利用设计模式来优化）

## 8、在多线程的环境下如何保证线程的安全？

## 9、copyOnWriteArrayList实现原理？为什么这样实现？有什么优点？

## 10、Vector实现原理？和copyOnWriteArrayList比较为什么读要加锁？

## 11、mybatis和mybatis-plus的区别？mybaits-plus的亮点是什么？hibernate同样能够实现不写sql为什么还要用mybatis-plus？mybatis中的一对多、多对一、多对多的实现原理？

## 12、mysql索引了解过么？底层的数据结构是如何的？主键索引和唯一索引在数据结构上有什么区别？B+树如何实现索引？

# 数字马力--国际部开发岗--国外支付宝业务

[计算机基础：](/Users/wangyang/Desktop/interview/计算机网络/计算机网络.md)

1. HTTPS 和 HTTP 的区别
2. GET 和 POST 的区别
3. 响应码301和302的区别

[java基础：](/Users/wangyang/Desktop/interview/JAVA基础/JAVA基础.md)

1. 多态原理
2. 泛型的原理（回答的一般）
3. JAVA的四种引用
4. 异常的继承

[JVM：](/Users/wangyang/Desktop/interview/JVM笔记/JVM.md)

1. JVM的划分
2. JVM的类加载过程
3. 双亲委派机制，好处是什么
4. 堆是如何分配内存的（回答的一般）
5. JMM聊一聊，如何保证原子性、一致性、可见性（有一些没回答上来）
6. JVM如何判断一个对象可以被回收（面试官引导后回答出来）
7. JVM垃圾回收机制的算法
8. 用过哪些调优的参数？用过jmap等条用工具么？

[JUC：](/Users/wangyang/Desktop/interview/JUC面试题/JUC.md)

1. 线程的状态
2. 线程如何阻塞和唤醒，sleep和wait的区别
3. synchronized 和 ReentrantLock 区别是什么？两者是公平锁还是非公平锁？
4. 什么是乐观锁和悲观锁
5. 并发编程的三要素是什么？
6. 聊一聊CAS
7. 聊一聊AQS
8. 锁的四种状态
9. ThreadLocal的原理，为什么会内存泄漏，有什么防止？

[HashMap：](/Users/wangyang/Desktop/interview/HashMap.md)

1. 聊一聊HashMap底层
2. 聊一聊ConcurrentHashMap

[Mysql](/Users/wangyang/Desktop/interview/MYSQL/MYSQL.md)

1. InnonDB事务的四大特性
2. InnonDB的隔离级别，分别会造成什么错误
3. mysql如何结局幻读（回答出一半）
4. 给定场景判定使用悲观锁还是乐观锁，具体用sql怎么写
5. mysql的行级锁有哪些
6. InnoDB底层的回表查询和索引覆盖
7. 索引的分类，主键索引和唯一索引的区别
8. 最左匹配原则
9. 哪些情况索引会失效
10. 分库分表聊一聊（聊了不少，提了一些问题，有些能说上来，有些说不上来，面试官会分享了一些知识）

[redis](/Users/wangyang/Desktop/interview/redis/redis.md)

1. redis的两种持久化方式

[消息中间件](/Users/wangyang/Desktop/interview/消息中间件/RabbitMQ.md)（回答的不多，问的也不多）

1. 消息中间件的事务
2. 消息中间件的原理



