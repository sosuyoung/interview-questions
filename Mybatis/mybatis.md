# 1、mybatis工厂模式

在Mybatis中运用工厂模式最典型的就是SqlSessionFactory。

SqlSession是Mybatis中最最最核心的一个模块了。

可以简单的理解，Mybatis中所有的sql都是通过SqlSession来最终执行的。

可以执行jdbc的操作（增删改查）。

SqlSessionFactory就是构建SqlSession对象的一个**工厂类**。

工厂模式用一句话来说就是用来帮你创建对象的。

SqlSessionFactory中有一个openSession(…)方法。

如下所示：

```java
public interface SqlSessionFactory {

  SqlSession openSession();

  SqlSession openSession(boolean autoCommit);

  SqlSession openSession(Connection connection);

  SqlSession openSession(TransactionIsolationLevel level);

  SqlSession openSession(ExecutorType execType);

  SqlSession openSession(ExecutorType execType, boolean autoCommit);

  SqlSession openSession(ExecutorType execType, TransactionIsolationLevel level);

  SqlSession openSession(ExecutorType execType, Connection connection);

  Configuration getConfiguration();
```

可以看到有很多种创建SqlSession的方式。

其中SqlSession openSession(ExecutorType execType);就是一个很典型的应用了工厂模式来达到目的的。

点进去这个SqlSession openSession(ExecutorType execType);到DefaultSqlSessionFactory类中，我们发现又调用了一个：

```java
 @Override
  public SqlSession openSession(ExecutorType execType) {
    return openSessionFromDataSource(execType, null, false);
  }
```

继续点进去：

>  可以看到，真的是见名之意了。功能就是打开会话的

```java
private SqlSession openSessionFromDataSource(ExecutorType execType, TransactionIsolationLevel level, boolean autoCommit) {
    Transaction tx = null;
    try {
      final Environment environment = configuration.getEnvironment();
      final TransactionFactory transactionFactory = getTransactionFactoryFromEnvironment(environment);
      tx = transactionFactory.newTransaction(environment.getDataSource(), level, autoCommit);

      //Executor：SQL语句的执行器
      final Executor executor = configuration.newExecutor(tx, execType);

      return new DefaultSqlSession(configuration, executor, autoCommit);
    } catch (Exception e) {
      closeTransaction(tx); // may have fetched a connection so lets call close()
      throw ExceptionFactory.wrapException("Error opening session.  Cause: " + e, e);
    } finally {
      ErrorContext.instance().reset();
    }
  }
```

代码如下：

>  可以看到，在创建Executor对象的时候，是根据不同的类型，创建不同的对象的。

```java
public Executor newExecutor(Transaction transaction, ExecutorType executorType) {
    executorType = executorType == null ? defaultExecutorType : executorType;
    executorType = executorType == null ? ExecutorType.SIMPLE : executorType;
    Executor executor;
    if (ExecutorType.BATCH == executorType) {
      executor = new BatchExecutor(this, transaction);
    } else if (ExecutorType.REUSE == executorType) {
      executor = new ReuseExecutor(this, transaction);
    } else {
      executor = new SimpleExecutor(this, transaction);
    }
    if (cacheEnabled) {
      executor = new CachingExecutor(executor);
    }
    executor = (Executor) interceptorChain.pluginAll(executor);
    return executor;
  }
```

# 2、mybatis的一级缓存和二级缓存



## mybatis一级缓存

### 一级缓存简介

一级缓存作用域是sqlsession级别的，同一个sqlsession中执行相同的sql查询（相同的sql和参数），第一次会去查询数据库并写到缓存中，第二次从一级缓存中取。

一级缓存是基于 PerpetualCache 的 HashMap 本地缓存，默认打开一级缓存。

### 何时清空一级缓存
如果中间sqlSession去执行commit操作（执行插入、更新、删除），则会清空SqlSession中的一级缓存，这样做的目的为了让缓存中存储的是最新的信息，避免脏读。

一级缓存时执行commit，close，增删改等操作，就会清空当前的一级缓存；当对SqlSession执行更新操作（update、delete、insert）后并执行commit时，不仅清空其自身的一级缓存（执行更新操作的效果），也清空二级缓存（执行commit()的效果）。

一级缓存无过期时间，只有生命周期
MyBatis在开启一个数据库会话时，会创建一个新的SqlSession对象，SqlSession对象中会有一个Executor对象，Executor对象中持有一个PerpetualCache对象，见下面代码。当会话结束时，SqlSession对象及其内部的Executor对象还有PerpetualCache对象也一并释放掉。

## mybatis二级缓存
### 二级缓存简介
它指的是Mybatis中SqlSessionFactory对象的缓存。由同一个SqlSessionFactory对象创建的SqlSession共享其缓存。

二级缓存是 mapper 映射级别的缓存，多个 SqlSession 去操作同一个 Mapper 映射的 sql 语句，多个SqlSession 可以共用二级缓存，二级缓存是跨 SqlSession 的。

### 二级缓存何时存入
在关闭sqlsession后(close)，才会把该sqlsession一级缓存中的数据添加到namespace的二级缓存中。

开启了二级缓存后，还需要将要缓存的pojo实现Serializable接口，为了将缓存数据取出执行反序列化操作，因为二级缓存数据存储介质多种多样，不一定只存在内存中，有可能存在硬盘中。

二级缓存有过期时间，但没有后台线程进行检测
需要注意的是，并不是key-value的过期时间，而是这个cache的过期时间，是flushInterval，意味着整个清空缓存cache，所以不需要后台线程去定时检测。

每当存取数据的时候，都有检测一下cache的生命时间，默认是1小时，如果这个cache存活了一个小时，那么将整个清空一下。

当 Mybatis 调用 Dao 层查询数据库时，先查询二级缓存，二级缓存中无对应数据，再去查询一级缓存，一级缓存中也没有，最后去数据库查找。

# 3、MyBatis 核心组件：

在解读源码之前，我们很有必要先了解 MyBatis 几大核心组件，知道他们都是做什么用的。

核心组件有：Configuration、SqlSession、Executor、StatementHandler、ParameterHandler、ResultSethandler。

下面简单介绍一下他们：

- Configuration：用于描述 MyBatis 主配置文件信息，MyBatis 框架在启动时会加载主配置文件，将配置信息转换为 Configuration 对象。
- SqlSession：面向用户的 API，是 MyBatis 与数据库交互的接口。
- Executor：SQL 执行器，用于和数据库交互。SqlSession 可以理解为 Executor 组件的外观（外观模式），真正执行 SQL 的是 Executor 组件。
- MappedStatement：用于描述 SQL 配置信息，MyBatis 框架启动时，XML 文件或者注解配置的 SQL 信息会被转换为 MappedStatement 对象注册到 Configuration 组件中。
- StatementHandler：封装了对 JDBC 中 Statement 对象的操作，包括为 Statement 参数占位符设置值，通过 Statement 对象执行 SQL 语句。
- TypeHandler：类型处理器，用于 Java 类型与 JDBC 类型之间的转换。
- ParameterHandler：用于处理 SQL 中的参数占位符，为参数占位符设置值。
- ResultSetHandler：封装了对 ResultSet 对象的处理逻辑，将结果集转换为 Java 实体对象。

# 4、简述 Mapper 执行流程：

SqlSession组件，它是用户层面的API。用户可利用 SqlSession 获取想要的 Mapper 对象（MapperProxy 代理对象）；当执行 Mapper 的方法，MapperProxy 会创建对应的 MapperMetohd，然后 MapperMethod 底层其实是利用 SqlSession 来执行 SQL。

但是真正执行 SQL 操作的应该是 Executor组 件，Executor 可以理解为 SQL 执行器，它会使用 StatementHandler 组件对 JDBC 的 Statement 对象进行操作。当 Statement 类型为 CallableStatement 和 PreparedStatement 时，会通过 ParameterHandler 组件为参数占位符赋值。

ParameterHandler 组件中会根据 Java 类型找到对应的 TypeHandler 对象，TypeHandler 中会通过 Statement 对象提供的 setXXX() 方法（例如setString()方法）为 Statement 对象中的参数占位符设置值。

StatementHandler 组件使用 JDBC 中的 Statement 对象与数据库完成交互后，当 SQL 语句类型为 SELECT 时，MyBatis 通过 ResultSetHandler 组件从 Statement 对象中获取 ResultSet 对象，然后将 ResultSet 对象转换为 Java 对象。

我们可以用一幅图来描述上面各个核心组件之间的关系：

![MyBatis 各大组件关系](https://img-blog.csdnimg.cn/20200527161433937.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L0hvd2luZnVu,size_16,color_FFFFFF,t_70)

# 5、Redis缓存问题

1、缓存穿透

- 缓存穿透是指查询一个一定不存在的数据，由于缓存是不命中，将去查询数据库，但是数 据库也无此记录，我们没有将这次查询的 null 写入缓存，这将导致这个不存在的数据每次 请求都要到存储层去查询，失去了缓存的意义。
- 在流量大时，可能DB就挂掉了，要是有人利用不存在的key频繁攻击我们的应用，这就是 漏洞。
- 解决: 缓存空结果、并且设置短的过期时间。

2、缓存雪崩

- 缓存雪崩是指在我们设置缓存时采用了相同的过期时间，导致缓存在某一时刻同时失

  效，请求全部转发到 DB，DB 瞬时压力过重雪崩。

- 解决:

  原有的失效时间基础上增加一个随机值，比如 1-5 分钟随机，这样每一个缓存的过期时间的 重复率就会降低，就很难引发集体失效的事件。

3、缓存击穿

-  对于一些设置了过期时间的key，如果这些key可能会在某些时间点被超高并发地访问， 是一种非常“热点”的数据。
-  这个时候，需要考虑一个问题:如果这个key在大量请求同时进来前正好失效，那么所 有对这个 key 的数据查询都落到 db，我们称为缓存击穿。
-  解决: 加锁

# 6、MyBatis动态sql有什么用？执行原理？有哪些动态sql？

MyBatis动态sql可以在Xml映射文件内，以标签的形式编写动态sql，执行原理是根据表达式的值 完成逻辑判断 并动态拼接sql的功能。

MyBatis提供了9种动态sql标签：trim | where | set | foreach | if | choose | when | otherwise | bind。

# 7、MyBatis是如何将sql执行结果封装为目标对象并返回的？都有哪些映射形式？

- **第一种**是使用标签，逐一定义数据库列名和对象属性名之间的映射关系。
- **第二种**是使用sql列的别名功能，将列的别名书写为对象属性名。
- 有了列名与属性名的映射关系后，MyBatis通过反射创建对象，同时使用反射给对象的属性逐一赋值并返回，那些找不到映射关系的属性，是无法完成赋值的。

# 8、一对一、一对多的关联查询 ？

一对一关联查询

```xml
<mapper namespace="com.lcb.mapping.userMapper">  
  <select id="getClass" parameterType="int" resultMap="ClassesResultMap">  
    select * from class c,teacher t where c.teacher_id=t.t_id and c.c_id=#{id}  
  </select>  
  <resultMap type="com.lcb.user.Classes" id="ClassesResultMap">  
    <!-- 实体类的字段名和数据表的字段名映射 -->  
    <id property="id" column="c_id"/>  
    <result property="name" column="c_name"/>  
    <association property="teacher" javaType="com.lcb.user.Teacher">  
      <id property="id" column="t_id"/>  
      <result property="name" column="t_name"/>  
    </association>  
  </resultMap>  
</mapper> 
```

一对多关联查询

```xml
<mapper namespace="com.lcb.mapping.userMapper">  
  <!--collection  一对多关联查询 -->  
  <select id="getClass2" parameterType="int" resultMap="ClassesResultMap2">  
    select * from class c,teacher t,student s where c.teacher_id=t.t_id and c.c_id=s.class_id and c.c_id=#{id}  
  </select>  
  <resultMap type="com.lcb.user.Classes" id="ClassesResultMap2">  
    <id property="id" column="c_id"/>  
    <result property="name" column="c_name"/>  
    <association property="teacher" javaType="com.lcb.user.Teacher">  
      <id property="id" column="t_id"/>  
      <result property="name" column="t_name"/>  
    </association>  
    <collection property="student" ofType="com.lcb.user.Student">  
      <id property="id" column="s_id"/>  
      <result property="name" column="s_name"/>  
    </collection>  
  </resultMap>  
</mapper>
```

## MyBatis实现一对一有几种方式?具体怎么操作的？
有联合查询和嵌套查询,联合查询是几个表联合查询,只查询一次, 通过在resultMap里面配置association节点配置一对一的类就可以完成；

嵌套查询是先查一个表，根据这个表里面的结果的 外键id，去再另外一个表里面查询数据,也是通过association配置，但另外一个表的查询通过select属性配置。

## MyBatis实现一对多有几种方式,怎么操作的？
有联合查询和嵌套查询。

联合查询是几个表联合查询,只查询一次,通过在resultMap里面的collection节点配置一对多的类就可以完成；

嵌套查询是先查一个表,根据这个表里面的 结果的外键id,去再另外一个表里面查询数据,也是通过配置collection,但另外一个表的查询通过select节点配置。
