```java
package cn.ibiandan.cloud.utils;  
  
import java.math.BigDecimal;  
import java.math.RoundingMode;  
import java.text.DecimalFormat;  
  
/**  
 * @ClassName: KqzlUtil * @Description: 计算AQI工具类  
 * @Author: 王洋  
 * @Email: wangyang@ibiandan.cn  
 * @Date: 2023-11-13 16:03 * @Version: 1.0 **/public class AirQualityUtil {  
    /**  
     * 计算IAQI的值  
     * 浓度值，IAQI低，IAQI高，低值，高值  
     *  
     * @param value 监测浓度值  
     * @param lowI  与lowL相对应的IAQI值  
     * @param highI 与highL相对应的IAQI值  
     * @param lowC  与监测浓度相近的污染物浓度低位值  
     * @param highC 与监测浓度相近的污染物浓度高位值  
     * @return (I高 - I低)/(C高-C低)*(C-C低)+I低  
     */  
    private static double IAQICal(double value, int lowI, int highI, int lowC, int highC) {  
        if (value < 0 || highI < 0 || lowI < 0 || highC < 0 || lowC < 0) {  
            return -99;  
        }  
        if (highC == lowC) {  
            return lowI;  
        }  
        return new BigDecimal((value - lowC) * (highI - lowI) / (highC - lowC) + lowI).setScale(1, RoundingMode.HALF_UP).doubleValue();  
    }  
  
    /**  
     * 计算pm2.5的IAQI  
     *     * @param pm25_value pm2.5浓度值  
     * @return double  
     */    public static double PM25IAQI(double pm25_value) {  
        double IAQI_PM25;  
        // 计算pm2.5的iaqi值  
        if (0 <= pm25_value && pm25_value < 35) {  
            // 浓度值，IAQI低，IAQI高，低值，高值  
            IAQI_PM25 = IAQICal(pm25_value, 0, 50, 0, 35); //调用函数（计算公式），同时也相当于赋值  
        } else if (35 <= pm25_value && pm25_value < 75) {  
            IAQI_PM25 = IAQICal(pm25_value, 50, 100, 35, 75); //调用函数（计算公式），同时也相当于赋值  
        } else if (75 <= pm25_value && pm25_value < 115) {  
            IAQI_PM25 = IAQICal(pm25_value, 100, 150, 75, 115); //调用函数（计算公式），同时也相当于赋值  
        } else if (115 <= pm25_value && pm25_value < 150) {  
            IAQI_PM25 = IAQICal(pm25_value, 150, 200, 115, 150); //调用函数（计算公式），同时也相当于赋值  
        } else if (150 <= pm25_value && pm25_value < 250) {  
            IAQI_PM25 = IAQICal(pm25_value, 200, 300, 150, 250);  
        } else if (250 <= pm25_value && pm25_value < 350) {  
            IAQI_PM25 = IAQICal(pm25_value, 300, 400, 250, 350);  
        } else if (350 <= pm25_value && pm25_value < 500) {  
            IAQI_PM25 = IAQICal(pm25_value, 400, 500, 350, 500);  
        } else {  
            IAQI_PM25 = -99;  
        }  
        return IAQI_PM25;  
    }  
  
    /**  
     * 计算pm10的IAQI  
     *     * @param pm10_value pm10浓度值  
     * @return double  
     */    public static double PM10IAQI(double pm10_value) {  
        double IAQI_PM10;  
        // 计算pm10的iaqi值  
        if (0 <= pm10_value && pm10_value < 50) {  
            // 浓度值，IAQI低，IAQI高，低值，高值  
            IAQI_PM10 = IAQICal(pm10_value, 0, 50, 0, 50); //调用函数（计算公式），同时也相当于赋值  
        } else if (50 <= pm10_value && pm10_value < 150) {  
            IAQI_PM10 = IAQICal(pm10_value, 50, 100, 50, 150); //调用函数（计算公式），同时也相当于赋值  
        } else if (150 <= pm10_value && pm10_value < 250) {  
            IAQI_PM10 = IAQICal(pm10_value, 100, 150, 150, 250); //调用函数（计算公式），同时也相当于赋值  
        } else if (250 <= pm10_value && pm10_value < 350) {  
            IAQI_PM10 = IAQICal(pm10_value, 150, 200, 250, 350); //调用函数（计算公式），同时也相当于赋值  
        } else if (350 <= pm10_value && pm10_value < 420) {  
            IAQI_PM10 = IAQICal(pm10_value, 200, 300, 350, 420);  
        } else if (420 <= pm10_value && pm10_value < 500) {  
            IAQI_PM10 = IAQICal(pm10_value, 300, 400, 420, 500);  
        } else if (500 <= pm10_value && pm10_value < 600) {  
            IAQI_PM10 = IAQICal(pm10_value, 400, 500, 500, 600);  
        } else {  
            IAQI_PM10 = -99;  
        }  
        return IAQI_PM10;  
    }  
  
    /**  
     * 计算co的IAQI  
     *     * @param co_value co浓度值（日数据）  
     * @return double  
     */    public static double COIAQI(double co_value) {  
        double IAQI_CO;  
        // 计算co的iaqi值  
        if (0 <= co_value && co_value < 2) {  
            // 浓度值，IAQI低，IAQI高，低值，高值  
            IAQI_CO = IAQICal(co_value, 0, 50, 0, 2); //调用函数（计算公式），同时也相当于赋值  
        } else if (2 <= co_value && co_value < 4) {  
            IAQI_CO = IAQICal(co_value, 50, 100, 2, 4); //调用函数（计算公式），同时也相当于赋值  
        } else if (4 <= co_value && co_value < 14) {  
            IAQI_CO = IAQICal(co_value, 100, 150, 4, 14); //调用函数（计算公式），同时也相当于赋值  
        } else if (14 <= co_value && co_value < 24) {  
            IAQI_CO = IAQICal(co_value, 150, 200, 14, 24); //调用函数（计算公式），同时也相当于赋值  
        } else if (24 <= co_value && co_value < 36) {  
            IAQI_CO = IAQICal(co_value, 200, 300, 24, 36);  
        } else if (36 <= co_value && co_value < 48) {  
            IAQI_CO = IAQICal(co_value, 300, 400, 36, 48);  
        } else if (48 <= co_value && co_value < 60) {  
            IAQI_CO = IAQICal(co_value, 400, 500, 48, 60);  
        } else {  
            IAQI_CO = -99;  
        }  
        return IAQI_CO;  
    }  
  
    /**  
     * 计算so2的IAQI  
     *     * @param so2_value so2浓度值（日数据）  
     * @return double  
     */    public static double SO2IAQI(double so2_value) {  
        double IAQI_SO2;  
        // 计算so2的iaqi值  
        if (0 <= so2_value && so2_value < 50) {  
            // 浓度值，IAQI低，IAQI高，低值，高值  
            IAQI_SO2 = IAQICal(so2_value, 0, 50, 0, 50); //调用函数（计算公式），同时也相当于赋值  
        } else if (50 <= so2_value && so2_value < 150) {  
            IAQI_SO2 = IAQICal(so2_value, 50, 100, 50, 150); //调用函数（计算公式），同时也相当于赋值  
        } else if (150 <= so2_value && so2_value < 475) {  
            IAQI_SO2 = IAQICal(so2_value, 100, 150, 150, 475); //调用函数（计算公式），同时也相当于赋值  
        } else if (475 <= so2_value && so2_value < 800) {  
            IAQI_SO2 = IAQICal(so2_value, 150, 200, 475, 800); //调用函数（计算公式），同时也相当于赋值  
        } else if (800 <= so2_value && so2_value < 1600) {  
            IAQI_SO2 = IAQICal(so2_value, 200, 300, 800, 1600);  
        } else if (1600 <= so2_value && so2_value < 2100) {  
            IAQI_SO2 = IAQICal(so2_value, 300, 400, 1600, 2100);  
        } else if (2100 <= so2_value && so2_value < 2620) {  
            IAQI_SO2 = IAQICal(so2_value, 400, 500, 1600, 2620);  
        } else {  
            IAQI_SO2 = -99;  
        }  
        return IAQI_SO2;  
    }  
  
    /**  
     * 计算o3的IAQI  
     *     * @param o3_value o3浓度值（日数据）  
     * @return double  
     */    public static double O3IAQI(double o3_value) {  
        double IAQI_O3;  
        // 计算03的iaqi值  
        if (0 <= o3_value && o3_value < 100) {  
            // 浓度值，IAQI低，IAQI高，低值，高值  
            IAQI_O3 = IAQICal(o3_value, 0, 50, 0, 100); //调用函数（计算公式），同时也相当于赋值  
        } else if (100 <= o3_value && o3_value < 160) {  
            IAQI_O3 = IAQICal(o3_value, 50, 100, 100, 160); //调用函数（计算公式），同时也相当于赋值  
        } else if (160 <= o3_value && o3_value < 215) {  
            IAQI_O3 = IAQICal(o3_value, 100, 150, 160, 215); //调用函数（计算公式），同时也相当于赋值  
        } else if (215 <= o3_value && o3_value < 265) {  
            IAQI_O3 = IAQICal(o3_value, 150, 200, 215, 265); //调用函数（计算公式），同时也相当于赋值  
        } else if (265 <= o3_value && o3_value < 800) {  
            IAQI_O3 = IAQICal(o3_value, 200, 300, 265, 800);  
        } else {  
            IAQI_O3 = -99;  
        }  
        return IAQI_O3;  
    }  
  
    /**  
     * 计算no2的IAQI  
     *     * @param no2_value no2浓度值（日数据）  
     * @return double  
     */    public static double NO2IAQI(double no2_value) {  
        double IAQI_NO2;  
        // 计算no2的iaqi值  
        if (0 <= no2_value && no2_value < 40) {  
            // 浓度值，IAQI低，IAQI高，低值，高值  
            IAQI_NO2 = IAQICal(no2_value, 0, 50, 0, 40); //调用函数（计算公式），同时也相当于赋值  
        } else if (40 <= no2_value && no2_value < 80) {  
            IAQI_NO2 = IAQICal(no2_value, 50, 100, 40, 80); //调用函数（计算公式），同时也相当于赋值  
        } else if (80 <= no2_value && no2_value < 180) {  
            IAQI_NO2 = IAQICal(no2_value, 100, 150, 80, 180); //调用函数（计算公式），同时也相当于赋值  
        } else if (180 <= no2_value && no2_value < 280) {  
            IAQI_NO2 = IAQICal(no2_value, 150, 200, 180, 280); //调用函数（计算公式），同时也相当于赋值  
        } else if (280 <= no2_value && no2_value < 565) {  
            IAQI_NO2 = IAQICal(no2_value, 200, 300, 280, 565);  
        } else if (565 <= no2_value && no2_value < 754) {  
            IAQI_NO2 = IAQICal(no2_value, 300, 400, 565, 754); //调用函数（计算公式），同时也相当于赋值  
        } else if (754 <= no2_value && no2_value < 940) {  
            IAQI_NO2 = IAQICal(no2_value, 400, 500, 754, 940);  
        } else {  
            IAQI_NO2 = -99;  
        }  
        return IAQI_NO2;  
    }  
    //------------------------空气质量小时数据计算  
  
    /**  
     * 计算co的IAQI  
     *     * @param co_value co浓度值（小时数据）  
     * @return double  
     */    public static double COIAQIForHour(double co_value) {  
        double IAQI_C0;  
        // 计算co的iaqi值  
        if (0 <= co_value && co_value < 5) {  
            // 浓度值，IAQI低，IAQI高，低值，高值  
            IAQI_C0 = IAQICal(co_value, 0, 50, 0, 5); //调用函数（计算公式），同时也相当于赋值  
        } else if (5 <= co_value && co_value < 10) {  
            IAQI_C0 = IAQICal(co_value, 50, 100, 5, 10); //调用函数（计算公式），同时也相当于赋值  
        } else if (10 <= co_value && co_value < 35) {  
            IAQI_C0 = IAQICal(co_value, 100, 150, 10, 35); //调用函数（计算公式），同时也相当于赋值  
        } else if (35 <= co_value && co_value < 60) {  
            IAQI_C0 = IAQICal(co_value, 150, 200, 35, 60); //调用函数（计算公式），同时也相当于赋值  
        } else if (60 <= co_value && co_value < 90) {  
            IAQI_C0 = IAQICal(co_value, 200, 300, 60, 90);  
        } else if (90 <= co_value && co_value < 120) {  
            IAQI_C0 = IAQICal(co_value, 300, 400, 90, 120);  
        } else if (120 <= co_value && co_value < 150) {  
            IAQI_C0 = IAQICal(co_value, 400, 500, 120, 150);  
        } else {  
            IAQI_C0 = -99;  
        }  
        return IAQI_C0;  
    }  
  
    /**  
     * 计算so2的IAQI  
     *     * @param so2_value so2浓度值（小时数据）  
     * @return double  
     */    public static double SO2IAQIForHour(double so2_value) {  
        double IAQI_SO2;  
        // 计算so2的iaqi值  
        if (0 <= so2_value && so2_value < 150) {  
            // 浓度值，IAQI低，IAQI高，低值，高值  
            IAQI_SO2 = IAQICal(so2_value, 0, 50, 0, 150); //调用函数（计算公式），同时也相当于赋值  
        } else if (150 <= so2_value && so2_value < 500) {  
            IAQI_SO2 = IAQICal(so2_value, 50, 100, 150, 500); //调用函数（计算公式），同时也相当于赋值  
        } else if (500 <= so2_value && so2_value < 650) {  
            IAQI_SO2 = IAQICal(so2_value, 100, 150, 500, 650); //调用函数（计算公式），同时也相当于赋值  
        } else if (650 <= so2_value && so2_value < 800) {  
            IAQI_SO2 = IAQICal(so2_value, 150, 200, 650, 800); //调用函数（计算公式），同时也相当于赋值  
        } else {  
            IAQI_SO2 = -99;  
        }  
        return IAQI_SO2;  
    }  
  
    /**  
     * 计算o3的IAQI  
     *     * @param o3_value o3浓度值（小时数据）  
     * @return double  
     */    public static double O3IAQIForHour(double o3_value) {  
        double IAQI_03;  
        // 计算03的iaqi值  
        if (0 <= o3_value && o3_value < 160) {  
            // 浓度值，IAQI低，IAQI高，低值，高值  
            IAQI_03 = IAQICal(o3_value, 0, 50, 0, 160); //调用函数（计算公式），同时也相当于赋值  
        } else if (160 <= o3_value && o3_value < 200) {  
            IAQI_03 = IAQICal(o3_value, 50, 100, 160, 200); //调用函数（计算公式），同时也相当于赋值  
        } else if (200 <= o3_value && o3_value < 300) {  
            IAQI_03 = IAQICal(o3_value, 100, 150, 200, 300); //调用函数（计算公式），同时也相当于赋值  
        } else if (300 <= o3_value && o3_value < 400) {  
            IAQI_03 = IAQICal(o3_value, 150, 200, 300, 400); //调用函数（计算公式），同时也相当于赋值  
        } else if (400 <= o3_value && o3_value < 800) {  
            IAQI_03 = IAQICal(o3_value, 200, 300, 400, 800);  
        } else if (800 <= o3_value && o3_value < 1000) {  
            IAQI_03 = IAQICal(o3_value, 300, 400, 800, 1000); //调用函数（计算公式），同时也相当于赋值  
        } else if (1000 <= o3_value && o3_value < 1200) {  
            IAQI_03 = IAQICal(o3_value, 400, 500, 1000, 1200);  
        } else {  
            IAQI_03 = -99;  
        }  
        return IAQI_03;  
    }  
  
    /**  
     * 计算no2的IAQI  
     *     * @param no2_value no2浓度值（小时数据）  
     * @return double  
     */    public static double NO2IAQIFourHour(double no2_value) {  
        double IAQI_NO2;  
        // 计算no2的iaqi值  
        if (0 <= no2_value && no2_value < 100) {  
            // 浓度值，IAQI低，IAQI高，低值，高值  
            IAQI_NO2 = IAQICal(no2_value, 0, 50, 0, 100); //调用函数（计算公式），同时也相当于赋值  
        } else if (100 <= no2_value && no2_value < 200) {  
            IAQI_NO2 = IAQICal(no2_value, 50, 100, 100, 200); //调用函数（计算公式），同时也相当于赋值  
        } else if (200 <= no2_value && no2_value < 700) {  
            IAQI_NO2 = IAQICal(no2_value, 100, 150, 200, 700); //调用函数（计算公式），同时也相当于赋值  
        } else if (700 <= no2_value && no2_value < 1200) {  
            IAQI_NO2 = IAQICal(no2_value, 150, 200, 700, 1200); //调用函数（计算公式），同时也相当于赋值  
        } else if (1200 <= no2_value && no2_value < 2340) {  
            IAQI_NO2 = IAQICal(no2_value, 200, 300, 1200, 2340);  
        } else if (2340 <= no2_value && no2_value < 3090) {  
            IAQI_NO2 = IAQICal(no2_value, 300, 400, 2340, 3090); //调用函数（计算公式），同时也相当于赋值  
        } else if (3090 <= no2_value && no2_value < 3840) {  
            IAQI_NO2 = IAQICal(no2_value, 400, 500, 3090, 3840);  
        } else {  
            IAQI_NO2 = -99;  
        }  
        return IAQI_NO2;  
    }  
  
    /**  
     * 根据IAQI计算空气质量等级  
     * 一级，优  
     * 二级，良  
     * 三级，轻度污染  
     * 四级，中度污染  
     * 五级，重度污染  
     * 六级，严重污染  
     *  
     * @param value IAQI值  
     * @return 1-6对应空气质量一级到六级  
     */  
    public static int airQualityClass(int value) {  
        int dj;  
        if (0 <= value && value <= 50) {  
            dj = 1;  
        } else if (51 <= value && value <= 100) {  
            dj = 2;  
        } else if (101 <= value && value <= 150) {  
            dj = 3;  
        } else if (151 <= value && value <= 200) {  
            dj = 4;  
        } else if (201 <= value && value <= 300) {  
            dj = 5;  
        } else if (value > 300) {  
            dj = 6;  
        } else {  
            // 有的数据可能数-99  
            //-99代表数字超出计算范围了，应该作废的，我这里不做其他处理直接6级  
            dj = 6;  
        }  
        return dj;  
    }  
}
```