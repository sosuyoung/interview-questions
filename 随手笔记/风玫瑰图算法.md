```java
    private static Map<String, List<Double>> windData;  
  
    public static void WindRoseData() {  
        windData = new HashMap<>();  
        // Initialize the map with the 16 wind directions  
        String[] directions = {"N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"};  
        for (String direction : directions) {  
            windData.put(direction, new ArrayList<>());  
        }  
    }  
  
    public static void addData(double direction, double speed) {  
        String windDirection = computeWindDirection(direction);  
        windData.get(windDirection).add(speed);  
    }  
  
    private static String computeWindDirection(double direction) {  
        String[] sectors = {"N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW", "N"};  
        return sectors[(int) Math.round(((direction % 360) / 22.5))];  
    }  
  
    public static Map<String, Map<String, Double>> computeProbabilities() {  
        Map<String, Map<String, Double>> probabilities = new HashMap<>();  
        for (Map.Entry<String, List<Double>> entry : windData.entrySet()) {  
            Map<String, Double> speedProbabilities = new HashMap<>();  
            long count0to2 = entry.getValue().stream().filter(speed -> speed >= 0 && speed < 2).count();  
            long count2to5 = entry.getValue().stream().filter(speed -> speed >= 2 && speed < 5).count();  
            long count5to8 = entry.getValue().stream().filter(speed -> speed >= 5 && speed < 8).count();  
            long count8to10 = entry.getValue().stream().filter(speed -> speed >= 8 && speed <= 10).count();  
            long totalDataPoints = entry.getValue().size();  
            speedProbabilities.put("0-2m", (double) count0to2 / totalDataPoints);  
            speedProbabilities.put("2-5m", (double) count2to5 / totalDataPoints);  
            speedProbabilities.put("5-8m", (double) count5to8 / totalDataPoints);  
            speedProbabilities.put("8-10m", (double) count8to10 / totalDataPoints);  
            probabilities.put(entry.getKey(), speedProbabilities);  
        }  
        return probabilities;  
    }  
  
    public static void main(String[] args) {  
        int[] Met_Dir = {33, 34, 36, 31, 115, 84,135,89,103,139,138,115};  
        double[] Met_Speed = {1.5, 1.1, 0.3, 0.3, 0.7, 0.9,1.5,0.7,2.1,1.7,2.6,1.1};  
        WindRoseData();  
        for (int i = 0; i < Met_Dir.length; i++) {  
            addData(Met_Dir[i], Met_Speed[i]);  
        }  
        Map<String, Map<String, Double>> probabilities = computeProbabilities();  
        System.out.println("风向区间\t风速区间\t概率");  
        int i = 1;  
        for (Map.Entry<String, Map<String, Double>> entry : probabilities.entrySet()) {  
            for (Map.Entry<String, Double> speedEntry : entry.getValue().entrySet()) {  
                System.out.printf("%s\t\t%s\t\t%.2f\n", i, speedEntry.getKey(), speedEntry.getValue());  
            }  
            i++;  
        }  
    }
```