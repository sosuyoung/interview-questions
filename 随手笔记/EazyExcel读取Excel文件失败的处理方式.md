## 问题描述：
	在SpringBoot和EazyExcel环境下读取Excel文件，在本地开发的时候一切正常，但是当项目部署到线上的时候，出现了问题。
## 问题一：java.io.FileNotFoundException: no such entry: "Workbook", had: [Book]
	该问题的原因是因为服务器的环境问题，我遇到的服务器是一个windows7系统，经过排查后，发现该电脑上没有安装Office组件，只安装了wps组件，这里出现这个问题，应该是应为本地没有高于2007以上的Excel组件。
	这里可以自行百度一下如何检查一下本地的COM组件
	服务器上的文件类型均为2003的版本，但是由于需要2003版本的旧文件也需要读取其中的数据，所以这里我在代码中加入前置处理
	因为2003的版本的文件类型为.xls，所以这里将所有读取xls的文件全部通过代码另存为xlsx格式
	这里需要注意一下，new Variant(51)中的51就是表示需要转换的文件类型，这里我们用的xlsx，所以用51，如果需要其他类型，可以参考结尾的表格！
```java
public File convertFileTypes(String inputFilePath) {  
  
    // 实例化ComThread线程与ActiveXComponent  
    ComThread.InitSTA();  
    ActiveXComponent wordApp = new ActiveXComponent("Excel.Application");  
    String outInputFilePath = System.getProperty("user.dir") + "\\" + System.currentTimeMillis() + ".xlsx";  
    try {  
        // 文件隐藏时进行应用操作  
        wordApp.setProperty("Visible", new Variant(false));  
        // 实例化模板Workbooks对象  
        Dispatch workbooks = wordApp.getProperty("Workbooks").toDispatch();  
        // 打开Workbooks进行另存为操作  
        Dispatch doc = Dispatch.invoke(workbooks, "Open", Dispatch.Method, new Object[]{inputFilePath, new Variant(true), new Variant(true)}, new int[1]).toDispatch();  
        Dispatch.invoke(doc, "SaveAs", Dispatch.Method, new Object[]{outInputFilePath, new Variant(51)}, new int[1]);  
        Dispatch.call(doc, "Close", new Variant(false));  
        logBox.appendText("转换文件格式成功!\n");  
        return new File(outInputFilePath);  
    } catch (Exception e) {  
        log.error("转换文件格式失败!\n", e);  
        logBox.appendText("转换文件格式失败!\n");  
        throw e;  
    } finally {  
        // 释放线程与ActiveXComponent  
        wordApp.invoke("Quit", new Variant[]{});  
        ComThread.Release();  
    }  
}
```

## 问题二：org.apache.poi.hssf.OldExcelFormatException: The supplied spreadsheet seems to be Excel 5.0/7.0 (BIFF5) format. POI only supports BIFF8 format (from Excel versions 97/2000/XP/2003)
	出现这个问题是因为在强制转文件格式的时候，源文件是BIFF5格式的，但是POI目前仅仅支持BIFF8格式的，所以同样需要问题一的方法进行另存为

## 问题三：jacob 报错 Can't co-create object

	问题一中使用额是jacob进行文件另存为的操作，但是在我实际运用中，又遇到了这个问题，
	后来经过排查后，发现是因为服务器上没有安装office的组件
	于是需要在服务器上安装上office组件，安装office2010以上的版本，问题即可解决