# Pandas简介

Pandas是一个开源的数据分析和数据处理库，它是基于Python变成语言的

Pandas提供了易于使用的数据接口和数据分析工具，特别适用于处理结构化数据，如表格型数据（类似于Excel表格）

Pandas是数据科学分析和分析领域中常用的工具之一，它使得用户能够轻松地从各种数据源中导入数据，并对数据进行高效的操作和分析



## Pandas主要引入了两种新的数据结构：DataFrame和Series

- **Series**：类似于一维数组或列表，是由一组数据以及与之相关的数据标签（索引）构成。Series可以看做是DataFrame中的一列，也可以是单独存在的一维数据结构

  ![](https://www.runoob.com/wp-content/uploads/2023/12/628084-20201205212241597-1156923446.png)

- **DataFrame**：类似于一个二维表格，它是Pandas中最重要的数据结构。DataFrame可以看做是由多个Series按列排列构成的表格，它既有行索引也有列索引，因此可以方便地进行行列选择、过滤、合并等操作

  ![](https://www.runoob.com/wp-content/uploads/2023/12/01_table_dataframe.svg)

## DataF可视为由多个Series组成的数据结构：

![](https://www.runoob.com/wp-content/uploads/2021/04/pandas-DataStructure.png)

### Pandas提供了丰富的功能，包括：

- 数据清洗：处理缺失数据、重复数据等
- 数据转换：改变数据的形状、结构或格式
- 数据分析：进行统计分析、聚合、分组等
- 数据可视化：通过整个Matplotlib和Seaborn等库，可以进行数据可视化

## Pandas应用

Pandas在数据科学和数据分析领域中具有广泛的应用，其中主要优势在于能够处理和分析结构化数据。

以下是Pandas的一些主要应用领域：

- **数据清洗和预处理**：Pandas被广泛应用于清理和预处理数据，包括处理缺失值、异常值、重复值等。它提供了各种方法来时使数据更合适进行进一步的分析。
- **数据分析和统计**：Pandas使数据分析变得更加简单，通过DataFrame和Series的灵活操作，用户可以轻松地进行统计分析、汇总、集合等操作。从均值、中位数到标准差和相关性分析、Pandas都提供了丰富的功能