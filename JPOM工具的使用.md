# 关于JPOM自动化部署（运维）工具的使用

## 1、地址

https://jpom-site.keepbx.cn/

## 2、介绍

​	JPOM是一款**简而轻的低侵入式在线构建、自动部署、日常运维、项目监控软件**，具体功能如下

1. 创建、修改、删除项目、Jar包管理

2. 实时查看控制台日志、备份日志、删除日志、导出日志

3. cpu、ram 监控、导出堆栈信息、查看项目进程端口、服务器状态监控

4. 多节点管理、多节点自动分发

5. 实时监控项目状态异常自动报警

6. 在线构建项目发布项目一键搞定

7. 多用户管理，用户项目权限独立(上传、删除权限可控制),完善的操作日志

8. 系统路径白名单模式，杜绝用户误操作系统文件

9. 在线管理Nginx配置、ssl证书文件

10. Tomcat状态、文件、war包在线实时管理

    **JPOM分为插件端和服务端，插件端建议放在目标服务器上，服务端可以放在本地，也可以放在目标服务器，但是插件端和服务端不能放在同一级目录下**

## 3、安装方式

- ### Linux

  Linux按照如下方法进行安装

  插件端：

  ```sh
  yum install -y wget && wget -O install.sh https://dromara.gitee.io/jpom/docs/install.sh && bash install.sh Agent
  
  备用地址:
  
  yum install -y wget && wget -O install.sh https://cdn.jsdelivr.net/gh/dromara/Jpom/docs/install.sh && bash install.sh Agent
  
  支持自动安装jdk环境:
  
  yum install -y wget && wget -O install.sh https://dromara.gitee.io/jpom/docs/install.sh && bash install.sh Agent jdk
  ```

  服务端：

  ```sh
  yum install -y wget && wget -O install.sh https://dromara.gitee.io/jpom/docs/install.sh && bash install.sh Server
  
  备用地址:
  
  yum install -y wget && wget -O install.sh https://cdn.jsdelivr.net/gh/dromara/Jpom/docs/install.sh && bash install.sh Server
  
  
  支持自动安装jdk环境:
  
  yum install -y wget && wget -O install.sh https://dromara.gitee.io/jpom/docs/install.sh && bash install.sh Server jdk
  
  支持自动安装jdk和maven环境:
  
  yum install -y wget && wget -O install.sh https://dromara.gitee.io/jpom/docs/install.sh && bash install.sh Server jdk+mvn
  ```

  当插件端和服务端都装在Linux时，启动方式分别为

- ### Windwos

  下载地址：

  https://gitee.com/dromara/Jpom/dromara/Jpom/attach_files/682745/download

  ​		下载后会有一个.zip压缩包，解压后，里面有两个.zip压缩包，agent是插件端，server是服务端，先不用管agent。将server压缩包进行解压，解压后会有一个Server.bat，点击运行，根据提示进行安装和启动

- ### Mac

  目前该工具暂不支持Mac的jar模式启动（也可能我没弄明白），可以从gitee上拉取源码进行编译启动

  gitee地址：https://gitee.com/dromara/Jpom?_from=gitee_search

  拉取后，目录下分为modules和web-vue两个重要文件，其中modules中有三个文件夹分别为agent、server、common，我们只需要启动server下面的即可（注意项目不能包含中文路径）,然后切换到web-vue目录下， 执行yarn install 命令，完成后，执行yarn serve命令，一切就绪后即可根据提示端口进行访问

## 4、操作

- 页面展示

  默认账号和密码为demo/demo123，登录后可修改

  ![image-20210907203941456](../../Library/Application%20Support/typora-user-images/image-20210907203941456.png)



- 具体操作
  - 点击节点管理，找到SSH管理，进入页面后点击新增，安装需要进行数据填写
  
  - 完成上一步后，接下来进行插件端的安装，这里分为两种方式
    - 在SSH管理页面找到刚刚添加的数据，在管连接点一列点击安装节点，根据要求填写需要安装插件端的信息，注意在安装路径一栏建议创建文件夹agent后安装在agent，否则安装的agent文件将会散乱分布在同级目录下，这里可能会报关于权限的问题，建议修改操作用户权限，如无法修改，建议使用第二种方式进行安装。
    - 在安装方式中，我们下载压缩包的时候，存在agent和server两个，现在我们把agent解压出来，然后将其上传到服务器上，之后对其进行chmod的授权，然后我们执行./Agent.sh start命令进行启动，注意控制台会打印出默认的账号密码
    
  - 完成agent安装后，如果是第一种方式安装，则在节点管理中的节点列表里可以直接看到，如果是第二种方式安装的，打开节点列表，点击新增，在弹出的窗口中，填写需要的信息（注意这里填写的是Agent的信息，所以Agent一定要处于启动的状态），其中节点地址中注意带上端口号，默认为2123，账号和密码会在上一步启动时控制台打印，如果是第一种方式启动的，那么找到对应目录下的log日志文件，在其中也可以看到账户密码。
  
    <img src="../../Library/Application%20Support/typora-user-images/image-20210908112516435.png" alt="image-20210908112516435" style="zoom:35%;" />
  
  - 添加完成后，点击编辑，重新打开这个添加节点页面，在绑定SSH中选择我们我们之前添加过的服务器，这样我们可以点击终端，再弹出的对话框中就可以操作我们的服务器。
  
  - 上面的步骤我们已经创建完成了SSH和对应的节点，接下来我们就可开始部署项目了，首先点击节点列表中的节点管理，在弹出的抽屉中点击白名单目录，在项目路径中写上需要部署项目的路径，可以写到放jar包的上一级。
  
  - 点击JDK管理，新增一个JDK
  
  - 点击项目列表，新增一个项目，其中运行方式选择jar方式，白名单中选择刚刚创建的白名单路径，项目文件夹一栏写上在白名单路径的前提下需要安装的路径，JDK选择刚刚新增的JDK。
  
  - 完成上一步后再项目列表中点击文件，上传项目jar包
  
  - 点击控制台，点击启动完成
  
- 监控

  - 项目首页

![image-20210908134825546](../../Library/Application%20Support/typora-user-images/image-20210908134825546.png)

