# 2.1 Redis 支持哪几种数据类型？

- String-字符串
- List-列表
- Set-集合
- Sorted Set-有序集合
- hash-哈希

# 2.3 Redis 中的管道有什么用？
一次请求/响应服务器能实现处理新的请求即使旧的请求还未被响应。这样就可以将多个命令发送到服务器，而不用等待回复，最后在一个步骤中读取该答复。
这就是管道（pipelining），是一种几十年来广泛使用的技术。
例如许多 POP3 协议已经实现支持这个功能，大大加快了从服务器下载新邮件的过程。

# 2.5 Jedis 与 Redisson 对比有什么优缺点？
Jedis 是 Redis 的 Java 实现的客户端，其 API 提供了比较全面的 Redis 命令的支持；

Redisson 实现了分布式和可扩展的 Java 数据结构，和 Jedis 相比，功能较为简单，不支持字符串操作，不支持排序、事务、管道、分区等 Redis 特性。Redisson 的宗旨是促进使用者对 Redis 的关注分离，从而让使用者能够将精力更集中地放在处理业务逻辑上。

# 2.9 都有哪些办法可以降低 Redis 的内存使用情况呢？

如果你使用的是 32 位的 Redis 实例，可以好好利用 Hash,list,sorted set,set 等集合类型数据， 因为通常情况下很多小的 Key-Value 可以用更紧凑的方式存放到一起。

# 2.10 查看 Redis 使用情况及状态信息用什么命令？

info

# 2.11 Redis 的内存用完了会发生什么

如果达到设置的上限，Redis 的写命令会返回错误信息（但是读命令还可以正常返回。）

或者你可以将 Redis 当缓存来使用配置淘汰机制，当 Redis 达到内存上限时会冲刷掉旧的内容。

# 2.12 Redis 是单线程的，如何提高多核 CPU 的利用率？

可以在同一个服务器部署多个 Redis 的实例，并把他们当作不同的服务器来使用，在某些时候，无论如何一个服务器是不够的，所以，如果你想使用多个 CPU，你可以考虑一下分片（shard）。

# 2.13 一个 Redis 实例最多能存放多少的 keys？
List、Set、Sorted Set 他们最多能存放多少元素？

理论上 Redis 可以处理多达 232 的 keys，并且在实际中进行了测试，每个实例至少存放了 2亿 5 千万的 keys。我们正在测试一些较大的值。任何 list、set、和 sorted set 都可以放 232 个元素。换句话说，Redis 的存储极限是系统中的可用内存值。

# 2.14 Redis 常见性能问题和解决方案？
Master 最好不要做任何持久化工作，如 RDB 内存快照和 AOF 日志文件

如果数据比较重要，某个 Slave 开启 AOF 备份数据，策略设置为每秒同步一次

为了主从复制的速度和连接的稳定性，Master 和 Slave 最好在同一个局域网内<
尽量避免在压力很大的主库上增加从库

主从复制不要用图状结构，用单向链表结构更为稳定，即：Master <- Slave1 <- Slave2<- Slave3…

# 2.15 修改配置不重启 Redis 会实时生效吗？
针对运行实例，有许多配置选项可以通过 CONFIG SET 命令进行修改，而无需执行任何形式的重启。 从 Redis 2.2 开始，可以从 AOF 切换到 RDB 的快照持久性或其他方式而不需要重启 Redis。检索 ‘CONFIG GET *’ 命令获取更多信息。

但偶尔重新启动是必须的，如为升级 Redis 程序到新的版本，或者当你需要修改某些目前CONFIG 命令还不支持的配置参数的时候。

# 2.16 MySQL 里有 2000w 数据，Redis 中只存 20w 的数据，如何保证 Redis 中的数据都是热点数据？
Redis 内存数据集大小上升到一定大小的时候，就会施行数据淘汰策略。

# Redis持久化
## 3.1 Redis 提供了哪几种持久化方式？
RDB 持久化方式能够在指定的时间间隔能对你的数据进行快照存储.

AOF 持久化方式记录每次对服务器写的操作,当服务器重启的时候会重新执行这些命令来恢复原始的数据,AOF 命令以 Redis 协议追加保存每次写的操作到文件末尾.Redis 还能对AOF 文件进行后台重写,使得 AOF 文件的体积不至于过大.

如果你只希望你的数据在服务器运行的时候存在,你也可以不使用任何持久化方式.

你也可以同时开启两种持久化方式, 在这种情况下, 当 Redis 重启的时候会优先载入 AOF 文件来恢复原始的数据,因为在通常情况下AOF 文件保存的数据集要比RDB 文件保存的数据集要完整.

最重要的事情是了解 RDB 和 AOF 持久化方式的不同,让我们以 RDB 持久化方式开始。

## 3.2 如何选择合适的持久化方式？
一般来说， 如果想达到足以媲美 PostgreSQL 的数据安全性， 你应该同时使用两种持久化功能。如果你非常关心你的数据， 但仍然可以承受数分钟以内的数据丢失，那么你可以只使用 RDB 持久化。

有很多用户都只使用 AOF 持久化， 但并不推荐这种方式： 因为定时生成 RDB 快照（snapshot）非常便于进行数据库备份， 并且 RDB 恢复数据集的速度也要比 AOF 恢复的速度要快，除此之外， 使用 RDB 还可以避免之前提到的 AOF 程序的 bug。

## 3.3 Redis 持久化数据和缓存怎么做扩容？
如果 Redis 被当做缓存使用，使用一致性哈希实现动态扩容缩容。

如果 Redis 被当做一个持久化存储使用，必须使用固定的 keys-to-nodes 映射关系，节点的数量一旦确定不能变化。否则的话(即 Redis 节点需要动态变化的情况），必须使用可以在运行时进行数据再平衡的一套系统，而当前只有 Redis 集群可以做到这样。



## 3.4 redis和memcached的比较

1. memcached 所有的值均是简单的字符串，Redis 作为其替代者，支持更为丰富的数据类型
2. Redis 的速度比 memcached 快很多
3. Redis 可以持久化其数据

## 3.5 redis为什么快

**1.纯内存KV操作**
Redis的操作都是基于内存的，CPU不是 Redis性能瓶颈,，Redis的瓶颈是机器内存和网络带宽。
在计算机的世界中，CPU的速度是远大于内存的速度的，同时内存的速度也是远大于硬盘的速度。redis的操作都是基于内存的，绝大部分请求是纯粹的内存操作，非常迅速。

**2.单线程操作**
使用单线程可以省去多线程时CPU上下文会切换的时间，也不用去考虑各种锁的问题，不存在加锁释放锁操作，没有死锁问题导致的性能消耗。对于内存系统来说，多次读写都是在一个CPU上，没有上下文切换效率就是最高的！既然单线程容易实现，而且 CPU 不会成为瓶颈，那就顺理成章的采用单线程的方案了
Redis 单线程指的是网络请求模块使用了一个线程，即一个线程处理所有网络请求，其他模块该使用多线程，仍会使用了多个线程。

**3.I/O 多路复用**
为什么 Redis 中要使用 I/O 多路复用这种技术呢？
首先，Redis 是跑在单线程中的，所有的操作都是按照顺序线性执行的，但是由于读写操作等待用户输入或输出都是阻塞的，所以 I/O 操作在一般情况下往往不能直接返回，这会导致某一文件的 I/O 阻塞导致整个进程无法对其它客户提供服务，而 I/O 多路复用就是为了解决这个问题而出现的。

**4.Reactor 设计模式**

Redis基于[Reactor模式](https://so.csdn.net/so/search?q=Reactor模式&spm=1001.2101.3001.7020)开发了自己的网络事件处理器，称之为文件事件处理器(File Event Hanlder)。文件事件处理器由Socket、IO多路复用程序、文件事件分派器(dispather)，事件处理器(handler)四部分组成。

# Redis集群
## 4.1 Redis 集群方案应该怎么做？都有哪些方案？
twemproxy
大概概念是，它类似于一个代理方式，使用方法和普通 Redis 无任何区别， 设置好它下属的多个 Redis 实例后， 使用时在本需要连接 Redis 的地方改为连接twemproxy，它会以一个代理的身份接收请求并使用一致性 hash 算法，将请求转接到具体 Redis，将结果再返回 twemproxy。使用方式简便(相对 Redis 只需修改连接端口)，对旧项目扩展的首选。 问题：twemproxy 自身单端口实例的压力，使用一致性 hash 后，对Redis 节点数量改变时候的计算值的改变，数据无法自动移动到新的节点。

codis
目前用的最多的集群方案，基本和 twemproxy 一致的效果，但它支持在 节点数量改变情况下，旧节点数据可恢复到新 hash 节点。

Redis cluster
3.0 自带的集群，特点在于他的分布式算法不是一致性 hash，而是 hash 槽的概念，以及自身支持节点设置从节点。具体看官方文档介绍。

在业务代码层实现
起几个毫无关联的 Redis 实例，在代码层，对 key 进行 hash 计算， 然后去对应的 Redis 实例操作数据。 这种方式对 hash 层代码要求比较高，考虑部分包括， 节点失效后的替代算法方案，数据震荡后的自动脚本恢复，实例的监控，等等

## 4.2 Redis 集群方案什么情况下会导致整个集群不可用？
有 A，B，C 三个节点的集群,在没有复制模型的情况下,如果节点 B 失败了，那么整个集群就会以为缺少 5501-11000 这个范围的槽而不可用。

## 4.3 说说 Redis 哈希槽的概念？
Redis 集群没有使用一致性 hash,而是引入了哈希槽的概念，Redis 集群有 16384 个哈希槽， 每个 key 通过 CRC16 校验后对 16384 取模来决定放置哪个槽，集群的每个节点负责一部分hash 槽。

## 4.4 Redis 集群的主从复制模型是怎样的
为了使在部分节点失败或者大部分节点无法通信的情况下集群仍然可用，所以集群使用了主 从复制模型,每个节点都会有 N-1 个复制品.

## 4.5 Redis 集群会有写操作丢失吗？为什么？
Redis 并不能保证数据的强一致性，这意味这在实际中集群在特定的条件下可能会丢失写操作。

## 4.6 Redis 集群之间是如何复制的？
异步复制

## 4.7 Redis 集群最大节点个数是多少？
16384 个

## 4.8 Redis 集群如何选择数据库？
Redis 集群目前无法做数据库选择

单节点，默认在 0 数据库。

## 4.9 分布式Redis 是前期做还是后期规模上来了再做好？为什么？
既然 Redis 是如此的轻量（单实例只使用 1M 内存）,为防止以后的扩容，最好的办法就是一开始就启动较多实例。即便你只有一台服务器，你也可以一开始就让 Redis 以分布式的方式运行，使用分区，在同一台服务器上启动多个实例。

一开始就多设置几个 Redis 实例，例如 32 或者 64 个实例，对大多数用户来说这操作起来可能比较麻烦，但是从长久来看做这点牺牲是值得的。

这样的话，当你的数据不断增长，需要更多的 Redis 服务器时，你需要做的就是仅仅将 Redis 实例从一台服务迁移到另外一台服务器而已（而不用考虑重新分区的问题）。一旦你添加了另一台服务器，你需要将你一半的 Redis 实例从第一台机器迁移到第二台机器。

## 4.10、如果Redis中的热点Key突然失效了，有什么可以保证大量的访问正常请求

1. 使用缓存预热：在缓存数据过期或重启服务时，可以预先通过定时任务或脚本加载缓存数据，以避免缓存穿透问题。
2. 使用限流算法：通过限流算法来限制单个请求的访问频率，以防止热点Key引发的雪崩效应。
3. 使用数据备份：可以通过数据备份，如主从复制、AOF持久化等方式，实现数据的冗余备份，以保证数据的安全性。
4. 分布式缓存：考虑使用分布式缓存系统，例如Memcached、Redis Cluster等，以实现数据的分布式存储。
5. 使用应用层缓存：在应用代码中，加入应用层缓存，以缓存热点数据，避免频繁的请求数据库，也可以降低对数据库的压力。

# Redis事务
## 5.1 怎么理解 Redis 事务？
事务是一个单独的隔离操作：事务中的所有命令都会序列化、按顺序地执行。事务在执行的 过程中，不会被其他客户端发送来的命令请求所打断。

事务是一个原子操作：事务中的命令要么全部被执行，要么全部都不执行。

## 5.2 Redis 事务相关的命令有哪几个？
MULTI
EXEC
DISCARD
WATCH

# Redis回收
## 6.1 Redis key 的过期时间和永久有效分别怎么设置？
EXPIRE 和 PERSIST 命令

## 6.2 Redis 如何做内存优化？
尽可能使用散列表（hashes），散列表（是说散列表里面存储的数少）使用的内存非常小，所以你应该尽可能的将你的数据模型抽象到一个散列表里面。比如你的 web 系统中有一个用户对象，不要为这个用户的名称，姓氏，邮箱，密码设置单独的 key,而是应该把这个用户的所有信息存储到一张散列表里面。

## 6.3 Redis 回收进程如何工作的？
一个客户端运行了新的命令，添加了新的数据。
Redis 检查内存使用情况，如果大于 maxmemory 的限制, 则根据设定好的策略进行回收。一个新的命令被执行，等等。
所以我们不断地穿越内存限制的边界，通过不断达到边界然后不断地回收回到边界以下。
如果一个命令的结果导致大量内存被使用（例如很大的集合的交集保存到一个新的键），不用多久内存限制就会被这个内存使用量超越。

## 6.4 Redis 回收使用的是什么算法？
[LRU 算法](../面经)

## 6.5 Redis 如何做大量数据插入？
Redis2.6 开始 Redis-cli 支持一种新的被称之为pipe mode 的新模式用于执行大量数据插入工作。

## 6.6 Redis 有哪几种数据淘汰策略？
no-eviction
返回错误当内存限制达到并且客户端尝试执行会让更多内存被使用的命令（大 部分的写入指令，但 DEL 和几个例外）
allkeys-lru
尝试回收最少使用的键（LRU），使得新添加的数据有空间存放。
volatile-lru
尝试回收最少使用的键（LRU），但仅限于在过期集合的键,使得新添加的数据有空间存放。
allkeys-random
回收随机的键使得新添加的数据有空间存放。
volatile-random
回收随机的键使得新添加的数据有空间存放，但仅限于在过期集合的键。
volatile-ttl
回收在过期集合的键，并且优先回收存活时间（TTL）较短的键,使得新添加的数据有空间存放。

# Redis分区
## 7.1 为什么要做 Redis 分区？
分区可以让 Redis 管理更大的内存，Redis 将可以使用所有机器的内存。
如果没有分区，你最多只能使用一台机器的内存。
分区使 Redis 的计算能力通过简单地增加计算机得到成倍提升,Redis 的网络带宽也会随着计算机和网卡的增加而成倍增长。

## 7.2 你知道有哪些 Redis 分区实现方案？
客户端分区
就是在客户端就已经决定数据会被存储到哪个 Redis 节点或者从哪个 Redis 节点读取。大多数客户端已经实现了客户端分区。
代理分区
意味着客户端将请求发送给代理，然后代理决定去哪个节点写数据或者读数据。代理根据分区规则决定请求哪些 Redis 实例，然后根据 Redis 的响应结果返回给客户端。Redis 和 memcached 的一种代理实现就是 Twemproxy
查询路由(Query routing)
客户端随机地请求任意一个 Redis 实例，然后由 Redis 将请求转发给正确的 Redis 节点。Redis Cluster 实现了一种混合形式的查询路由，但并不是直接将请求从一个 Redis 节点转发到另一个 Redis 节点，而是在客户端的帮助下直接redirected 到正确的 Redis 节点。
## 7.3 Redis 分区有什么缺点？
涉及多个 key 的操作通常不会被支持。例如你不能对两个集合求交集，因为他们可能被存储到不同的 Redis 实例（实际上这种情况也有办法，但是不能直接使用交集指令）。

同时操作多个 key,则不能使用 Redis 事务.

分区使用的粒度是key，不能使用一个非常长的排序key 存储一个数据集（The partitioning granularity is the key, so it is not possible to shard a dataset with a single huge key like a very big sorted set）.

当使用分区的时候，数据处理会非常复杂，例如为了备份你必须从不同的 Redis 实例和主机同时收集 RDB / AOF 文件。

分区时动态扩容或缩容可能非常复杂。Redis 集群在运行时增加或者删除 Redis 节点，能做到最大程度对用户透明地数据再平衡，但其他一些客户端分区或者代理分区方法则不支持 这种特性。然而，有一种预分片的技术也可以较好的解决这个问题。
